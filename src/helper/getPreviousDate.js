exports.get20YearAGoDate = () => {
  return new Date(new Date().setFullYear(new Date().getFullYear() - 18));
};
