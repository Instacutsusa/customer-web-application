// REDUX ACTION TYPES

export const SET_ACTIVE_FILTER_TAB = "SET_ACTIVE_FILTER_TAB";
// Define cart action types
export const SHOW_CART = "SHOW_CART";
export const SHOW_TOAST = "SHOW_TOAST";

// Define user action types
export const USER_SIGNUP_ERROR = "USER_SIGNUP_ERROR";
export const USER_LOGIN = "USER_LOGIN";
export const USER_LOGOUT = "USER_LOGOUT";
export const USER_PROFILE = "USER_PROFILE";

export const INIT_CL = "INIT_CL";
export const UPDATE_REFRESH_TOKEN_CL = "UPDATE_REFRESH_TOKEN_CL";

export const INIT_PURVEYORS_LIST = "INIT_PURVEYORS_LIST";

export const PURVEYOR_MAJOR_CATEGORY = "PURVEYOR_MAJOR_CATEGORY";
