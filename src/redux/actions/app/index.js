import * as types from '../../types';

// SHOW CART showCart
export const showCart = () => dispatch => {
  dispatch({
    type: types.SHOW_CART,
  });
};

// Set sideFilter active Filter Tab
export const setActiveFilterTab = data => dispatch => {
  dispatch({
    type: types.SET_ACTIVE_FILTER_TAB,
    payload: data,
  });
};

// SHOW TOAST showToast
export const showToastAction = data => dispatch => {
  dispatch({
    type: types.SHOW_TOAST,
    payload: data,
  });
};

export const initializeCL = data => dispatch => {
  dispatch({
    type: types.INIT_CL,
    payload: data,
  });
};

export const updateRefreshTokenCL = data => dispatch => {
  dispatch({
    type: types.UPDATE_REFRESH_TOKEN_CL,
    payload: data,
  });
};

export const initializePurveyorsList = data => dispatch => {
  dispatch({
    type: types.INIT_PURVEYORS_LIST,
    payload: data,
  });
};

export const purveyorMajorCategory = data => dispatch => {
  dispatch({
    type: types.PURVEYOR_MAJOR_CATEGORY,
    payload: data,
  });
};
