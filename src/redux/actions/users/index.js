import * as types from "../../types";

// User Signup
export const userSignupError = (error) => (dispatch) => {
  dispatch({
    type: types.USER_SIGNUP_ERROR,
    payload: error,
  });
};

// User log in
export const userLogin = (user) => (dispatch) => {
  dispatch({
    type: types.USER_LOGIN,
    payload: user,
  });
};

// User profile
export const userProfile = (user) => (dispatch) => {
  dispatch({
    type: types.USER_PROFILE,
    payload: user,
  });
};

export const userLogout = () => (dispatch) => {
  dispatch({
    type: types.USER_LOGOUT,
  });
};
