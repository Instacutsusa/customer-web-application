import { USER_LOGIN, USER_LOGOUT, USER_PROFILE } from "../../types";

const userState = {};

const userReducer = (state = userState, action) => {
  switch (action.type) {
    case USER_LOGIN:
      return {
        ...state,
        user: action.payload,
      };
    case USER_LOGOUT:
      return {
        ...state,
        user: {},
      };

    case USER_PROFILE:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};

export default userReducer;
