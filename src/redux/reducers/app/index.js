import {
  SET_ACTIVE_FILTER_TAB,
  SHOW_CART,
  SHOW_TOAST,
  INIT_CL,
  PURVEYOR_MAJOR_CATEGORY,
  UPDATE_REFRESH_TOKEN_CL,
  INIT_PURVEYORS_LIST,
} from '../../types';

const initialAppState = {
  active_filter_tab: 'marketplace',
  showCart: false,
  showToast: false,
  toastMessage: '',
  CL: {},
  allPurveyors: [],
  purveyorMajorCategory: [],
};

const appReducer = (state = initialAppState, action) => {
  switch (action.type) {
    case SET_ACTIVE_FILTER_TAB: {
      return {
        ...state,
        active_filter_tab: action.payload,
      };
    }
    case SHOW_CART:
      return {
        ...state,
        showCart: !state.showCart,
      };
    case SHOW_TOAST:
      return {
        ...state,
        showToast: !state.showToast,
        toastMessage: action.payload,
      };
    case INIT_CL:
      return {
        ...state,
        CL: action.payload,
      };
    case UPDATE_REFRESH_TOKEN_CL: {
      return {
        ...state,
        CL: {
          refresh_token: action.payload,
        },
      };
    }
    case INIT_PURVEYORS_LIST:
      return {
        ...state,
        allPurveyors: action.payload,
      };
    case PURVEYOR_MAJOR_CATEGORY:
      return {
        ...state,
        purveyorMajorCategory: action.payload,
      };
    default:
      return state;
  }
};

export default appReducer;
