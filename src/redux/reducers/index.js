import { combineReducers } from 'redux';
import appReducer from './app';
import userReducer from './users';

// COMBINED REDUCERS
const reducers = {
  app: appReducer,
  user: userReducer,
};

export default combineReducers(reducers);
