import React from "react";
import StarRatings from "react-star-ratings";
import Review from "../../components/Review/review";

const StylistReview = (props) => {
  return (
    <>
      <div className="review-heading border-0 pt-4">
        <div className="title">
          <h5>Reviews ({props.rating_count})</h5>
        </div>
        <div className="review-score">
          <h4>
            {props?.avg_rating} <span>/ 5</span>
          </h4>
          <StarRatings
            rating={props?.avg_rating || 0}
            starDimension="15px"
            starSpacing="3px"
            starRatedColor="rgb(255, 153, 31)"
          />
        </div>
        <h6>{props.rating_count} reviews</h6>
      </div>
      <Review stylisDetail={props.stylisDetail} />
    </>
  );
};

export default StylistReview;
