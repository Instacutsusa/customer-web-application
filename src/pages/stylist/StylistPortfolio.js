import React from "react";
import PropTypes from "prop-types";

const StylistPortfolio = ({ portfolio_images, portfolio_videos }) => {
  return (
    <>
      {portfolio_images?.map((image, index) => {
        return (
          <div className="row" key={index}>
            <div className="col custom-col">
              <div className="portfolio">
                <img src={image} alt="" />
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
};

StylistPortfolio.propTypes = {
  portfolio_images: PropTypes.array,
  portfolio_videos: PropTypes.array,
};

export default StylistPortfolio;
