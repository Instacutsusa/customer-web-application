import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import ActiveStylist from "../../components/Stylist/stylist";
import StylistList from "../../components/Stylist/StylistList/StylistList";
import Header from "../../components/DynamicHeader/Header";


const Stylist = (props) => {
  
  const noServiceText = "No Active Stylists In your Location";
  const differentService = "Try Advanced Stylists or Change Your Chosen Location";
  
    return (
      <Fragment>
        <Header {...props} />
        <div className="container pb-5">
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/">Home</Link>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                Stylist
              </li>
            </ol>
          </nav>
          <ActiveStylist />
          <StylistList
            noServiceText={noServiceText}
            differentService={differentService}
          />
        </div>
      </Fragment>
    );
}

export default Stylist;
