import React, { Component, Fragment } from 'react';
import SideBar from '../../components/Sidebar/Sidebar';
import SidebarHeader from '../../components/Sidebar/SidebarHeader/SidebarHeader';
import GiftImg from '../../assets/images/gift.png';
import HandShakeImg from '../../assets/images/hand.png';
import './reward.css';

class Rewards extends Component {
    state={
        toggleSidebar: false,
        active: "reward"
    }
    SideBarToggle = () => {
        this.setState({
            toggleSidebar: !this.state.toggleSidebar
        })
    }
    render() {

        const heading = "Rewards",
              subHeading = "My Rewards";
        const { toggleSidebar, active } = this.state;
        return(
            <Fragment>
            
                <div className="d-flex pb-5">
                    <div className="">
                        <SideBar SideBarToggle={this.SideBarToggle} active={active} toggleSidebar={toggleSidebar}/>
                    </div>
                    <div className="sideBar-header"> 
                        <SidebarHeader heading={heading} subHeading={subHeading} toggleSidebar={toggleSidebar} SideBarToggle={this.SideBarToggle}/>
                        <div className="row w-md-75 m-auto pt-md-5 pt-3">
                            <div className="col-md-6">
                                <div className="reward-section">
                                    <div className="reward-img">
                                        <img src={HandShakeImg} alt=" " />
                                    </div>
                                    <h5>Invite Your Friends <br/>and Earn $10</h5>
                                    <p>Invite 30+ Contacts on your phone to instacuts <br/> &amp; we will credit $10 to your wallet</p>
                                    <button>Invite Friends</button>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="reward-section border-0">
                                    <div className="reward-img">
                                        <img src={GiftImg} alt=" " />
                                    </div>
                                    <h5>You Win, I Win</h5>
                                    <p>For every friend who uses your voucher code<br/> we will give you and your friend $10</p>
                                    <button>INV129DS90A</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default Rewards;