import React, { Component, Fragment } from "react";
import { geolocated } from "react-geolocated";
import "./address.css";
import NavIcon from "../../assets/icons/nav-icon.svg";
import LocationIcon from "../../assets/icons/location-icon.svg";
import LocationSearchInput from "./LocationSearchInput";
import { getAddLocation, getListAddresses } from "../../api/myAccounts.api";
import { NotificationAlert } from "../../common/NotificationAlert";
import {
  NOTIFICATIONSMESSAGE,
  NOTIFICATION_TYPE,
} from "../../_constant/notificationsMessage";
import Loader from "../../components/Loader/Loader";
import GoogleMap from "../../components/GoogleMap";
import { Modal } from "react-bootstrap";

class AddressMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalOpen: false,
      DefaultLocation: { lat: 29.7604267, lng: -95.3698028 },
      location: { lat: "", lng: "" },
      address: "",
      // gps: falMapPickerse,
      isMarkerShown: false,
      activeLink: "home",
      formatted_address: "",
      addressTag: "",
      isEdit: false,
      editAddressId: "",
      addressData: [],
      editAddressInfo: [],
      isLoader: true,
      mapLoader: true,
      generateNewMap: true,
      isEnabledPermission: false,
    };
    this.handlePermission = this.handlePermission.bind(this);
  }

  componentDidMount = async () => {
    const query = new URLSearchParams(this.props.location.search);
    const isEdit = query.get("edit");
    const editAddressId = query.get("address_id");
    if (isEdit) {
      this.handlePermission();
      await this.fetchEditAddress(editAddressId, isEdit);
    } else {
      this.handlePermission();
      this.setCurrentLocation();
      this.setState({
        ...this.state,
        isLoader: false,
      });
    }
  };

  setCurrentLocation = () => {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          if (position) {
            this.setState({
              location: {
                lat: position.coords.latitude,
                lng: position.coords.longitude,
              },
            });
          }
        },
        (error) => {
          console.log("geo location error", error);
        },
        { enableHighAccuracy: true }
      );
    }
  };

  handlePermission = () => {
    var that = this;
    navigator.permissions
      .query({ name: "geolocation" })
      .then(function (result) {
        if (result.state === "granted") {
          that.setState({
            ...that.state,
            isEnabledPermission: true,
          });
        } else if (result.state === "prompt") {
          //   navigator.geolocation.getCurrentPosition(revealPosition,positionDenied,geoSettings);
        } else if (result.state === "denied") {
          that.setState({
            ...that.state,
            isEnabledPermission: false,
            modalOpen: true,
          });
          if (!that.state.isEdit) {
            that.setState({
              ...that.state,
              location: {
                lat: 37.09024,
                lng: -95.712891,
              },
            });
          }
        }
      });
  };

  fetchEditAddress = async (editAddressId, isEdit) => {
    try {
      this.setState({
        ...this.state,
        isEdit: true,
        isLoader: true,
      });

      const { data } = await getListAddresses();

      const editInfo = data.find((address) => address._id === editAddressId);
      const addressTag = editInfo.title;
      this.setState({
        ...this.state,
        addressData: data,
        editAddressInfo: editInfo,
        activeLink: editInfo.address_type,
        editAddressId,
        addressTag,
        location: {
          lat: editInfo.lat,
          lng: editInfo.lng,
        },
      });
    } catch (err) {
      console.log(err);
    } finally {
      this.setState({
        ...this.state,
        isLoader: false,
      });
    }
  };

  handleModal = (value) => {
    this.setState({
      ...this.state,
      modalOpen: value,
    });
  };

  handleActiveClick = (e) => {
    const { name } = e.target;
    this.setState({
      ...this.state,
      activeLink: name,
    });
  };

  onChangeHandler = (e) => {
    const { name } = e.target;
    this.setState({
      [name]: e.target.value,
    });
  };

  handleGps = () => {
    this.setState({
      gps: true,
    });
  };

  onGetCurrentLocation = () => {
    this.handlePermission();
    if (this.state.isEnabledPermission) {
      const { latitude, longitude } = this?.props?.coords;
      this.setState({
        ...this.state,
        location: {
          lat: latitude,
          lng: longitude,
        },
      });
    }
  };

  setGenerateNewMap = (value) => {
    this.setState({
      ...this.state,
      generateNewMap: value,
    });
  };

  // Get searched address Lat and Long
  onGetCurrentLocationLatLong = (data, address, newMap) => {
    this.setState({
      ...this.state,
      location: data,
      formatted_address: address,
      address: address.formatted_address,
    });
  };

  // Add new customer / edit address when click on 'SAVE LOCATION' button
  onAddNewLocationHandler = async () => {
    const {
      location,
      formatted_address,
      activeLink,
      addressTag,
      isEdit,
      editAddressId,
    } = this.state;
    try {
      const user = JSON.parse(localStorage.getItem("customer_info"));
      const city = formatted_address.address_components.find((components) =>
        components.types.includes("administrative_area_level_2")
      );
      const state = formatted_address.address_components.find((components) =>
        components.types.includes("administrative_area_level_1")
      );
      const country = formatted_address.address_components.find((components) =>
        components.types.includes("country")
      );
      const apiBody = {
        user_id: user._id,
        address_type: activeLink,
        address: formatted_address?.formatted_address,
        ...location,
        title: activeLink === "other" ? addressTag : activeLink,
        city: city.long_name,
        state: state.long_name,
        country: country.long_name,
        country_code: "+1",
        address_id: isEdit ? editAddressId : null,
      };
      await getAddLocation(apiBody);

      const notifications_msg = isEdit
        ? NOTIFICATIONSMESSAGE.UPDATE_ADDRESS
        : NOTIFICATIONSMESSAGE.ADD_NEW_ADDRESS;
      NotificationAlert(notifications_msg, NOTIFICATION_TYPE.SUCCESS);

      //  Redirect to my profile page if user try to update his address
      isEdit
        ? this.props.history.push("/myaccount")
        : this.props.history.push("/home");
    } catch (err) {
      NotificationAlert(
        NOTIFICATIONSMESSAGE.API_ERROR,
        NOTIFICATION_TYPE.ERROR
      );
      console.log(err);
    }
  };

  onMarkerInteraction = (childKey, childProps, mouse) => {
    this.setState({
      draggable: false,
      location: {
        lat: mouse.lat,
        lng: mouse.lng,
      },
    });
  };

  render() {
    const {
      modalOpen,
      isEdit,
      DefaultLocation,
      address,
      addressTag,
      activeLink,
      location,
      editAddressInfo,
      generateNewMap,
      isLoader,
      isEnabledPermission,
    } = this.state;
    if (isLoader) {
      return <Loader />;
    }
    return (
      <Fragment>
        <Modal
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          show={modalOpen}
          onHide={() => this.handleModal(false)}
          className="locationPermissionModal"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-lg">
              <img src={LocationIcon} alt="" className="mx-2" />
              Location permission
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5>
              Please enabled the location permissin to get current location
            </h5>
          </Modal.Body>
          <Modal.Footer />
        </Modal>
        <div className="container pb-5">
          <div className="row">
            <div className="col-md-12">
              <div className="back-btn mt-5">
                {/* <Link to="/"> */}
                <span onClick={() => this.props.history.goBack()}>
                  <i
                    className="fa fa-long-arrow-left mr-2"
                    aria-hidden="true"
                  ></i>{" "}
                  Back
                </span>
                {/* </Link> */}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="m-auto col-lg-5 offset-lg-3 col-md-8 offset-md-2">
              <div className="signin-container mb-0 pb-5">
                <div className="title">
                  <h4>Select your location</h4>
                  <h6>
                    Move the map pin to find your location and enter your
                    address details below.
                  </h6>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-10 col-md-12">
              <div className="form-row d-flex flex-md-nowrap">
                <div className="form-group w-100 mr-md-4 position-relative address-input-container">
                  <LocationSearchInput
                    onGetCurrentLocationLatLong={
                      this.onGetCurrentLocationLatLong
                    }
                    address={address}
                    editAddressInfo={editAddressInfo}
                  />

                  <img src={NavIcon} alt="" className="nav-icon" />
                  <i className="fa fa-check" aria-hidden="true"></i>
                </div>
                {activeLink === "other" && (
                  <div className="form-group w-100 position-relative address-input-container">
                    <input
                      type="text"
                      className="form-control address-input"
                      placeholder="Marray's House"
                      id="otherAddress"
                      name="addressTag"
                      value={addressTag}
                      onChange={this.onChangeHandler}
                    />
                    <i className="fa fa-at" aria-hidden="true"></i>
                    <i className="fa fa-check" aria-hidden="true"></i>
                  </div>
                )}
              </div>
            </div>
            <div className="col-lg-2 col-md-12 text-center mb-4">
              <button
                className="save-location-btn"
                onClick={this.onAddNewLocationHandler}
              >
                Save Location
              </button>
            </div>
          </div>

          <div className="row">
            <div className="col-md-12">
              <div className="map-container">
                <div className="address-type">
                  <button
                    name="home"
                    className={activeLink === "home" ? " active" : ""}
                    onClick={this.handleActiveClick}
                  >
                    Home
                  </button>
                  <button
                    name="work"
                    className={activeLink === "work" ? " active" : ""}
                    onClick={this.handleActiveClick}
                  >
                    work
                  </button>
                  <button
                    name="other"
                    onClick={this.handleActiveClick}
                    className={activeLink === "other" ? " active" : ""}
                  >
                    other
                  </button>
                </div>
                <GoogleMap
                  isEdit={isEdit}
                  isEnabledPermission={isEnabledPermission}
                  defaultLocation={DefaultLocation}
                  location={location}
                  setGenerateNewMap={(value) => this.setGenerateNewMap(value)}
                  generateNewMap={generateNewMap}
                  onGetCurrentLocation={this.onGetCurrentLocation}
                  onGetCurrentLocationLatLong={this.onGetCurrentLocationLatLong}
                  onMarkerInteraction={(mouse, childKey, childProps) =>
                    this.onMarkerInteraction(childKey, childProps, mouse)
                  }
                />
                {/* <div className="gps-button">
                  {gps ? (handleApiLoaded
                    <div className="gps-on">
                      <button>GPS ACTIVE</button>
                    </div>
                  ) : (
                    <div className="gps-off">
                      <button onClick={this.handleGps}>TURN ON GPS</button>
                    </div>
                  )}
                </div> */}
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(AddressMap);
