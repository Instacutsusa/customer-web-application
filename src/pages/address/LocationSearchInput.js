import React, { useEffect, useState } from "react";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from "react-places-autocomplete";
import "./address.css";

const LocationSearchInput = (props) => {
    const [address, _address] = useState("");
    const [formatedAddress, _formatedAddress] = useState("");

  useEffect(() => {
    const {editAddressInfo} = props;
    if(editAddressInfo){
      _address(editAddressInfo.address || "");
      handleSelect(editAddressInfo.address || "");
    }
  },[]);

  useEffect(() => {
    if(props.address){
      _address(props.address);
    }
  },[props.address])

  const handleChange = (address) => {
    _address(address);
  };

  const handleSelect = async(address) => {
    const addressArray = await geocodeByAddress(address);
    const latLng = await getLatLng(addressArray[0]);
    if(addressArray[0]){
      _address(addressArray[0].formatted_address);
        _formatedAddress(addressArray[0]);
        props.onGetCurrentLocationLatLong(
          latLng,
          addressArray[0],
          false,
        )
    }
  };

    return (
      <PlacesAutocomplete
        value={address}
        onChange={handleChange}
        onSelect={(address) => handleSelect(address)}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <div>
            <input
              {...getInputProps({
                placeholder: "Search Places ...",
                className: "location-search-input form-control address-input",
              })}
            />
            <div className="autocomplete-dropdown-container">
              {loading && <div>Loading...</div>}
              {suggestions.map((suggestion,index) => {
                const className = suggestion.active
                  ? "suggestion-item--active"
                  : "suggestion-item";
                // inline style for demonstration purpose
                const style = suggestion.active
                  ? { backgroundColor: "#fafafa", cursor: "pointer" }
                  : { backgroundColor: "#ffffff", cursor: "pointer" };
                return (
                  <div
                    key={`addressSuggestion.${index}`}
                    {...getSuggestionItemProps(suggestion, {
                      className,
                      style,
                    })}
                  >
                    <span>{suggestion.description}</span>
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
    );
}

export default LocationSearchInput;
