import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { Calendar } from "react-date-range";
import PasswordStrengthBar from "react-password-strength-bar";
import PasswordChecklist from "react-password-checklist";

import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";
import "./SignUp.css";
import InstacutsIcons from "../../assets/images/instacuts.png";
import CountryImage from "../../assets/images/map.png";
import PhoneIcon from "../../assets/icons/Smartphone.svg";
// import ShowIcon from "../../assets/icons/Show.svg";
import CalendarIcon from "../../assets/icons/Calendar.svg";
import { get20YearAGoDate } from "../../helper/getPreviousDate";
import { customeSignUp } from "../../api/login/login.api";
import { NotificationAlert } from "../../common/NotificationAlert";
import { NOTIFICATIONSMESSAGE } from "../../_constant/notificationsMessage";
// import getDeviceToken from "../../helper/getDeviceToken";

class SignIn extends Component {
  state = {
    fname: "",
    lname: "",
    phone_number: "",
    country_code: "",
    password: "",
    dateofbirth: "",
    gender: "",
    email: "",
    isErrors: false,
    errors: [],
    accountCreation: false,
    showDropDown: false,
    date: "",
    showDatePicker: false,
    overlayDrop: false,
    isValidPassword: false,
  };

  componentDidMount = async () => {
    const { state } = this.props.location;
    if (
      !Object(state).hasOwnProperty("phoneNumber") ||
      !Object(state).hasOwnProperty("country_code")
    ) {
      return this.props.history.push("/");
    }

    this.setState({
      ...this.state,
      phone_number: state?.phoneNumber,
      country_code: state?.country_code || "+1",
    });
  };

  onDisplayError = (field) => {
    const { errors } = this.state;
    const err = errors?.map((error) => {
      return error.param === field && error.msg;
    });
    return err;
  };

  onChangeHandler = (e) => {
    const { name } = e.target;
    this.setState({
      [name]: e.target.value,
    });
  };

  onHandleDropdown = () => {
    this.setState({
      showDropDown: !this.state.showDropDown,
    });
  };

  handleShowDatePicker = () => {
    this.setState({
      showDatePicker: !this.state.showDatePicker,
      overlayDrop: true,
    });
  };

  onSubmit = async (e) => {
    e.preventDefault();
    const apiBody = {
      firstname: this.state.fname,
      lastname: this.state.lname,
      email: this.state.email,
      password: this.state.password,
      gender: this.state.gender,
      dob: this.state.date,
      phone_number: this.state.phone_number,
      country_code: this.state.country_code,
    };
    try {
      const { data, status, message } = await customeSignUp(apiBody);
      if (status === 200) {
        NotificationAlert(message, "success");
        this.props.history.push("/profile");
        localStorage.setItem("customer_info", JSON.stringify(data));
        localStorage.setItem("access_token", data.token);
      }
    } catch (err) {
      this.setState({
        ...this.state,
        isErrors: true,
      });
      NotificationAlert(NOTIFICATIONSMESSAGE.SIGNUP_INVALID_VALUE, "error");
      const errors = err.response.data.data;
      this.setState({
        errors,
      });
    }

    // this.setState({
    //   isErrors: true,
    // });
    // console.log(this.state);
  };

  handleOverlay = () => {
    this.setState({
      overlayDrop: !this.state.overlayDrop,
      showDatePicker: false,
    });
  };

  render() {
    const {
      phone_number,
      fname,
      lname,
      password,
      accountCreation,
      email,
      // showDropDown,
      isValidPassword,
      date,
      showDatePicker,
      overlayDrop,
    } = this.state;

    return (
      <Fragment>
        <span
          className={
            overlayDrop === true ? "screen-overlay show" : "screen-overlay"
          }
          onClick={this.handleOverlay}
        ></span>
        <Header {...this.props} />
        <div>
          <div className="container">
            <div className="row">
              <div className="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                <div className="signin-container">
                  <div className="title">
                    <h4>Getting Started</h4>
                    <h6>Create an account to continue!</h6>
                  </div>
                  <div className="form-container">
                    <form onSubmit={this.onSubmit}>
                      <div className="form-row">
                        <div className="form-group mb-4 col-md-6 position-relative">
                          <label>First Name</label>
                          <input
                            type="text"
                            className="form-control"
                            name="fname"
                            id="firstName"
                            value={fname}
                            onChange={this.onChangeHandler}
                            placeholder="First Name"
                            autoComplete="off"
                            // className={isErrors ? "error" : ""}
                          />
                          {/* {fname && (
                            <i
                              className="fa fa-check position-absolute"
                              aria-hidden="true"
                            ></i>
                          )} */}
                          <span
                            className={
                              this.state.isErrors
                                ? "errors error-message"
                                : "error-message"
                            }
                          >
                            {this.onDisplayError("firstname")}
                          </span>
                        </div>
                        <div className="form-group mb-4 col-md-6 position-relative">
                          <label>Last Name</label>
                          <input
                            type="text"
                            className="form-control"
                            id="lastName"
                            name="lname"
                            value={lname}
                            onChange={this.onChangeHandler}
                            placeholder="Last Name"
                            autoComplete="off"
                          />
                          {/* {lname && (
                            <i
                              className="fa fa-check position-absolute"
                              aria-hidden="true"
                            ></i>
                          )} */}
                          <span
                            className={
                              this.state.isErrors
                                ? "errors error-message"
                                : "error-message"
                            }
                          >
                            {this.onDisplayError("lastname")}
                          </span>
                        </div>
                      </div>
                      <div className="form-group mb-4 position-relative">
                        <label>Email</label>
                        <input
                          type="email"
                          className="form-control"
                          id="email"
                          value={email}
                          name="email"
                          onChange={this.onChangeHandler}
                          placeholder="Email"
                          autoComplete="off"
                        />
                        {/* {email && (
                          <i
                            className="fa fa-check position-absolute"
                            aria-hidden="true"
                          ></i>
                        )} */}
                        <span
                          className={
                            this.state.isErrors
                              ? "errors error-message"
                              : "error-message"
                          }
                        >
                          {this.onDisplayError("email")}
                        </span>
                      </div>
                      <div className="form-group position-relative">
                        {/* <i className="fa fa-mobile position-absolute" aria-hidden="true"></i> */}
                        <img
                          src={PhoneIcon}
                          alt=""
                          className="position-absolute right-icon"
                        />
                        <input
                          className="form-control px-5"
                          id="phoneNumber"
                          name="phoneNumber"
                          value={phone_number}
                          disabled
                          onChange={this.onChangeHandler}
                          placeholder="Phone Number"
                        />
                        <img src={CountryImage} alt="" />
                        <span
                          className={
                            this.state.isErrors
                              ? "errors error-message"
                              : "error-message"
                          }
                        >
                          {this.onDisplayError("phone_number")}
                        </span>
                      </div>
                      <div className="form-group mb-4 position-relative">
                        <label>Password</label>
                        {/* <PasswordStrengthBar password={password} /> */}

                        <div className="accordion" id="accordionExample">
                          <input
                            type="password"
                            id="password"
                            name="password"
                            className="form-control accordion"
                            value={password}
                            onChange={this.onChangeHandler}
                            onBlur={() =>
                              document
                                .getElementById("collapseOne")
                                .classList.remove("show")
                            }
                            onFocus={() =>
                              document
                                .getElementById("collapseOne")
                                .classList.add("show")
                            }
                            placeholder="Password"
                            autoComplete="off"
                          />

                          <div
                            id="collapseOne"
                            className="collapse"
                            aria-labelledby="headingOne"
                            data-parent="#accordionExample"
                          >
                            <PasswordStrengthBar
                              className="password_strength_bar"
                              password={password}
                              minLength={8}
                            />
                            <div className="card">
                              <PasswordChecklist
                                rules={[
                                  "minLength",
                                  "specialChar",
                                  "number",
                                  "capital",
                                ]}
                                minLength={8}
                                value={password}
                                onChange={(isValid) => {
                                  this.setState({
                                    isValidPassword: isValid,
                                  });
                                }}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-row">
                        <div className="form-group select-gender position-relative  mb-4 col-md-6">
                          <label>Date of Birth</label>

                          <input
                            type="text"
                            className="form-control pr-5"
                            id="fname"
                            name="handleOverlay"
                            value={date}
                            onChange={this.onChangeHandler}
                            placeholder="Select Date"
                            autoComplete="off"
                            onFocus={this.handleShowDatePicker}
                          />
                          <img
                            src={CalendarIcon}
                            alt=""
                            className="position-absolute calendar-icon"
                          />
                        </div>
                        <div className="form-group select-gender mb-4 col-md-6 position-relative">
                          <label>Gender</label>
                          <select
                            className=""
                            name="gender"
                            onChange={this.onChangeHandler}
                          >
                            <option className="custom-dropdown-input">
                              Select Gender
                            </option>
                            <option
                              value="men"
                              className="custom-dropdown-input"
                            >
                              Male
                            </option>
                            <option
                              value="women"
                              className="custom-dropdown-input"
                            >
                              Female
                            </option>
                          </select>
                          {/* <div className="filter-service custom-dropdown">
                          <div
                            className="custom-dropdown-input"
                            onClick={this.onHandleDropdown}
                          >
                            Male
                          </div>
                          <div
                            className={
                              showDropDown
                                ? "filter-actions"
                                : "filter-actions d-none"
                            }
                          >
                            <h5>Gender</h5>
                            <ul>
                              <li
                                className="active"
                                onClick={() => this.onHandleDropdown("men")}
                              >
                                Male
                              </li>
                              <li
                                onClick={() => this.onHandleDropdown("women")}
                              >
                                Female
                              </li>
                            </ul>
                          </div>
                        </div> */}
                          <i
                            className="fa fa-chevron-down"
                            aria-hidden="true"
                          ></i>
                        </div>
                      </div>
                      <button
                        type="submit"
                        // onClick={this.onSubmit}
                        className="btn btn-primary"
                        disabled={isValidPassword !== 4}
                      >
                        Create Account
                      </button>
                    </form>
                  </div>
                  <h6>
                    Already have an account ? <Link to="/">Login</Link>
                  </h6>
                </div>
              </div>
            </div>
          </div>
        </div>
        {accountCreation && (
          <div className="upload-image-modal">
            <div className="modal-container">
              <div className="modal-header mt-2">
                <img src={InstacutsIcons} alt="" />
              </div>
              <div className="content my-4">
                <h5>Account Creation Successful</h5>
                <p>Now you can comfortably get Salon Services at Home!</p>
              </div>
            </div>
          </div>
        )}

        {showDatePicker && (
          <div>
            <Calendar
              onChange={(item) => {
                this.setState({
                  date: item,
                  showDatePicker: false,
                  overlayDrop: false,
                });
              }}
              date={date}
              maxDate={get20YearAGoDate()}
            />
            <div className="calender-close">
              <i
                className="fa fa-times"
                aria-hidden="true"
                onClick={this.handleOverlay}
              ></i>
            </div>
          </div>
        )}
        <Footer />
      </Fragment>
    );
  }
}

export default SignIn;
