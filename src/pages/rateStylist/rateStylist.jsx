import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import StarRatingComponent from "react-star-rating-component";
import Ratingbg from "../../assets/images/rating.png";
import confirm from "../../assets/images/confirmation.png";
import "./rateStylist.css";
import Header from "../../components/DynamicHeader/Header";

class RateStylish extends Component {
  state = {
    rating: 0,
    value: null,
    price: "",
  };

  onStarClick = (nextValue, prevValue, name) => {
    this.setState({ rating: nextValue });
  };

  handleKeypress = (event) => {
    this.setState({ value: event.target.value });
  };

  handleChange = (event) => {
    this.setState({ price: event.target.value });
  };

  render() {
    const { rating, price } = this.state;
    let length = this.state.value ? this.state.value.length : 0;

    return (
      <Fragment>
        <Header title="Order Cart" {...this.props} />
        <div className="container">
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/">Home</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to="/">Stylist</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to="/">Stylist Details</Link>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                Rate Your Stylist
              </li>
            </ol>
          </nav>
          <div className="row">
            <div className="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
              <div className="rating-header">
                <div className="rating-img position-relative">
                  <img src={Ratingbg} alt="" className="w-100 black-bg" />
                  <div className="rating-stylish position-absolute">
                    <div className="text-center pb-3 rating-stylist-img">
                      <img src={confirm} alt="" />
                    </div>
                    <h5>John Jacob</h5>
                    <p>Order Complete!</p>
                  </div>
                </div>
              </div>
              <div className="card mt-4">
                <div className="rating-content-header text-center">
                  <h4>Rate Your Stylist</h4>
                  <StarRatingComponent
                    name="rate1"
                    starCount={5}
                    value={rating}
                    onStarClick={this.onStarClick.bind(this)}
                    // renderStarIcon={() => <i className="" ></i>}
                    renderStarIcon={(index, value) => {
                      return (
                        <span>
                          <i
                            className={
                              index <= value ? "fa fa-star" : "fa fa-star-o"
                            }
                            aria-hidden="true"
                          />
                        </span>
                      );
                    }}
                    renderStarIconHalf={() => {
                      return (
                        <span>
                          <span style={{ position: "absolute" }}>
                            <i className="fa fa-star" />
                          </span>
                          <span>
                            <i className="fa fa-star-half-o" />
                          </span>
                        </span>
                      );
                    }}
                  />
                  <div className="position-relative w-75 m-auto">
                    <textarea
                      onChange={(event) => this.handleKeypress(event)}
                      className="mt-4 mb-3"
                      placeholder="Share your experience..."
                    >
                      {this.state.value}
                    </textarea>
                    <div className="position-absolute experience-text-length">
                      <span>{length}</span>
                    </div>
                  </div>
                </div>

                <div className="rating-content-header">
                  <h4>Tip Your Stylist</h4>
                  <p>Your Default Payment Method Will Be Charged</p>
                  <div className="tip-content">
                    <h6>Most Tipped</h6>
                    <div className="tip-price">
                      <ul className="list-unstyled d-flex justify-content-between">
                        <li>15%</li>
                        <li>20%</li>
                        <li>25%</li>
                      </ul>
                    </div>
                    <h6>Custom</h6>
                    <div className="custom-tip pb-5 mb-3 text-center position-relative">
                      <input
                        type="number"
                        onChange={this.handleChange}
                        value={price}
                        placeholder="Enter Amount"
                        className={price !== "" ? "blackbginput" : ""}
                      />
                      <i
                        className={
                          price !== ""
                            ? "whiteIcon fa fa-usd position-absolute"
                            : "fa fa-usd position-absolute"
                        }
                        aria-hidden="true"
                      ></i>
                    </div>
                  </div>
                  <button>Complete</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default RateStylish;
