import React, { Component, Fragment } from "react";
import SideBar from "../../components/Sidebar/Sidebar";
import SidebarHeader from "../../components/Sidebar/SidebarHeader/SidebarHeader";
import { getBookingAllList } from "../../api/booking.api";
import Loader from "../../components/Loader/Loader";
import windowSize from "react-window-size";

class HistoryBooking extends Component {
  state = {
    data: [],
    isLoader: true,
    toggleSidebar: false,
    active: "history",
  };

  componentDidMount = () => {
    this.fetchAllBookingsList();
    this.setSideBar();
    window.addEventListener("resize",this.setSideBar);
  };

  setSideBar = () => {
    let value = false;
    if(this.props.windowWidth < 991){
      value = true;
    }else{
      value = false;
    }
    this.setState({
      ...this.state,
      toggleSidebar: value,
    })
  }

  fetchAllBookingsList = async () => {
    try {
      this.setState({
        ...this.state,
        isLoader: true,
      });
      const { data } = await getBookingAllList(10, 1, "old");
      this.setState({
        ...this.state,
        data,
      });
    } catch (err) {
      console.log(err);
    } finally {
      this.setState({
        ...this.state,
        isLoader: false,
      });
    }
  };

  SideBarToggle = () => {
    this.setState({
      toggleSidebar: !this.state.toggleSidebar,
    });
  };

  componentWillUnmount = () => {
    window.removeEventListener('resize');
  }
  render() {
    const heading = "History",
      subHeading = "My Bookings";

    const { data, toggleSidebar, active, isLoader } = this.state;

    return (
      <Fragment>
        <div className="d-flex">
          <div className="">
            <SideBar
              SideBarToggle={this.SideBarToggle}
              active={active}
              toggleSidebar={toggleSidebar}
            />
          </div>
          <div className="sideBar-header">
            <SidebarHeader
              heading={heading}
              subHeading={subHeading}
              toggleSidebar={toggleSidebar}
              SideBarToggle={this.SideBarToggle}
            />
            {isLoader ? (
              <Loader />
            ) : (
              <div className="history-container">
                {data &&
                  data.map((item, index) => {
                    return (
                      <div
                        className="booking-list booking-history d-flex align-items-center justify-content-between py-4"
                        key={index}
                      >
                        <div className="booking-content d-flex align-items-center justify-content-between">
                          <div className="position-relative mr-4">
                            <img src={item.stylist_profile} alt="" />
                            <div className="reload-overlay">
                              <i
                                className="fa fa-refresh"
                                aria-hidden="true"
                              ></i>
                            </div>
                            <i
                              className="fa fa-circle mr-2 position-absolute"
                              aria-hidden="true"
                            ></i>
                          </div>
                          <div className="booking-type d-md-flex w-100 justify-content-between">
                            <h5>{item.order_number}</h5>
                            <p>On-Demand Order</p>
                          </div>
                        </div>
                        <div className="d-md-flex booking-content justify-content-between">
                          <div className="booking-date">
                            <h6>
                              {new Date(parseInt(item.date))
                                .toDateString()
                                .toString()}
                            </h6>
                          </div>
                          <div className="myaccount-action booking-completed d-flex justify-content-end">
                            <button>
                              COMPLETED
                              <i
                                className="fa fa-angle-right pl-3"
                                aria-hidden="true"
                              ></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    );
                  })}
              </div>
            )}
          </div>
        </div>
      </Fragment>
    );
  }
}

export default windowSize(HistoryBooking);
