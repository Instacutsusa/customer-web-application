import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { connect } from "react-redux";

import ServicesCard from "../../components/Services/ServicesCard/ServicesCard";

import "./stylistDetails.css";
import Banner from "../../assets/images/banner.png";
import Profile from "../../assets/images/profile1.png";
import FilterIcon from "../../assets/images/icon.png";
import stylistAvatar from "../../assets/images/stylist-avatar.svg";

import Header from "../../components/DynamicHeader/Header";
import CheckIcon from "../../assets/icons/check-circle.svg";
import {
  addStylistFromFav,
  getStylistDetails,
  getStylistServices,
  removeStylistFromFav,
} from "../../api/stylist.api";
import { NotificationAlert } from "../../common/NotificationAlert";
import {
  NOTIFICATIONSMESSAGE,
  NOTIFICATION_TYPE,
} from "../../_constant/notificationsMessage";
import Loader from "../../components/Loader/Loader";
import StylistReview from "../stylist/StylistReview";
import StylistPortfolio from "../stylist/StylistPortfolio";

import PaginationBlock from "../../components/Pagination/PaginationBlock";
import { config } from "../../config/config";

const tempServiceDataArray = [...new Array(100)].map((ele, index) => {
  return {
    id: index + 1,
    rating: `${index + 1} fabulous`,
    ...ele,
  };
});
class StylistDetails extends Component {
  state = {
    services: {},
    tempServicesData: [],
    noServiceText: "No Services Available In your Location",
    differentService: "Try Different Service or Change Your Chosen Location",
    quantity: 0,
    showFilter: false,
    stylist_id: "",
    profile_id: "",
    stylisDetail: {},
    isLoader: true,
    isFavLoader: false,
    currentPage: 1,
  };

  componentDidMount = () => {
    const { user_profile } = this.props;
    const params = this.props.match.params;
    const stylist_id = params.stylist_id;
    const user = user_profile.find((user) => {
      return user.default_profile === true;
    });
    if (user) {
      const profile_id = user ? user._id : user_profile[0]?._id;
      this.setState({
        ...this.state,
        stylist_id: stylist_id,
        stylistId: stylist_id,
        profile_id,
      });

      this.fetchStylistDetail(stylist_id, user._id);
      this.fetchStylistServices(stylist_id, profile_id);

      this.setCurrentPage(1);
    }
  };

  // Fetch stylist Services
  fetchStylistServices = async (stylist_id, profile_id) => {
    try {
      const apiBody = {
        stylist_id,
        stylist_level: "advanced",
        profile_id,
        country: "United States",
        state_code: "TX",
        page: 1,
        limit: 10,
      };
      const { data } = await getStylistServices(apiBody);
      this.setState({
        ...this.state,
        services: data,
      });
    } catch (err) {
      console.log(err);
    }
  };

  // fetch Stylist details
  fetchStylistDetail = async (stylist_id, profile_id) => {
    try {
      const apiBody = {
        stylist_id,
        profile_id,
      };
      const { data } = await getStylistDetails(apiBody);
      this.setState({
        ...this.state,
        stylisDetail: data[0],
      });
    } catch (err) {
      NotificationAlert(
        NOTIFICATIONSMESSAGE.API_ERROR,
        NOTIFICATION_TYPE.ERROR
      );
      console.log(err);
    } finally {
      this.setState({
        ...this.state,
        isLoader: false,
        isFavLoader: false,
      });
    }
  };

  handleShowFilter = () => {
    this.setState((prevState) => ({
      showFilter: !prevState.showFilter,
    }));
  };

  handleQuantity = () => {
    this.setState({
      quantity: this.state.quantity + 1,
    });
  };

  onFavStylisthandler = async (is_fav) => {
    try {
      this.setState({
        ...this.state,
        isFavLoader: true,
      });
      const { profile_id, stylistId } = this.state;
      const user = JSON.parse(localStorage.getItem("customer_info"));
      const apiBody = { stylist_id: stylistId, user_id: user._id };
      if (is_fav) await removeStylistFromFav(apiBody);
      else await addStylistFromFav(apiBody);
      await this.fetchStylistDetail(stylistId, profile_id);
    } catch (err) {
      console.log("Error", err);
      NotificationAlert(
        NOTIFICATIONSMESSAGE.API_ERROR,
        NOTIFICATION_TYPE.ERROR
      );
    } finally {
      this.setState({
        ...this.state,
        isFavLoader: false,
      });
    }
  };

  setCurrentPage = (page) => {
    let offset = (page - 1) * 9;
    const currentPageData = tempServiceDataArray.slice(offset, offset + 9);
    this.setState({
      ...this.state,
      currentPage: page,
      tempservicedata: currentPageData,
    });
  };

  render() {
    const {
      services,
      quantity,
      showFilter,
      stylisDetail,
      isLoader,
      isFavLoader,
      currentPage,
      tempservicedata,
    } = this.state;

    if (isLoader) return <Loader />;

    return (
      <Fragment>
        <Header {...this.props} />
        <div className="container pb-5 stylist-detail-container">
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/">Home</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to="/stylist">Stylist</Link>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                Stylist Details
              </li>
            </ol>
          </nav>
          <div className="row">
            <div className="col-md-12 banner-container-padding">
              <div className="banner-container">
                <div className="banner-img">
                  <img src={Banner} alt="" />
                </div>
              </div>
            </div>
          </div>
          <div className="row profile-details-container">
            <div className="col-md-12 stylist-detail-header">
              <div className="stylist-profile-image position-relative">
                <img
                  src={
                    !stylisDetail.profile.includes("https")
                      ? `${
                          config.IMAGE_URL +
                          "/" +
                          config.STYLIST_DETAIL_PROFILE_DIAMENTION +
                          "/" +
                          stylisDetail.profile
                        }`
                      : stylistAvatar
                  }
                  alt=""
                />
                {stylisDetail?.online ? (
                  <i
                    className="fa fa-circle mr-2 position-absolute"
                    aria-hidden="true"
                  ></i>
                ) : (
                  <i
                    className="fa fa-circle mr-2 position-absolute text-secondary"
                    aria-hidden="true"
                  ></i>
                )}
              </div>
              <div
                className="stylist-fav-icon"
                role="button"
                onClick={() => this.onFavStylisthandler(stylisDetail.is_fav)}
              >
                {isFavLoader ? (
                  <i className="fa fa-spinner fa-spin"></i>
                ) : (
                  <i
                    className={`fa fa-heart ${
                      stylisDetail.is_fav ? "fav-stylist" : "unfav-stylist"
                    }`}
                    aria-hidden="true"
                  ></i>
                )}
              </div>
            </div>
            <div className="col-lg-5 col-md-6">
              <div className="stylist-detail">
                <h4>{stylisDetail.full_name}</h4>
                <h6>
                  {stylisDetail?.experience?.charAt(0).toUpperCase() +
                    stylisDetail?.experience?.slice(1)}{" "}
                  Stylist
                  {/* <i className="fa fa-check-square-o mx-2" aria-hidden="true"></i> */}
                  <img src={CheckIcon} alt="" className="mx-2" />
                  <i className="fa fa-star-o mr-2" aria-hidden="true"></i>{" "}
                  {Math.round(stylisDetail.avg_rating * 100) / 100} (
                  {stylisDetail.rating_count})
                </h6>
              </div>
            </div>
            <div className="col-lg-7 col-md-6">
              <div className="stylist-skills">
                <h5>Skills</h5>
                <div className="skills-list">
                  {stylisDetail?.skills?.map((skill, index) => {
                    return (
                      <span className="badge badge-secondary" key={index}>
                        {skill}
                      </span>
                    );
                  })}
                </div>
              </div>
            </div>
            <div className="col-md-12">
              <div className="stylist-detail-text pt-md-5 pb-md-4 pb-3">
                <p>{stylisDetail.about}</p>
              </div>
            </div>
          </div>
          <div className="row position-relative">
            <div className="col-md-12">
              <Tabs>
                <TabList className="react-tabs__tab-list detail-stylist-tabs">
                  <Tab className="react-tabs__tab detail-stylist-tab">
                    Services
                  </Tab>
                  <Tab className="react-tabs__tab detail-stylist-tab">
                    Portfolio
                  </Tab>
                  <Tab className="react-tabs__tab detail-stylist-tab">
                    Reviews ({stylisDetail.rating_count})
                  </Tab>
                </TabList>
                <TabPanel>
                  {/* <div className="filter-service pr-4">
                    show all{" "}
                    <img
                      src={FilterIcon}
                      alt=""
                      className="ml-2"
                      onClick={this.handleShowFilter}
                      onBlur={() => this.setState({ showFilter: false })}
                    />
                    <div
                      className={
                        showFilter ? "filter-actions" : "filter-actions d-none"
                      }
                    >
                      <h5>Short By</h5>
                      <ul>
                        <li className="active">Newest First</li>
                        <li>Highest First</li>
                        <li>Lowest First</li>
                      </ul>
                    </div>
                  </div> */}
                  <div className="row pt-4">
                    <ServicesCard
                      servicesData={tempservicedata}
                      // servicesData={services.result}
                      quantity={quantity}
                      handleQuantity={this.handleQuantity}
                    />
                    <PaginationBlock
                      currentPage={currentPage}
                      totalCount={tempServiceDataArray?.length}
                      pageSize={9}
                      onPageChange={(page) => this.setCurrentPage(page)}
                    />
                  </div>
                </TabPanel>
                <TabPanel>
                  <StylistPortfolio
                    portfolio_images={stylisDetail.portfolio_images}
                    portfolio_videos={stylisDetail.portfolio_videos}
                  />
                </TabPanel>
                <TabPanel>
                  <StylistReview
                    avg_rating={Math.round(stylisDetail.avg_rating * 100) / 100}
                    rating_count={stylisDetail.rating_count}
                    stylisDetail={stylisDetail}
                  />
                </TabPanel>
              </Tabs>
            </div>
          </div>
          <div
            className={
              quantity ? "service-cart-modal" : "service-cart-modal d-none"
            }
          >
            <div className="container d-flex justify-content-between align-items-center">
              <div className="d-flex">
                <div className="stylist-profile-image position-relative">
                  <img src={Profile} alt="" />
                  <i
                    className={`fa fa-circle mr-2 position-absolute ${
                      stylisDetail.online ? "online-stylist" : "offline-stylist"
                    }`}
                    aria-hidden="true"
                  ></i>
                </div>
                <div className="stylist-detail-modal ml-3">
                  <h4>Samantha William</h4>
                  <h6>
                    Junior Stylist
                    {/* <i className="fa fa-check-square-o mx-2" aria-hidden="true"></i>  */}
                    <img src={CheckIcon} alt="" className="mx-2" />
                    <i className="fa fa-star-o mr-2" aria-hidden="true"></i> 4.9
                    (74)
                  </h6>
                </div>
              </div>
              <div className="stylist-fav-icon d-flex">
                <div className="quantity-box mr-3">
                  <h5>
                    1 <span className="ml-3">$100</span>
                  </h5>
                </div>
                <Link to="">
                  Next{" "}
                  <i className="fa fa-long-arrow-right" aria-hidden="true"></i>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = ({ user }) => ({
  user_profile: user.user_profile,
});

export default connect(mapStateToProps)(StylistDetails);
