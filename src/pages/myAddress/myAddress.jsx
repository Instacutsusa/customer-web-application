import React, { Component, Fragment } from "react";
import SideBar from "../../components/Sidebar/Sidebar";
import SidebarHeader from "../../components/Sidebar/SidebarHeader/SidebarHeader";
import "./myAddress.css";
import LocationIcon from "../../assets/icons/locations.svg";
import {
  getListAddresses,
  onDeleteAddress,
  setActiveAddress,
} from "../../api/myAccounts.api";
import { NotificationAlert } from "../../common/NotificationAlert";
import {
  NOTIFICATIONSMESSAGE,
  NOTIFICATION_TYPE,
} from "../../_constant/notificationsMessage";
import { Link } from "react-router-dom";

class MyAccount extends Component {
  state = {
    data: [],
    toggleSidebar: false,
    active: "address",
  };

  componentDidMount = async () => {
    this.fetchAllAdressList();
  };

  /**
   * Used to fetch all listed address
   */
  fetchAllAdressList = async () => {
    const { data } = await getListAddresses();
    this.setState({
      data,
    });
  };

  SideBarToggle = () => {
    this.setState({
      toggleSidebar: !this.state.toggleSidebar,
    });
  };

  /**
   * Used to DELETE the address
   * @param {String} id - Id which we want to delete
   */
  onDeleteAddress = async (id) => {
    try {
      const apiBody = {
        address_id: id,
      };
      const { message } = await onDeleteAddress(apiBody);
      this.fetchAllAdressList();
      NotificationAlert(message, NOTIFICATION_TYPE.SUCCESS);
    } catch (err) {
      console.log(err);
    }
  };
  /**
   * Used to DELETE the address
   * @param {String} id - Id which we want to set as default address
   */
  onSetActiveLocation = async (id) => {
    try {
      const apiBody = { address_id: id, active: 1 };
      await setActiveAddress(apiBody);
      this.fetchAllAdressList();
      NotificationAlert(
        NOTIFICATIONSMESSAGE.SET_ADDRESS_ACTIVE,
        NOTIFICATION_TYPE.SUCCESS
      );
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const heading = "My Account",
      subHeading = "Addresses";

    const { data, toggleSidebar, active } = this.state;

    return (
      <Fragment>
        <div className="d-flex">
          <div className="">
            <SideBar
              SideBarToggle={this.SideBarToggle}
              active={active}
              toggleSidebar={toggleSidebar}
            />
          </div>
          <div className="sideBar-header">
            <SidebarHeader
              heading={heading}
              subHeading={subHeading}
              toggleSidebar={toggleSidebar}
              SideBarToggle={this.SideBarToggle}
            />
            <div className="myaccount-container">
              {data &&
                data.map((item, index) => {
                  return (
                    <div
                      className="myaccount-list d-flex align-items-center justify-content-between py-4"
                      key={index}
                    >
                      <div className="myaccount-content d-flex align-items-center w-50">
                        {/* <i className="fa fa-map-marker mr-4" aria-hidden="true"></i> */}
                        <img
                          src={LocationIcon}
                          alt=""
                          className="address-icon mr-4"
                        />
                        <div className="myaccount-address d-md-flex w-100">
                          <div className="w-md-50">
                            <Link
                              to={`address?edit=true&address_id=${item._id}`}
                              className="address-title"
                            >
                              {item.title}
                            </Link>
                          </div>
                          <p>{item.address}</p>
                        </div>
                      </div>
                      <div className="myaccount-action d-flex">
                        {item.active && (
                          <div className="myaccount-active">
                            <span>Active</span>
                          </div>
                        )}
                        {!item.active && (
                          <div
                            className="myaccount-inactive"
                            onClick={() => this.onSetActiveLocation(item._id)}
                          >
                            <span>Set as active</span>
                          </div>
                        )}
                        <button onClick={() => this.onDeleteAddress(item._id)}>
                          Delete{" "}
                          <i className="fa fa-times" aria-hidden="true"></i>
                        </button>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default MyAccount;
