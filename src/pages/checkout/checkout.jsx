import React, { Component, Fragment } from 'react';
import Header from '../../components/DynamicHeader/Header';
import PaymentMethod from '../../components/Payment/PaymentMethod/PaymentMethod';
import { MultiStepForm, Step } from 'react-multi-form';
import './checkout.css';
import PickSlot from '../../components/Payment/PickSlot/PickSlot';
import Footer from '../../components/Footer/Footer';

class Checkout extends Component {
    state={
        switchChecked: false,
        data: [
            {
                cardType: 'visa',
                selected: false
            },
            {
                cardType: 'visa',
                selected: true
            }
        ],
        showDetails: false,
        step: 1
    }

    getChckeboxValue = (event) => {
        this.setState({
            switchChecked: !this.state.switchChecked
        })
    }

    handleShowDetails = () => {
        this.setState({
            showDetails: !this.state.showDetails
        })
    }

    continueHandler =() => {
        this.setState({
            step: this.state.step+1
        })
    }

    render() {
        const { switchChecked, data, showDetails, step } = this.state;
        return (
            <Fragment>
                <Header title="checkout" {...this.props}/>
                <div className="container pb-5">
                    <div className="row">
                        <div className="col-md-12 custom-steps">
                            <MultiStepForm activeStep={step} accentColor="#000">
                                <Step label="Pick Slot" className="checkout-multistep">
                                    <PickSlot step={step} continueHandler={this.continueHandler}/>
                                </Step>
                                <Step label="Payment" className="checkoutd-multistep">
                                    <PaymentMethod 
                                        switchChecked={switchChecked} 
                                        data={data} 
                                        _getChckeboxValue={this.getChckeboxValue}
                                        showDetails={showDetails}
                                        _handleShowDetails={this.handleShowDetails}
                                    />
                                </Step>
                                <Step label="Checkout">
                                <p>Three</p>
                                </Step>
                            </MultiStepForm>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default Checkout;