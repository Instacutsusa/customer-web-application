import React, { Component, Fragment } from "react";
import OtpInput from "react-otp-input";
import Footer from "../../../components/Footer/Footer";
import Header from "../../../components/Header/Header";
import { Link } from "react-router-dom";
import "./otp.css";
import { customerLogin, customeVerifyOTP } from "../../../api/login/login.api";
import { NotificationAlert } from "../../../common/NotificationAlert";
import { NOTIFICATIONSMESSAGE } from "../../../_constant/notificationsMessage";

class OtpAuth extends Component {
  constructor() {
    super();
    this.state = {
      time: {},
      seconds: 4,
      otp: "",
      isErrors: false,
      isResendOtp: false,
      phoneNumber: "",
      country_code: "",
      isLoader: false,
    };
    this.timer = 0;
    this.startTimer = this.startTimer.bind(this);
    this.countDown = this.countDown.bind(this);
  }

  secondsToTime(secs) {
    let hours = Math.floor(secs / (60 * 60));

    let divisor_for_minutes = secs % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);

    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);

    let obj = {
      h: hours,
      m: minutes,
      s: seconds,
    };
    return obj;
  }

  componentDidMount() {
    this.startTimer();
    const { state } = this.props.location;

    if (
      !Object(state).hasOwnProperty("phoneNumber") ||
      !Object(state).hasOwnProperty("country_code")
    ) {
      return this.props.history.push("/");
    }

    this.setState({
      ...this.state,
      phoneNumber: state.phoneNumber,
      country_code: state.country_code,
    });

    let timeLeftVar = this.secondsToTime(this.state.seconds);
    this.setState({ time: timeLeftVar });
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  startTimer() {
    if (this.timer === 0 && this.state.seconds > 0) {
      this.timer = setInterval(this.countDown, 1000);
    }
  }

  countDown() {
    // Remove one second, set state so a re-render happens.
    let seconds = this.state.seconds - 1;
    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds,
    });

    // Check if we're at zero.
    if (seconds === 0) {
      this.setState({
        ...this.state,
        isResendOtp: true,
      });
      clearInterval(this.timer);
    }
  }

  onChangeHandler = (otp) => {
    this.setState({
      otp,
    });
  };

  /**
   * Used to verify customer OTP
   */
  onSubmit = async () => {
    const { country_code, phoneNumber, otp } = this.state;
    this.setState({
      isLoader: true,
    });

    const apiBody = {
      phone_number: phoneNumber.replace(/[^\d]/g, "").substring(1),
      country_code: country_code,
      token: otp,
    };
    try {
      const { status, data } = await customeVerifyOTP(apiBody);
      localStorage.setItem("customer_info", JSON.stringify(data));
      localStorage.setItem("access_token", data.token);

      if (status === 200) {
        NotificationAlert(NOTIFICATIONSMESSAGE.OTP_VERIFY, "success");
        this.props.history.push("/home");
      }
    } catch (err) {
      NotificationAlert(NOTIFICATIONSMESSAGE.OTP_VERIFY_FAIL, "error");
    } finally {
      this.setState({
        ...this.state,
        isLoader: false,
      });
    }
  };

  /**
   * Used to resend otp to customer
   */
  onResendOtpHandler = async () => {
    const { phoneNumber, country_code } = this.state;
    this.setState(
      {
        ...this.state,
        otp: "",
        seconds: 30,
      },
      () => this.startTimer()
    );

    const apiBody = {
      phone_number: phoneNumber.replace(/[^0-9]/g, "").substring(1),
      country_code: country_code,
    };

    const { user_exists } = await customerLogin(apiBody);
    if (!user_exists)
      this.props.history.push({
        pathname: "/register",
        state: {
          phoneNumber: this.state.phoneNumber,
          country_code: country_code,
        },
      });
    NotificationAlert(NOTIFICATIONSMESSAGE.OTP_RESEND, "success");
  };

  render() {
    const { otp, isResendOtp, phoneNumber, country_code, isLoader } =
      this.state;
    return (
      <Fragment>
        <Header {...this.props} />
        <div>
          <div className="container">
            <div className="row">
              <div className="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                <div className="login-container">
                  <div className="title">
                    <h4>OTP Authentication</h4>
                    <h6>
                      An authentication code has been sent to <br />
                      {phoneNumber}
                    </h6>
                  </div>
                  <div className="timer">
                    {this.state.time.s !== 0 && <h3>0:{this.state.time.s}</h3>}
                  </div>
                  <div className="otp-container">
                    <OtpInput
                      value={otp}
                      onChange={this.onChangeHandler}
                      numInputs={6}
                      separator={<span>&nbsp;</span>}
                      className="otp-input"
                    />
                    <div className="d-flex justify-content-between mt-4">
                      <h6>
                        Login with
                        <Link
                          to={{
                            pathname: "/password",
                            state: {
                              phone_number: phoneNumber,
                              country_code,
                            },
                          }}
                        >
                          {" "}
                          Password
                        </Link>
                      </h6>
                      <fieldset disabled>
                        <h6
                          tabIndex="-1"
                          className={!isResendOtp ? "resend_link_disable" : ""}
                          onClick={this.onResendOtpHandler}
                        >
                          <Link tabIndex="-1" to="#">
                            Resend Code
                          </Link>
                        </h6>
                      </fieldset>
                    </div>
                    <button
                      onClick={this.onSubmit}
                      disabled={otp.length === 6 ? false : true}
                    >
                      {isLoader ? (
                        <i className="fa fa-spinner fa-spin"></i>
                      ) : (
                        <i
                          className="fa fa-long-arrow-right"
                          aria-hidden="true"
                        ></i>
                      )}{" "}
                      Verify
                    </button>
                  </div>
                  <h6>
                    By Signing up, you agree to our{" "}
                    <Link to="#">Terms and Conditions</Link>
                  </h6>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

export default OtpAuth;
