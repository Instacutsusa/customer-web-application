import React, { Component, Fragment } from "react";
import Footer from "../../../components/Footer/Footer";
import Header from "../../../components/Header/Header";
import { Link } from "react-router-dom";
import LockIcon from "../../../assets/icons/Icon-Lock.svg";
import ShowIcon from "../../../assets/icons/Show.svg";
import { customeLoginWithPassword } from "../../../api/login/login.api";
import { NotificationAlert } from "../../../common/NotificationAlert";

class Password extends Component {
  state = {
    password: "",
    isErrors: false,
    phoneNumber: "",
    country_code: "",
  };

  componentDidMount() {
    const { state } = this.props.location;
    this.setState({
      ...this.state,
      phoneNumber: state.phone_number,
      country_code: state.country_code,
    });
  }

  onChangeHandler = (e) => {
    this.setState({
      password: e.target.value,
    });
  };

  onSubmit = async (e) => {
    const { phoneNumber, country_code, password } = this.state;
    e.preventDefault();
    this.setState({
      isErrors: true,
    });

    try {
      const apiBody = {
        phone_number: phoneNumber.replace(/[^\d]/g, "").substring(1),
        country_code,
        password,
      };
      const { data, status } = await customeLoginWithPassword(apiBody);
      if (status === 200) {
        localStorage.setItem("customer_info", JSON.stringify(data));
        localStorage.setItem("access_token", data.token);
        this.props.history.push("/profile");
      }
    } catch (err) {
      const errorMsg = err.response.data.message;
      NotificationAlert(errorMsg, "error");
    }
  };

  render() {
    const { password, isErrors, phoneNumber, country_code } = this.state;
    return (
      <Fragment>
        <Header {...this.props}/>
        <div>
          <div className="container">
            <div className="row">
              <div className="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                <div className="login-container">
                  <div className="title">
                    <h4>Login</h4>
                    <h6>Enter Password to Sign in</h6>
                  </div>
                  <div className="form-container">
                    <form onSubmit={this.onSubmit}>
                      <div className="form-group position-relative password-field">
                        {/* <i className="fa fa-lock position-absolute" aria-hidden="true"></i> */}
                        <img
                          src={LockIcon}
                          alt=""
                          className="position-absolute right-icon"
                        />
                        <label>Password</label>
                        <input
                          type="password"
                          id="password"
                          name="password"
                          value={password}
                          onChange={this.onChangeHandler}
                          className={isErrors ? "error" : ""}
                          placeholder="Password"
                        />
                        <div className="showpassword">
                          {/* <i className="fa fa-eye position-absolute" aria-hidden="true"></i> */}
                          <img
                            src={ShowIcon}
                            alt=""
                            className="position-absolute eye-icon"
                          />
                        </div>
                      </div>
                      <button type="submit" disabled={!password}>
                        Proceed{" "}
                        <i
                          className="fa fa-long-arrow-right"
                          aria-hidden="true"
                        ></i>
                      </button>
                    </form>
                  </div>
                  <h6>
                    Login with{" "}
                    <Link
                      to={{
                        pathname: "/otp",
                        state: {
                          phoneNumber: phoneNumber,
                          country_code,
                        },
                      }}
                    >
                      OTP
                    </Link>
                  </h6>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

export default Password;
