import React, { Component, Fragment } from "react";

import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";
import { Link, withRouter } from "react-router-dom";
import CountryImage from "../../assets/images/map.png";
import PhoneIcon from "../../assets/icons/Smartphone.svg";
import { customerLogin } from "../../api/login/login.api";

class Login extends Component {
  state = {
    phoneNumber: "",
    country_code: "+91",
    isErrors: false,
    redirect: false,
    isLoader: false,
  };

  onChangeHandler = (e) => {
    this.setState({
      phoneNumber: e.target.value,
    });
  };

  onSubmit = async (e) => {
    const { phoneNumber, country_code } = this.state;
    e.preventDefault();
    this.setState({
      isErrors: true,
      isLoader: true,
    });

    try {
      const phone_number = phoneNumber.replace(/[^0-9]/g, "").substring(1);
      const apiBody = {
        phone_number: phone_number,
        country_code: country_code,
      };
      const { user_exists, status } = await customerLogin(apiBody);

      if (!user_exists)
        this.props.history.push({
          pathname: "/register",
          state: {
            phoneNumber: this.state.phoneNumber,
            country_code: this.state.country_code,
          },
        });

      if (status === 200 && user_exists)
        this.props.history.push({
          pathname: "/otp",
          state: {
            phoneNumber: phoneNumber,
            country_code: this.state.country_code,
          },
        });
    } catch (err) {
      console.log(err);
    } finally {
      this.setState({
        ...this.state,
        isLoader: false,
      });
    }
  };

  // Formate phone number into US format
  formatPhoneNumber(value) {
    // if input value is falsy eg if the user deletes the input, then just return
    if (!value) return "+1" + value;

    // clean the input for any non-digit values.
    var phoneNumber = value.replace(/[^\d+]/g, "");

    // phoneNumberLength is used to know when to apply our formatting for the phone number
    const phoneNumberLength = phoneNumber.length;

    if (phoneNumberLength < 2) {
      return `+1 (${phoneNumber.slice(0, 2)})`;
    }

    if (phoneNumberLength <= 5) {
      // we need to return the value with no formatting if its less then four digits
      // this is to avoid weird behavior that occurs if you  format the area code to early
      return `+1 ${phoneNumber.slice(2, 5)}`;
    }

    // if phoneNumberLength is greater than 4 and less the 7 we start to return
    // the formatted number
    if (phoneNumberLength < 9) {
      return `+1 (${phoneNumber.slice(2, 5)}) ${phoneNumber.slice(5, 8)}`;
    }

    // finally, if the phoneNumberLength is greater then seven, we add the last
    // bit of formatting and return it.
    return `+1 (${phoneNumber.slice(2, 5)}) ${phoneNumber.slice(
      5,
      8
    )}-${phoneNumber.slice(8, 12)}`;
  }

  handleInput = (e) => {
    // this is where we'll call the phoneNumberFormatter function
    const formattedPhoneNumber = this.formatPhoneNumber(e.target.value);
    // we'll set the input value using our setInputValue

    this.setState({
      ...this.state,
      phoneNumber: formattedPhoneNumber,
    });
  };

  render() {
    const { phoneNumber, isLoader } = this.state;
    return (
      <Fragment>
        <Header {...this.props} />
        <div>
          <div className="container">
            <div className="row">
              <div className="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                <div className="login-container">
                  <div className="title">
                    <h4
                      onClick={() => {
                        this.props.history.push("/otp");
                      }}
                    >
                      Login
                    </h4>
                    <h6>Enter Phone Number to Register Or Sign In</h6>
                  </div>
                  <div className="form-container">
                    <form onSubmit={this.onSubmit}>
                      <div className="form-group position-relative">
                        {/* <i className="fa fa-mobile position-absolute" aria-hidden="true"></i> */}
                        <img
                          src={PhoneIcon}
                          alt=""
                          className="position-absolute right-icon"
                        />
                        <input
                          onChange={(e) => this.handleInput(e)}
                          onFocus={(e) => this.handleInput(e)}
                          value={phoneNumber}
                          // className={isErrors ? "error" : ""}
                          placeholder="Phone Number"
                        />

                        {/* <input
                          type="number"
                          name="phoneNumber"
                          value={phoneNumber}
                          onChange={this.onChangeHandler}
                          className={isErrors ? "error" : ""}
                          placeholder="Phone Number"
                        /> */}
                        <img src={CountryImage} alt="" />
                      </div>
                      {/* <span
                        className={
                          isErrors ? "errors error-message" : "error-message"
                        }
                      >
                        Phone number already registered!
                      </span> */}
                      <button type="submit" disabled={!phoneNumber}>
                        {isLoader && <i className="fa fa-spinner fa-spin"></i>}{" "}
                        Continue{" "}
                        <i
                          className="fa fa-long-arrow-right"
                          aria-hidden="true"
                        ></i>
                      </button>
                    </form>
                  </div>
                  <h6>
                    By Signing up, you agree to our{" "}
                    <Link to="#">Terms and Conditions</Link>
                  </h6>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

export default withRouter(Login);
