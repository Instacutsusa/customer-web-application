import React, { Component, Fragment } from "react";
import Modal from "react-bootstrap/Modal";
import Header from "../../components/DynamicHeader/Header";
import ProfileImg from "../../assets/images/profile.png";
import "./orderCart.css";
import Footer from "../../components/Footer/Footer";
import Ticket from "../../assets/images/Ticket.png";
import path from "../../assets/images/path.png";
import voucherImg from "../../assets/images/voucher.png";
import noVoucherImg from "../../assets/images/novoucher.png";
import FavStylist from "../../components/favStylist/favStylist";
import CancallationPolicy from "../../components/cancellationModal/cancellationPolicy";
import windowSize from "react-window-size";
import DropIcon from "../../assets/images/home-drop.png";
import { Dropdown } from "react-bootstrap";
import MapIcon from "../../assets/images/icon-map.png";
import MapsIcon from "../../assets/icons/map-icon.svg";
import DeleteIcon from "../../assets/icons/delete.svg";
import MinusIcon from "../../assets/icons/minus.svg";
import PlusIcon from "../../assets/icons/plus.svg";

class OrderCart extends Component {
  state = {
    data: [
      {
        name: "John Cena",
        services: [
          {
            name: "Women's Hair Cut",
            price: "$100",
            oldPrice: "$80",
          },
          {
            name: "Women's Hair Cut",
            price: "$100",
            oldPrice: "$80",
          },
        ],
      },
      {
        name: "John Cena",
        services: [
          {
            name: "Women's Hair Cut",
            price: "$100",
            oldPrice: "$80",
          },
          {
            name: "Women's Hair Cut",
            price: "$100",
            oldPrice: "$80",
          },
        ],
      },
    ],
    showDetails: false,
    isCancellationModalOpen: false,
    applyVoucher: false,
    removeVoucherModal: false,
    removeSelectionModal: false,
    overlayDrop: false,
  };

  handleShowDetails = () => {
    this.setState({
      showDetails: !this.state.showDetails,
    });
  };

  handleOverlay = () => {
    this.setState({
      overlayDrop: !this.state.overlayDrop,
    });
  };

  handleModal = (value, e) => {
    switch (value) {
      case "open":
        this.setState({
          isCancellationModalOpen: true,
          applyVoucher: false,
          removeVoucherModal: false,
          removeSelectionModal: false,
        });
        return;

      case "openVoucher":
        this.setState({
          isCancellationModalOpen: false,
          applyVoucher: true,
          removeSelectionModal: false,
          removeVoucherModal: false,
        });
        return;

      case "removeVoucher":
        this.setState({
          isCancellationModalOpen: false,
          applyVoucher: false,
          removeVoucherModal: true,
          removeSelectionModal: false,
        });
        return;

      case "removeSelection":
        this.setState({
          isCancellationModalOpen: false,
          applyVoucher: false,
          removeVoucherModal: false,
          removeSelectionModal: true,
        });
        return;

      case "close":
        this.setState({
          isCancellationModalOpen: false,
          applyVoucher: false,
          removeVoucherModal: false,
          removeSelectionModal: false,
        });
        return;

      default:
        return;
    }
  };

  render() {
    const {
      data,
      showDetails,
      isCancellationModalOpen,
      applyVoucher,
      removeVoucherModal,
      removeSelectionModal,
      overlayDrop,
    } = this.state;
    const { windowWidth } = this.props;

    return (
      <Fragment>
        <span
          className={
            overlayDrop === true ? "screen-overlay show" : "screen-overlay"
          }
          onClick={this.handleOverlay}
        ></span>
        <Header title="Order Cart" {...this.props} />
        <div className="container py-3 pt-md-0 pb-md-5">
          <div className="row">
            <div className="col-lg-8">
              <FavStylist />
              {windowWidth < 768 && (
                <div className="card">
                  <div className="order-details-card">
                    <div className="details-header mb-3">
                      <h4>Order Details</h4>
                    </div>
                    <div className="details-body">
                      <div className="detail-content d-flex justify-content-between my-2">
                        <h5>Location: </h5>
                        <h6 className="d-flex align-items-center">
                          Home{" "}
                          <Dropdown>
                            <Dropdown.Toggle>
                              <img
                                src={DropIcon}
                                alt=""
                                onClick={this.handleOverlay}
                                className="ml-2"
                              />
                            </Dropdown.Toggle>

                            <Dropdown.Menu className="address-dro-width">
                              <div className="location-dropdown">
                                <div className="location-dropdown-header">
                                  <div className="location-img text-center">
                                    <img src={MapIcon} alt="" />
                                  </div>
                                  <div className="location-text py-4">
                                    <h5>Select Your Location</h5>
                                  </div>
                                </div>
                                <div className="location-items">
                                  <div className="location-item mb-3 active">
                                    <h6>
                                      <img src={MapsIcon} alt="" />
                                      {/* <i className="fa fa-map-marker" aria-hidden="true"></i> */}
                                    </h6>
                                    <div className="location-address ml-2">
                                      <h5>Home</h5>
                                      <h6>7306 Madisyn Manors Suite 387</h6>
                                    </div>
                                    <div className="ml-5">
                                      <span className="badge badge-secondary location-badge">
                                        Active
                                      </span>
                                    </div>
                                  </div>
                                  <div className="location-item mb-3">
                                    <h6>
                                      <img src={MapsIcon} alt="" />
                                      {/* <i className="fa fa-map-marker" aria-hidden="true"> </i>*/}
                                    </h6>
                                    <div className="location-address ml-2">
                                      <h5>Work</h5>
                                      <h6>7306 Madisyn Manors Suite 387</h6>
                                    </div>
                                  </div>
                                  <button className="location-new-address">
                                    Add New Address
                                  </button>
                                </div>
                              </div>
                            </Dropdown.Menu>
                          </Dropdown>
                        </h6>
                      </div>
                      <div className="detail-content d-flex justify-content-between my-2">
                        <h5>Stylist: </h5>
                        <h6>Junior Stylist</h6>
                      </div>
                      <div className="detail-content d-flex justify-content-between my-2">
                        <h5>Service Category: </h5>
                        <h6>Home</h6>
                      </div>
                      <div className="detail-content d-flex justify-content-between my-2">
                        <h5>Preferred Stylist: </h5>
                        <h6>None</h6>
                      </div>
                    </div>
                  </div>
                </div>
              )}
              <div className="cart-content">
                {data &&
                  data.map((item, index) => {
                    return (
                      <div className="cart-content-list" key={index}>
                        <div className="cart-content-header">
                          <div className="profile-img position-relative">
                            <img src={ProfileImg} alt="" />
                            <i
                              className="fa fa-circle mr-2 position-absolute"
                              aria-hidden="true"
                            ></i>
                          </div>
                          <div className="cart-header-text">
                            <h5>{item.name}</h5>
                            <h6>Selected</h6>
                          </div>
                        </div>
                        <div className="order-details pt-4">
                          <div className="order-details-header">
                            <div className="row">
                              <div className="col-md-12">
                                <h6>Services</h6>
                              </div>
                            </div>
                          </div>
                          {item.services.map((service, index) => {
                            return (
                              <div
                                className="order-list d-flex justify-content-between align-items-baseline py-3"
                                key={index}
                              >
                                <div className="order-title">
                                  <h5>{service.name}</h5>
                                </div>
                                <div className="order-price">
                                  <h5>
                                    <span>$100</span> $80
                                  </h5>
                                </div>
                                <div className="order-quantity">
                                  <div className="counter">
                                    <img
                                      src={MinusIcon}
                                      alt=""
                                      className="mr-2"
                                    />
                                    {/* <i className="fa fa-minus mr-2" aria-hidden="true"></i> */}
                                    <span>1</span>
                                    <img
                                      src={PlusIcon}
                                      className="ml-2"
                                      alt=""
                                    />
                                    {/* <i className="fa fa-plus ml-2" aria-hidden="true"></i> */}
                                  </div>
                                </div>
                                {windowWidth > 767 && (
                                  <Fragment>
                                    <div className="order-price">
                                      <h5>$80</h5>
                                    </div>
                                    <div className="delete-icon">
                                      <img src={DeleteIcon} alt="" />
                                      {/* <i className="fa fa-trash-o" aria-hidden="true"></i> */}
                                    </div>
                                  </Fragment>
                                )}
                              </div>
                            );
                          })}
                          <div className="order-total-price">
                            <h4>
                              <span>Total Bill</span>$225
                            </h4>
                          </div>
                        </div>
                      </div>
                    );
                  })}
              </div>
            </div>
            <div className="col-lg-4">
              {windowWidth > 767 && (
                <div className="card">
                  <div className="order-details-card">
                    <div className="details-header mb-3">
                      <h4>Order Details</h4>
                    </div>
                    <div className="details-body">
                      <div className="detail-content d-flex justify-content-between my-2">
                        <h5>Location: </h5>
                        <h6 className="d-flex align-items-center">
                          Home{" "}
                          <Dropdown>
                            <Dropdown.Toggle>
                              <img
                                src={DropIcon}
                                alt=""
                                onClick={this.handleOverlay}
                                className="ml-2"
                              />
                            </Dropdown.Toggle>

                            <Dropdown.Menu className="address-dro-width">
                              <div className="location-dropdown">
                                <div className="location-dropdown-header">
                                  <div className="location-img text-center">
                                    <img src={MapIcon} alt="" />
                                  </div>
                                  <div className="location-text py-4">
                                    <h5>Select Your Location</h5>
                                  </div>
                                </div>
                                <div className="location-items">
                                  <div className="location-item mb-3 active">
                                    <h6>
                                      <img src={MapsIcon} alt="" />
                                      {/* <i className="fa fa-map-marker" aria-hidden="true"></i> */}
                                    </h6>
                                    <div className="location-address ml-2">
                                      <h5>Home</h5>
                                      <h6>7306 Madisyn Manors Suite 387</h6>
                                    </div>
                                    <div className="ml-5">
                                      <span className="badge badge-secondary location-badge">
                                        Active
                                      </span>
                                    </div>
                                  </div>
                                  <div className="location-item mb-3">
                                    <h6>
                                      <img src={MapsIcon} alt="" />
                                      {/* <i className="fa fa-map-marker" aria-hidden="true"></i> */}
                                    </h6>
                                    <div className="location-address ml-2">
                                      <h5>Work</h5>
                                      <h6>7306 Madisyn Manors Suite 387</h6>
                                    </div>
                                  </div>
                                  <button className="location-new-address">
                                    Add New Address
                                  </button>
                                </div>
                              </div>
                            </Dropdown.Menu>
                          </Dropdown>
                        </h6>
                      </div>
                      <div className="detail-content d-flex justify-content-between my-2">
                        <h5>Stylist: </h5>
                        <h6>Junior Stylist</h6>
                      </div>
                      <div className="detail-content d-flex justify-content-between my-2">
                        <h5>Service Category: </h5>
                        <h6>Home</h6>
                      </div>
                      <div className="detail-content d-flex justify-content-between my-2">
                        <h5>Preferred Stylist: </h5>
                        <h6>None</h6>
                      </div>
                    </div>
                  </div>
                </div>
              )}
              <div className="card">
                <div className="voucher-card d-flex justify-content-between">
                  <div className="">
                    <img src={Ticket} alt="" />
                  </div>
                  <input type="text" placeholder="Vouchers" />
                  <button onClick={(e) => this.handleModal("openVoucher", e)}>
                    Apply
                  </button>
                </div>
              </div>
              <div className="card">
                <div className="order-bill-card">
                  <div className="details-header">
                    <h5>Total bill</h5>
                    <h3>$450</h3>
                  </div>
                  <div className="bill-details-body">
                    <div
                      className="bill-collapse"
                      onClick={this.handleShowDetails}
                    >
                      <h5>Bills Details</h5>
                      {showDetails === true ? (
                        <i className="fa fa-angle-up" aria-hidden="true"></i>
                      ) : (
                        <i className="fa fa-angle-down" aria-hidden="true"></i>
                      )}
                    </div>
                    {showDetails && (
                      <Fragment>
                        <div className="detail-content d-flex justify-content-between my-2">
                          <h5>Service Charges(4): </h5>
                          <h6>$410</h6>
                        </div>
                        <div className="detail-content d-flex justify-content-between my-2">
                          <h5>Convenience Fee: </h5>
                          <h6>$20</h6>
                        </div>
                        <div className="detail-content d-flex justify-content-between my-2">
                          <h5>Discount: </h5>
                          <h6>- $20</h6>
                        </div>
                        <div className="detail-content d-flex justify-content-between my-2">
                          <h5>Voucher(DX239 Applied) 99% OFF: </h5>
                          <h6>- $20</h6>
                        </div>
                        <div className="detail-content d-flex justify-content-between my-2">
                          <h5>Tax: </h5>
                          <h6>- $20</h6>
                        </div>
                      </Fragment>
                    )}
                  </div>
                  <CancallationPolicy
                    isCancellationModalOpen={isCancellationModalOpen}
                    handleModal={this.handleModal}
                  />
                  <div className="btn-container mt-4 d-flex fixed-mobile">
                    <button className="schedultBtn d-flex">
                      Schedule{" "}
                      <i className="fa fa-calendar ml-2" aria-hidden="true"></i>
                    </button>
                    <button className="orderBtn d-flex">
                      Order Now{" "}
                      <i
                        className="fa fa-arrow-right ml-2"
                        aria-hidden="true"
                      ></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal
          show={applyVoucher}
          onHide={(e) => this.handleModal("close", e)}
          className="voucher-modal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Select Voucher</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="voucher-content">
              <div className="voucher-input">
                <h6>Add Voucher</h6>
                <input type="text" placeholder="Enter Voucher Code" />
                <button>Use Voucher</button>
              </div>
              <div className="available-Voucher">
                <div className="voucher-list d-flex mt-4">
                  <div className="voucher-img position-relative">
                    <img src={voucherImg} alt="" />
                    <img src={path} alt="" className="path-icon" />
                  </div>
                  <div className="voucher-details">
                    <div className="voucher-content d-flex justify-content-between mb-2">
                      <h5>DX239</h5>
                      <h6>Use Voucher </h6>
                    </div>
                    <div className="voucher-text">
                      <p>OFF 99% - Min. $200 Services</p>
                    </div>
                    <div className="voucher-date d-flex justify-content-between">
                      <p>Valid Thru: 26.01.2021</p>
                      <h5>T&amp;C</h5>
                    </div>
                  </div>
                </div>
                <div className="voucher-list d-flex mt-4">
                  <div className="voucher-img position-relative">
                    <img src={voucherImg} alt="" />
                    <img src={path} alt="" className="path-icon" />
                  </div>
                  <div className="voucher-details">
                    <div className="voucher-content d-flex justify-content-between mb-2">
                      <h5>DX239</h5>
                      <h6>Use Voucher </h6>
                    </div>
                    <div className="voucher-text">
                      <p>OFF 99% - Min. $200 Services</p>
                    </div>
                    <div className="voucher-date d-flex justify-content-between">
                      <p>Valid Thru: 26.01.2021</p>
                      <h5>T&amp;C</h5>
                    </div>
                  </div>
                </div>
              </div>
              <div className="voucher-input mt-4">
                <h6>No Applicable</h6>
              </div>
              <div className="available-Voucher">
                <div className="voucher-list d-flex mt-4">
                  <div className="voucher-img position-relative">
                    <img src={noVoucherImg} alt="" />
                    <img src={path} alt="" className="path-icon" />
                  </div>
                  <div className="voucher-details">
                    <div className="voucher-content d-flex justify-content-between mb-2">
                      <h5>DX239</h5>
                      <h6>Use Voucher </h6>
                    </div>
                    <div className="voucher-text">
                      <p>OFF 99% - Min. $200 Services</p>
                    </div>
                    <div className="voucher-date d-flex justify-content-between">
                      <p>Valid Thru: 26.01.2021</p>
                      <h5>T&amp;C</h5>
                    </div>
                  </div>
                </div>
                <div className="voucher-list d-flex mt-4">
                  <div className="voucher-img position-relative">
                    <img src={noVoucherImg} alt="" />
                    <img src={path} alt="" className="path-icon" />
                  </div>
                  <div className="voucher-details">
                    <div className="voucher-content d-flex justify-content-between mb-2">
                      <h5>DX239</h5>
                      <h6>Use Voucher </h6>
                    </div>
                    <div className="voucher-text">
                      <p>OFF 99% - Min. $200 Services</p>
                    </div>
                    <div className="voucher-date d-flex justify-content-between">
                      <p>Valid Thru: 26.01.2021</p>
                      <h5>T&amp;C</h5>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Modal.Body>
        </Modal>
        <Modal
          show={removeVoucherModal}
          onHide={(e) => this.handleModal("close", e)}
          className="remove-voucher-modal"
          scrollable={true}
        >
          <Modal.Header>
            <Modal.Title>Remove Voucher?</Modal.Title>
          </Modal.Header>
          <Modal.Body></Modal.Body>
          <Modal.Footer>
            <h5 onClick={(e) => this.handleModal("close", e)}>Discard</h5>
            <h6 onClick={(e) => this.handleModal("close", e)}>Remove</h6>
          </Modal.Footer>
        </Modal>
        <Modal
          show={removeSelectionModal}
          onHide={(e) => this.handleModal("close", e)}
          className="remove-voucher-modal"
          scrollable={true}
        >
          <Modal.Header>
            <Modal.Title>Clear Your Selections?</Modal.Title>
          </Modal.Header>
          <Modal.Body></Modal.Body>
          <Modal.Footer>
            <h5 onClick={(e) => this.handleModal("close", e)}>Discard</h5>
            <h6 onClick={(e) => this.handleModal("close", e)}>Remove</h6>
          </Modal.Footer>
        </Modal>
      </Fragment>
    );
  }
}

export default windowSize(OrderCart);
