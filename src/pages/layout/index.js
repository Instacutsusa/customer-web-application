import React, { Component, Fragment } from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import SideBar from '../../components/Sidebar/Sidebar';
import Rewards from '../../pages/reward/reward';
import UserProfile from '../../pages/userProfile/userProfile';
import Booking from '../../pages/booking/booking';
import HistoryBooking from '../../pages/history/history';
import MyAccount from '../../pages/myAddress/myAddress';
import PrefferStylist from '../../pages/preferredStylist/stylist';
import Payment from '../../pages/payment/Payment';

class Layout extends Component {
    render() {
        return (
            <Fragment>
                <SideBar />
                    <Router>
                        <Switch>
                            <Route path="/rewards" component={Rewards}/>
                            <Route path="/user" component={UserProfile}/>
                            <Route path="/booking" component={Booking}/>
                            <Route path="/history" component={HistoryBooking}/>
                            <Route path="/myaccount" component={MyAccount}/>
                            <Route path="/stylists" component={PrefferStylist}/>
                            <Route path="/payment" component={Payment}/>
                        </Switch>
                    </Router>
            </Fragment>
        );
    }
}

export default Layout;