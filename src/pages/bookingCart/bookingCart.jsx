import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import Footer from "../../components/Footer/Footer";
import CancallationPolicy from "../../components/cancellationModal/cancellationPolicy";
import confirm from "../../assets/images/confirmation.png";
import "./bookingCart.css";
import Header from "../../components/DynamicHeader/Header";
import CancelIcon from "../../assets/icons/cancel-icon.svg";
import FileIcon from "../../assets/icons/file-icon.svg";
import RefreshIcon from "../../assets/icons/refresh-icon.svg";
import CallIcon from "../../assets/icons/call-icon.svg";

class BookingCart extends Component {
  state = {
    data: [
      {
        name: "Daniel",
      },
      {
        name: "John",
      },
    ],
    showDetails: false,
    isCancellationModalOpen: false,
    pending: false,
    schedule: false,
    active: false,
    cancelled: false,
    scheduled: true,
  };

  handleShowDetails = () => {
    this.setState({
      showDetails: !this.state.showDetails,
    });
  };

  handleModal = (value, e) => {
    switch (value) {
      case "open":
        this.setState({
          isCancellationModalOpen: true,
        });
        return;

      case "close":
        this.setState({
          isCancellationModalOpen: false,
        });
        return;

      default:
        return;
    }
  };

  render() {
    const {
      data,
      showDetails,
      isCancellationModalOpen,
      pending,
      schedule,
      active,
      cancelled,
      scheduled,
    } = this.state;

    return (
      <Fragment>
        <Header title="Order Cart" {...this.props} />
        <div className="container pb-5">
          <nav
            aria-label="breadcrumb"
            className="d-flex justify-content-between align-items-center mt-md-4"
          >
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/">Home</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to="/">Stylist</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to="/">Stylist Details</Link>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                Order-#HST-B-1034
              </li>
            </ol>
            <Link to="#" className="help-link">
              Help
            </Link>
          </nav>
          <div className="row">
            <div className="col-md-7">
              <div className="card">
                <div className="fav-stylist-header d-flex align-items-center justify-content-between">
                  <h5>Status</h5>
                  {pending === true && (
                    <div className="d-flex align-items-center order-status pending">
                      <span>-</span>
                      <h4>Pending</h4>
                    </div>
                  )}
                  {active && (
                    <div className="d-flex align-items-center order-status active">
                      <span>-</span>
                      <h4>Active</h4>
                    </div>
                  )}
                  {scheduled && (
                    <div className="d-flex align-items-center order-status scheduled">
                      <span>-</span>
                      <h4>Scheduled</h4>
                    </div>
                  )}
                  {active && (
                    <div className="d-flex align-items-center order-status completed">
                      <span>-</span>
                      <h4>Completed</h4>
                    </div>
                  )}
                  {cancelled && (
                    <div className="d-flex align-items-center order-status cancelled">
                      <span>-</span>
                      <h4>Cancelled</h4>
                    </div>
                  )}
                </div>
              </div>
              <div className="card">
                <div className="booking-card-header mb-4">
                  <h6>Assigned Stylist</h6>
                </div>
                {schedule === true && (
                  <div className="booking-card-confirmation d-flex justify-content-between align-items-center">
                    <div className="confirmation-status d-flex align-items-center">
                      <img src={confirm} alt=" " />
                      <div className="confirmation-text ml-3">
                        <h5>Awaiting Confirmation</h5>
                        <p>Request Sent to Nearby Stylists</p>
                      </div>
                    </div>
                    <div className="confirmation-time">
                      <h6>
                        <span>Expires In:</span> 13:49 Mins
                      </h6>
                    </div>
                  </div>
                )}

                {pending && (
                  <div className="booking-card-confirmation d-flex justify-content-between align-items-center">
                    <div className="confirmation-status d-flex align-items-center">
                      <div className="reschedule-booking">
                        <div className="reschedule-text">
                          <p>
                            <i className="fa fa-refresh" aria-hidden="true"></i>{" "}
                            Retry
                          </p>
                        </div>
                      </div>
                      <div className="confirmation-text ml-3">
                        <h5>Request Expired</h5>
                        <p>Stylists are busy serving</p>
                      </div>
                    </div>
                    <div className="confirmation-time">
                      <h6>
                        <i className="fa fa-refresh" aria-hidden="true"></i>{" "}
                        Reschedule
                      </h6>
                    </div>
                  </div>
                )}

                {active ||
                  (scheduled && (
                    <Fragment>
                      <div className="booking-card-confirmation active-booking d-flex justify-content-between align-items-center">
                        <div className="confirmation-status d-flex align-items-center">
                          <img src={confirm} alt=" " />
                          <div className="confirmation-text ml-3">
                            <h5>Kriston Watson</h5>
                            <p>
                              {scheduled
                                ? "Scheduled Booking"
                                : "Direct Booking"}
                            </p>
                          </div>
                        </div>
                        <div className="confirmation-time text-right">
                          {scheduled ? (
                            <span className="schedule-time">
                              Nov 05, 2020, 09:38 am
                            </span>
                          ) : (
                            <h6>ETA: 45 Mins</h6>
                          )}
                          <Link to="#" className="profile-link">
                            View Profile{" "}
                            <i
                              className="fa fa-chevron-right"
                              aria-hidden="true"
                            ></i>
                          </Link>
                        </div>
                      </div>
                      <div className="service-otp d-flex justify-content-between">
                        <h5>Service OTP</h5>
                        <h5>Show</h5>
                      </div>
                    </Fragment>
                  ))}
              </div>
              <div className="card">
                <div className="booking-card-header pb-4">
                  <h6>Services</h6>
                </div>
                {data.map((item, index) => {
                  return (
                    <div className="booking-cart-serives" key={index}>
                      <div className="cart-content-header border-0">
                        <div className="profile-img position-relative">
                          <img
                            src="/static/media/profile.57ee9545.png"
                            alt=""
                          />
                          <i
                            className="fa fa-circle mr-2 position-absolute"
                            aria-hidden="true"
                          ></i>
                        </div>
                        <div className="cart-header-text">
                          <h5>{item.name}</h5>
                          <h6>Select</h6>
                        </div>
                      </div>
                      <div className="booking-cart-items-detail d-flex justify-content-between align-items-center pb-3">
                        <div className="booking-item-title">
                          <h5>Women's Hair Blowout x 1</h5>
                        </div>
                        <div className="booking-item-price">
                          <h5>$80</h5>
                        </div>
                      </div>
                      <div className="booking-cart-items-detail d-flex justify-content-between align-items-center pb-3">
                        <div className="booking-item-title">
                          <h5>Women's Hair Blowout x 1</h5>
                        </div>
                        <div className="booking-item-price">
                          <h5>$80</h5>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
            <div className="col-md-5">
              <div className="card">
                <div className="booking-card-header">
                  <h6 className="pb-1">Payment Methods</h6>
                  <div className="detail-content d-flex justify-content-between mt-2">
                    <h5>Wallet </h5>
                    <h6>$0</h6>
                  </div>
                  <div className="detail-content d-flex justify-content-between mt-2">
                    <h5>Card Ending with ****3995 </h5>
                    <h6>$300</h6>
                  </div>
                </div>
              </div>
              <div className="card">
                <div className="booking-card-header">
                  <h6 className="pb-1">Service Location</h6>
                  <div className="detail-content d-flex justify-content-between mt-2">
                    <h5>474 ottawa Ave, Hasbrouck Heights, NJ, 07604</h5>
                  </div>
                </div>
              </div>
              <div className="card">
                <div className="order-bill-card">
                  <div className="details-header">
                    <h5>Total paid</h5>
                    <h3>$450</h3>
                  </div>
                  <div className="bill-details-body">
                    <div
                      className="bill-collapse"
                      onClick={this.handleShowDetails}
                    >
                      <h5>Invoice Details</h5>
                      {showDetails === true ? (
                        <i className="fa fa-angle-up" aria-hidden="true"></i>
                      ) : (
                        <i className="fa fa-angle-down" aria-hidden="true"></i>
                      )}
                    </div>
                    {showDetails && (
                      <Fragment>
                        <div className="detail-content d-flex justify-content-between my-2">
                          <h5>Service Charges(4): </h5>
                          <h6>$410</h6>
                        </div>
                        <div className="detail-content d-flex justify-content-between my-2">
                          <h5>Convenience Fee: </h5>
                          <h6>$20</h6>
                        </div>
                        <div className="detail-content d-flex justify-content-between my-2">
                          <h5>Discount: </h5>
                          <h6>- $20</h6>
                        </div>
                        <div className="detail-content d-flex justify-content-between my-2">
                          <h5>Voucher(DX239 Applied) 99% OFF: </h5>
                          <h6>- $20</h6>
                        </div>
                        <div className="detail-content d-flex justify-content-between my-2">
                          <h5>Tax: </h5>
                          <h6>- $20</h6>
                        </div>
                      </Fragment>
                    )}
                  </div>
                  <CancallationPolicy
                    isCancellationModalOpen={isCancellationModalOpen}
                    handleModal={this.handleModal}
                  />
                </div>
              </div>
              <div className="card">
                <div className="booking-actions d-flex justify-content-between">
                  <div className="booking-action-item text-center">
                    {/* <i className="fa fa-window-close-o" aria-hidden="true"></i> */}
                    <img src={CancelIcon} alt="" />
                    <h5 className="mt-3">Cancel</h5>
                  </div>
                  <div className="booking-action-item text-center">
                    {/* <i className="fa fa-file-text-o" aria-hidden="true"></i> */}
                    <img src={FileIcon} alt="" />
                    <h5 className="mt-3">Details</h5>
                  </div>
                  <div className="booking-action-item text-center">
                    {/* <i className="fa fa-refresh" aria-hidden="true"></i> */}
                    <img src={RefreshIcon} alt="" />
                    <h5 className="mt-3">Reschedule</h5>
                  </div>
                  <div className="booking-action-item text-center">
                    {/* <i className="fa fa-volume-control-phone" aria-hidden="true"></i> */}
                    <img src={CallIcon} alt="" />
                    <h5 className="mt-3">Call Stylist</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default BookingCart;
