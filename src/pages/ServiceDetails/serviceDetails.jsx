import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import Banner from "../../assets/images/services.png";
import Safety from "../../assets/images/safety.png";
import "./servicesDetail.css";
import HappyCustomers from "../../components/HappyCustomers/HappyCustomers";
import Header from "../../components/DynamicHeader/Header";
import Slider from "react-slick";

class StylistDetails extends Component {
  state = {};

  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 200,
      arrows: true,
      autoplay: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
          },
        },
      ],
    };
    return (
      <Fragment>
        <Header {...this.props} />
        <div className="container pb-5">
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/">Home</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to="/stylist">Stylist</Link>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                Stylist Details
              </li>
            </ol>
          </nav>
          <div className="row">
            <div className="col-md-12">
              <div className="banner-container carousel-banner position-relative">
                {/* <Carousel showThumbs={false} showStatus={false}> */}
                <Slider {...settings} className="carousel slide ">
                  <div>
                    <div className="banner-img">
                      <img src={Banner} alt="" />
                    </div>
                  </div>
                  <div>
                    <div className="banner-img">
                      <img src={Banner} alt="" />
                    </div>
                  </div>
                </Slider>
                <div className="banner-card-content">
                  <div className="banner-card">
                    <h4>Women's Hair Cut</h4>
                    <h6>
                      Junior Stylist{" "}
                      <i className="fa fa-star-o mr-2" aria-hidden="true"></i>{" "}
                      4.9 (74)
                    </h6>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Nullam tempor erat at tortor blandit, eget blandit lectus
                      rhoncus. Aliquam gravida lorem tincidunt cursus
                      sollicitudin. Nulla lorem ipsum, ornare eget iaculis quis,
                      blandit et quam.
                    </p>
                  </div>
                  <div className="discount-card mt-3">
                    <div className="discount-card-header">
                      <h5>Save 10%</h5>
                    </div>
                    <div className="discount-card-body">
                      <h4>$60.00</h4>
                      <span>$90.00</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row service-details-customer-section">
            <div className="col-md-12">
              <HappyCustomers />
            </div>
            <div className="col-md-12">
              <div className="safety-banner position-relative">
                <div className="safety-image">
                  <img src={Safety} alt="" />
                </div>
                <div className="safety-content">
                  <p>Learn About Safety Protocols</p>
                  <i className="fa fa-chevron-right" aria-hidden="true"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default StylistDetails;
