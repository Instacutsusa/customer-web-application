import React, { Component, Fragment } from 'react';
import SideBar from '../../components/Sidebar/Sidebar';
import SidebarHeader from '../../components/Sidebar/SidebarHeader/SidebarHeader';
import Profile from '../../components/Profile/Profile';

class UserProfile extends Component {

    state={
        toggleSidebar: false,
        active: "profile"
    }

    SideBarToggle = () => {
        this.setState({
            toggleSidebar: !this.state.toggleSidebar
        })
    }

    render() {

        const heading = "Edit Profile",
              subHeading = "Manage your profile";
        const { toggleSidebar, active } = this.state;
        return(
            <Fragment>
                <div className="d-flex">
                    <div className="">
                        <SideBar SideBarToggle={this.SideBarToggle} active={active} toggleSidebar={toggleSidebar}/>
                    </div>
                    <div className="sideBar-header"> 
                        <SidebarHeader heading={heading} subHeading={subHeading} toggleSidebar={toggleSidebar} SideBarToggle={this.SideBarToggle}/>
                        <div className="row m-auto pt-2">
                            <Profile />
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default UserProfile;