import React, { Component, Fragment } from "react";
import Header from "../../components/Header/Header";
import Profile from "../../components/Profile/Profile";

class ProfilePage extends Component {
  render() {
    return (
      <Fragment>
        <Header {...this.props}/>
        <Profile />
      </Fragment>
    );
  }
}

export default ProfilePage;
