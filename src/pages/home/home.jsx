import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { getCustomerProfile } from '../../api/profile.api';
import { getAllInactiveStylist } from '../../api/stylist.api';
import Categories from '../../components/Categories/Categories';
import Header from '../../components/DynamicHeader/Header';
import ServicesList from '../../components/Services/ServicesList';
import ActiveStylist from '../../components/Stylist/stylist';
import { userProfile } from '../../redux/actions';

class Home extends Component {

    state={
        active: false
    }

    componentDidMount = async() => {
        const res = await getAllInactiveStylist();
        this.loadAllFamilyMember();
    }
    
    handleActiveCat = () => {
        this.setState(prevState => ({
            active: !prevState.active
        }))
    }

    loadAllFamilyMember = async () => {
        try {            
          const { data } = await getCustomerProfile();
          const profileData = [];
          let default_profile;
    
          data.map((profile) => {
            if (profile.default_profile) default_profile = profile.profile;
            return profileData.push({
              name: profile.firstname + " " + profile.lastname,
              accountHolder: profile.default_profile,
              selected: profile.default_profile,
              ...profile,
            });
          });

          this.props.dispatch(userProfile({ user_profile: profileData }));
        } catch (err) {
          console.log("err", err);
        }
      };

    render() {
        const { active } = this.state;
        return (
            <Fragment>
                <Header {...this.props} />
                <div className="container pb-5">
                    <Categories activeCat={active} handleActiveCat={this.handleActiveCat}/>
                    <ServicesList />
                    { active ? <ActiveStylist /> : " " }
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    state: state,
})

export default connect(mapStateToProps)(Home);