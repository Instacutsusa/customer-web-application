import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import {
  deletePrefferedStylist,
  getPrefferedStylistList,
} from "../../../api/stylist.api";
import { NotificationAlert } from "../../../common/NotificationAlert";

import Loader from "../../../components/Loader/Loader";
import {
  NOTIFICATIONSMESSAGE,
  NOTIFICATION_TYPE,
} from "../../../_constant/notificationsMessage";

class PrefferedStylist extends Component {
  state = {
    data: [],
    isLoader: true,
  };
  componentDidMount = () => {
    this.fetchPrefferedStylistList();
  };

  // FETCH ALL PREFFEED STYLIST
  fetchPrefferedStylistList = async () => {
    try {
      const user_id = JSON.parse(localStorage.getItem("customer_info"))._id;
      const apiBody = {
        user_id: user_id,
      };
      const { data } = await getPrefferedStylistList(apiBody);
      this.setState({
        ...this.state,
        data,
      });
    } catch (err) {
      console.log(err);
    } finally {
      this.setState({
        ...this.state,
        isLoader: false,
      });
    }
  };

  // DELETE / REMOVE PREFFEED STYLIST
  onRemovePrefferedStylist = async (service_id) => {
    try {
      this.setState({
        ...this.state,
        isLoader: true,
      });

      const apiBody = {
        service_id,
      };

      await deletePrefferedStylist(apiBody);
      NotificationAlert(
        NOTIFICATIONSMESSAGE.DELETE_PREFERED_STYLIST,
        NOTIFICATION_TYPE.SUCCESS
      );
      this.fetchPrefferedStylistList();
    } catch (err) {
      console.log(err);
    } finally {
      this.setState({
        ...this.state,
        isLoader: false,
      });
    }
  };

  render() {
    const { data, isLoader } = this.state;

    if (isLoader) return <Loader />;

    return (
      <Fragment>
        {data.length &&
          data.map((item, index) => {
            return (
              <div
                className="booking-list preferred-stylist d-flex align-items-center justify-content-between py-4"
                key={index}
              >
                <div className="booking-content d-flex align-items-center">
                  <div className="position-relative mr-4">
                    <img src={item.stylist.profile} alt="" />
                  </div>
                  <div className="booking-type d-md-flex justify-content-between w-100">
                    <Link
                      to={`/stylist-details/${
                        item.stylist._id +
                        "/" +
                        item.stylist.experience +
                        "/" +
                        item.stylist.firstname +
                        " " +
                        item.stylist.lastname
                      }`}
                      className="text-decoration-none text-dark"
                    >
                      {item.stylist.firstname + " " + item.stylist.lastname}
                    </Link>
                    <p>{item.stylist.experience} Stylist</p>
                  </div>
                </div>
                <div className="myaccount-action d-flex">
                  <button
                    className="d-flex align-items-center"
                    onClick={() =>
                      this.onRemovePrefferedStylist(item.stylist._id)
                    }
                  >
                    DELETE{" "}
                    <i className="fa fa-times ml-2" aria-hidden="true"></i>
                  </button>
                </div>
              </div>
            );
          })}
      </Fragment>
    );
  }
}

export default PrefferedStylist;
