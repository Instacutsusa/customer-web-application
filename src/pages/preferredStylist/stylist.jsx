import React, { Component, Fragment } from "react";
import SideBar from "../../components/Sidebar/Sidebar";
import PrefferedStylist from "./stylist/preferredStylist";
import BlockedStlist from ".//blockedStylisy/blockedStylist";
import SidebarHeader from "../../components/Sidebar/SidebarHeader/SidebarHeader";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import "./stylist.css";

class PrefferStylist extends Component {
  state = {
    data: [],
    toggleSidebar: false,
    active: "stylist",
  };

  SideBarToggle = () => {
    this.setState({
      toggleSidebar: !this.state.toggleSidebar,
    });
  };
  render() {
    const heading = "My Account",
      subHeading = "Stylist";

    const { data, toggleSidebar, active } = this.state;

    return (
      <Fragment>
        <div className="d-flex">
          <div className="">
            <SideBar
              SideBarToggle={this.SideBarToggle}
              active={active}
              toggleSidebar={toggleSidebar}
            />
          </div>
          <div className="sideBar-header">
            <SidebarHeader
              heading={heading}
              subHeading={subHeading}
              toggleSidebar={toggleSidebar}
              SideBarToggle={this.SideBarToggle}
            />
            <div className="history-container">
              <Tabs>
                <TabList className="react-tabs__tab-list preffered-stylist-tabs pt-3">
                  <Tab className="react-tabs__tab mr-3">Preffered Stylist</Tab>
                  <Tab className="react-tabs__tab">Blocked Stylist</Tab>
                </TabList>
                <TabPanel>
                  <PrefferedStylist data={data} />
                </TabPanel>
                <TabPanel>
                  <BlockedStlist data={data} />
                </TabPanel>
              </Tabs>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default PrefferStylist;
