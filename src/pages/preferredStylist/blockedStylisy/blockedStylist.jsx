import React, { Component, Fragment } from "react";
import {
  deleteBlockedStylist,
  getBlockedStylistList,
} from "../../../api/stylist.api";
import { NotificationAlert } from "../../../common/NotificationAlert";

import Loader from "../../../components/Loader/Loader";
import {
  NOTIFICATIONSMESSAGE,
  NOTIFICATION_TYPE,
} from "../../../_constant/notificationsMessage";
class BlockedStlist extends Component {
  state = {
    blockedStylists: [],
    switchChecked: false,
    isLoader: true,
  };

  componentDidMount = () => {
    this.fetchBlockedStylistList();
  };

  // FETCH ALL BLOCLED STYLIST
  fetchBlockedStylistList = async () => {
    try {
      const { data } = await getBlockedStylistList();
      this.setState({
        ...this.state,
        blockedStylists: data,
      });
    } catch (err) {
      console.log(err);
    } finally {
      this.setState({
        ...this.state,
        isLoader: false,
      });
    }
  };

  // DELETE BLOCKED STYLIST
  onDeleteBlcokedStylist = async (stylist_id) => {
    try {
      this.setState({
        ...this.state,
        isLoader: true,
      });
      const apiBody = {
        stylist_id,
      };

      await deleteBlockedStylist(apiBody);
      NotificationAlert(
        NOTIFICATIONSMESSAGE.DELETE_BLOCKED_STYLIST,
        NOTIFICATION_TYPE.SUCCESS
      );
      this.fetchBlockedStylistList();
    } catch (err) {
      this.setState({
        ...this.state,
        isLoader: false,
      });
      NotificationAlert(
        NOTIFICATIONSMESSAGE.API_ERROR,
        NOTIFICATION_TYPE.ERROR
      );
    }
  };

  //   getChckeboxValue = (event) => {
  //     this.setState({
  //       switchChecked: !this.state.switchChecked,
  //     });
  //   };

  render() {
    const { blockedStylists, isLoader } = this.state;

    if (isLoader) return <Loader />;

    return (
      <Fragment>
        {blockedStylists.length &&
          blockedStylists.map((item, index) => {
            return (
              <div
                className="booking-list preferred-stylist d-flex align-items-center justify-content-between py-4"
                key={index}
              >
                <div className="booking-content d-flex align-items-center">
                  <div className="position-relative mr-4">
                    <img src={item.image} alt="" />
                  </div>
                  <div className="booking-type d-md-flex justify-content-between w-100">
                    <h5>{item.name}</h5>
                    <p>{item.experience} Stylist</p>
                  </div>
                </div>
                <div className="myaccount-action blocked-stylist d-flex align-items-center">
                  {/* <div className="mt-2 mr-3">
                    <label className="switch">
                      <input
                        type="checkbox"
                        onChange={this.getChckeboxValue}
                        value={switchChecked}
                        checked={switchChecked}
                      />
                      <span className="slider round"></span>
                    </label>
                  </div> */}
                  <button
                    className="d-flex align-items-center"
                    onClick={() => this.onDeleteBlcokedStylist(item.stylist_id)}
                  >
                    DELETE{" "}
                    <i className="fa fa-times ml-2" aria-hidden="true"></i>
                  </button>
                </div>
              </div>
            );
          })}
      </Fragment>
    );
  }
}

export default BlockedStlist;
