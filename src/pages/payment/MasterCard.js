import React from "react";
import { Dropdown } from "react-bootstrap";

import "react-credit-cards/es/styles-compiled.css";
import "./Payment.css";
import CardBgImg from "../../assets/images/credit-card.svg";
import CreditCard from "../../assets/images/mastercard_logo.svg";
import AddPaymentIcon from "../../assets/icons/add-payment.svg";
import ViewPaymentIcon from "../../assets/icons/payment.svg";
import DeleteIcon from "../../assets/icons/delete-icon.svg";
import EditIcon from "../../assets/icons/edit.svg";

const MasterCard = ({ data, handleModal, setDefaultCustomerCard }) => {
  return (
    <>
      <div className="slider-banner-img position-relative">
        <img src={CardBgImg} alt="" />
        {data.isDefault && (
          <div className="position-absolute d-flex default-card">
            <i className="fa fa-check-circle mr-2" aria-hidden="true"></i>{" "}
            Default
          </div>
        )}
        <div className="position-absolute credit-card-details">
          <div className="credit-card-img text-right">
            <img src={CreditCard} alt="" className="d-inline" />
          </div>
          <div className="card-holder-name">
            <h6>Card Holder Name</h6>
            <h4>{data.account_holder_name}</h4>
          </div>
          <div className="d-flex card-holder-name">
            <h5>**** **** **** {data.lastd}</h5>
            <h5>
              {data.exp_year}/{data.exp_month}
            </h5>
          </div>
        </div>
        <div className="card-drop-down position-absolute">
          <Dropdown>
            <Dropdown.Toggle>
              <i className="fa fa-ellipsis-h" aria-hidden="true"></i>
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <div className="card-drop-item">
                <h5 onClick={() => setDefaultCustomerCard(data._id)}>
                  <i className="fa fa-star-o" aria-hidden="true"></i>
                  Set as Default
                </h5>
              </div>
              <div className="card-drop-item">
                <h5
                  className="d-flex align-items-center"
                  onClick={(e) => handleModal("edit", e, data)}
                >
                  {/* <i className="fa fa-trash-o" aria-hidden="true"></i>  */}
                  <img src={EditIcon} alt="" className="mr-2" />
                  Edit Card
                </h5>
              </div>
              {/* <div className="card-drop-item">
                <h5
                  className="d-flex align-items-center"
                  onClick={(e) => handleModal("add", e)}
                >
                  <i className="fa fa-trash-o" aria-hidden="true"></i> 
                  <img src={ViewPaymentIcon} alt="" className="mr-2" />
                  View Card
                </h5>
              </div> */}
              {/* <div className="card-drop-item">
                <h5
                  className="d-flex align-items-center"
                  onClick={(e) => handleModal("add", e)}
                >
                  <i className="fa fa-trash-o" aria-hidden="true"></i> 
                  <img src={AddPaymentIcon} alt="" className="mr-2" /> Add New
                  Card
                </h5>
              </div> */}
              <div
                className="card-drop-item"
                onClick={() => ""}
              >
                <h5 className="d-flex align-items-center">
                  {/* <i className="fa fa-trash-o" aria-hidden="true"></i>  */}
                  <img src={DeleteIcon} alt="" className="mr-2" /> Delete
                </h5>
              </div>
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </div>
    </>
  );
};

export default MasterCard;
