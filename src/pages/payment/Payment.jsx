import React, { Component, Fragment } from "react";
import "react-credit-cards/es/styles-compiled.css";
import Card from "react-credit-cards";

import SideBar from "../../components/Sidebar/Sidebar";
import SidebarHeader from "../../components/Sidebar/SidebarHeader/SidebarHeader";
import { Dropdown } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import "./Payment.css";
import windowSize from "react-window-size";
import Calendar from "../../components/Calendar/Calendar";
import Slider from "react-slick";
import AddPaymentIcon from "../../assets/icons/add-payment.svg";
import cardHolder from "../../assets/icons/calendar-icon.svg";
import UserIcon from "../../assets/icons/Icon-User.svg";
import IconCredit from "../../assets/icons/Icon-Credit-Cart.svg";
import InstacutsIcons from "../../assets/images/instacuts.png";

import VisaCard from "./VisaCard";
import MasterCard from "./MasterCard";
import {
  addNewCustomerCard,
  getAllCardList,
  getwalletTransactions,
  setDeafultCart,
  updateCustomerCard,
} from "../../api/payment.api";
import { NotificationAlert } from "../../common/NotificationAlert";
import {
  NOTIFICATIONSMESSAGE,
  NOTIFICATION_TYPE,
} from "../../_constant/notificationsMessage";
import {
  formatCreditCardNumber,
  formatCVC,
  formatExpirationDate,
} from "../../_utils/cardInputFormat";
import Loader from "../../components/Loader/Loader";
import { id } from "date-fns/locale";

class Payment extends Component {
  state = {
    toggleSidebar: false,
    cvc: "",
    expiry: "",
    focus: "",
    name: "",
    number: "",
    active: "payment",
    cardModal: "",
    showCalendar: false,
    accountCreation: false,
    masterCardData: [],
    visaCardData: [],
    walletTransactions: {},
    focused: "",
    issuer: "",
    isValid: false,
    cardInfo: {
      cardHolderName: "",
      cardNumber: "",
      lastd: "",
      cvc: "",
      expiryDate: "",
      zipCode: "",
    },
    isLoader: false,
    allCardInfo: [],
  };

  componentDidMount = () => {
    this.fetchAllCardList();
    this.fetchAllWalletTransactions();
  };

  // FETCH WALLE TRASCATIONS
  fetchAllWalletTransactions = async () => {
    const { data } = await getwalletTransactions();
    this.setState({
      ...this.state,
      walletTransactions: data,
    });
  };

  // FETCH ALL CARD LIST
  fetchAllCardList = async () => {
    try {
      this.setState({
        ...this.state,
        isLoader: true,
      });
      const { data } = await getAllCardList();

      const masterCardData = data.cards.filter(
        (card) => card.type === "mastercard"
      );
      const visaCardData = data.cards.filter(
        (card) => card.type.toLowerCase() === "visa"
      );

      this.setState({
        ...this.state,
        masterCardData,
        visaCardData,
        allCardInfo: data,
      });
    } catch (err) {
      console.log(err);
    } finally {
      this.setState({
        ...this.state,
        isLoader: false,
      });
    }
  };

  SideBarToggle = () => {
    this.setState({
      toggleSidebar: !this.state.toggleSidebar,
    });
  };

  handleShowCalendar = () => {
    this.setState({
      showCalendar: !this.state.showCalendar,
    });
  };

  handleModal = (value, e, cardData) => {
    switch (value) {
      case "add":
        this.setState({
          cardModal: "add",
        });
        return;

      case "edit":
        const {
          _id,
          lastd,
          account_holder_name,
          exp_month,
          exp_year,
          zip_code,
        } = cardData;
        const expiryDate = this.formatExpiratyDate(exp_month, exp_year);
        this.setState({
          ...this.state,
          cardModal: "edit",
          cardInfo: {
            ...this.state.cardInfo,
            card_id: _id || "",
            lastd: lastd || "",
            cardNumber: `************${lastd}`,
            cardHolderName: account_holder_name || "",
            expiryDate: expiryDate || "",
            zipCode: zip_code || "",
          },
        });
        return;

      case "close":
        this.setState({
          cardModal: "",
          cardInfo: {
            ...this.state.cardInfo,
            card_id: "",
            lastd: "",
            cardNumber: "",
            cardHolderName: "",
            expiryDate: "",
            zipCode: "",
          },
        });
        return;

      default:
        return;
    }
  };

  /**
   * Used to set default card
   * @param {String} cardId CardId of card
   */
  setDefaultCustomerCard = async (cardId) => {
    try {
      const apiBody = {
        cardId,
      };
      const { message } = await setDeafultCart(apiBody);

      NotificationAlert(message, NOTIFICATION_TYPE.SUCCESS);
      this.fetchAllCardList();
    } catch (err) {
      NotificationAlert(
        NOTIFICATIONSMESSAGE.API_ERROR,
        NOTIFICATION_TYPE.ERROR
      );
      console.log(err);
    }
  };

  // USED TO ADD NEW CUSTOMER CARD
  onSaveCardHandler = (e, isEdit) => {
    e.preventDefault();
    this.setState({
      ...this.state,
      isLoader: true,
    });
    const { cardInfo, issuer, isValid } = this.state;
    if (isEdit === "add") {
      this.addCardData(cardInfo, issuer, isValid);
    } else {
      this.updateCardData(cardInfo);
    }
  };

  formatExpiratyDate = (exp_month, exp_year) => {
    let expMonth = exp_month?.toString().split("");
    if (expMonth < 10 && expMonth?.length === 1) {
      return `0${expMonth.join("").toString()}/${exp_year}`;
    } else {
      return `${expMonth}/${exp_year}`;
    }
  };

  addCardData = async (cardInfo, issuer, isValid) => {
    try {
      if (!isValid) {
        return NotificationAlert(
          NOTIFICATIONSMESSAGE.INVALID_CARD_NUMBER,
          NOTIFICATION_TYPE.ERROR
        );
      }

      const apiBody = {
        holder_name: cardInfo.cardHolderName,
        zip_code: cardInfo.zipCode,
        type: issuer,
        exp_year: cardInfo.expiryDate.split("/")[0],
        exp_month: cardInfo.expiryDate.split("/")[1],
        customerId: "tok_" + issuer,
        lastd: cardInfo.cardNumber.substring(cardInfo.cardNumber.length - 4),
      };
      const { message } = await addNewCustomerCard(apiBody);
      NotificationAlert(message, NOTIFICATION_TYPE.SUCCESS);
      this.handleModal("close");
      this.fetchAllCardList();
    } catch (err) {
      const errMessage = err.response.data.message;
      NotificationAlert(errMessage, NOTIFICATION_TYPE.ERROR);
      console.log(err);
    } finally {
      this.setState({
        isLoader: false,
      });
    }
  };

  updateCardData = async (cardInfo) => {
    try {
      const apiBody = {
        card_id: cardInfo?.card_id,
        holder_name: cardInfo?.cardHolderName,
        zip_code: cardInfo?.zipCode,
        exp_year: cardInfo?.expiryDate.split("/")[0],
        exp_month: cardInfo?.expiryDate.split("/")[1],
      };
      const { message } = await updateCustomerCard(apiBody);
      NotificationAlert(message, NOTIFICATION_TYPE.SUCCESS);
      this.handleModal("close");
      this.fetchAllCardList();
    } catch (err) {
      const errMessage = err.response.data.message;
      NotificationAlert(errMessage, NOTIFICATION_TYPE.ERROR);
      console.log(err);
    } finally {
      this.setState({
        isLoader: false,
      });
    }
  };

  onChangeHandler = (e) => {
    const { name, value } = e.target;
    const { cardInfo } = this.state;
    const cardData = { ...cardInfo };
    cardData[name] = value;
    this.setState({
      ...this.state,
      cardInfo: cardData,
    });
  };

  handleInputFocus = ({ target }) => {
    this.setState({
      focused: target.name,
    });
  };

  handleInputChange = ({ target }) => {
    if (target.name === "cardNumber") {
      target.value = formatCreditCardNumber(target.value);
    } else if (target.name === "expiryDate") {
      target.value = formatExpirationDate(target.value);
    } else if (target.name === "cvc") {
      target.value = formatCVC(target.value);
    }
    const { cardInfo } = this.state;
    const cardData = { ...cardInfo };
    cardData[target.name] = target.value;
    this.setState({
      ...this.state,
      cardInfo: cardData,
    });
  };

  handleCallback = ({ issuer }, isValid) => {
    if (isValid) {
      this.setState({ issuer, isValid });
    }
  };

  render() {
    const heading = "Payment",
      subHeading = "Manage your payment";
    const { windowWidth } = this.props;
    const {
      cardModal,
      masterCardData,
      visaCardData,
      walletTransactions,
      cardInfo,
      focused,
      isLoader,
    } = this.state;

    const { active, toggleSidebar, showCalendar, accountCreation } = this.state;

    var settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      arrows: false,
      slidesToScroll: 1,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 1600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    };

    return (
      <Fragment>
        <div className="d-flex">
          <div className="">
            <SideBar
              SideBarToggle={this.SideBarToggle}
              active={active}
              toggleSidebar={toggleSidebar}
            />
          </div>
          <div className="sideBar-header">
            <SidebarHeader
              heading={heading}
              subHeading={subHeading}
              toggleSidebar={toggleSidebar}
              SideBarToggle={this.SideBarToggle}
            />
            {isLoader ? (
              <Loader />
            ) : (
              <div className="history-container">
                <div className="row">
                  {/* ---------- No Payment Method Added Screen ------------ */}

                  {/* <div className="col-lg-6 offset-lg-3 col-md-8 offset-md-2 mt-md-5 pt-md-5">
                                    <div className="no-service-container text-center">
                                        <div className="img-container">
                                            <img src={Logo} alt="" />
                                        </div>
                                        <div className="content">
                                            <h5 className="pb-3">No Active Payment Methods</h5>
                                            <h6>Add a New Card to start booking your services.</h6>
                                            <Link to="/">Add Payment Method</Link>
                                        </div>
                                    </div>
                                </div> */}

                  {/* ---------- Payment Method Screen ------------ */}

                  <div className="col-md-9">
                    <div className="latest-transactions d-flex justify-content-between align-items-center py-4">
                      <h5>Payment Method</h5>
                      <div className="card-drop-down">
                        <Dropdown>
                          <Dropdown.Toggle>
                            <i
                              className="fa fa-ellipsis-h"
                              aria-hidden="true"
                            ></i>
                          </Dropdown.Toggle>

                          <Dropdown.Menu>
                            {/* <div className="card-drop-item">
                            <h5 onClick={(e) => this.handleModal("edit", e)}>
                              <img
                                src={ViewPaymentIcon}
                                alt=""
                                className="mr-3"
                              />{" "}
                              View Card
                            </h5>
                          </div> */}
                            <div className="card-drop-item">
                              <h5 onClick={(e) => this.handleModal("add", e)}>
                                <img
                                  src={AddPaymentIcon}
                                  alt=""
                                  className="mr-3"
                                />{" "}
                                Add New Card
                              </h5>
                            </div>
                          </Dropdown.Menu>
                        </Dropdown>
                      </div>
                    </div>
                    {/* <Carousel responsive={responsive} arrows={false} className="pb-5 payment-carousel"> */}
                    <Slider
                      {...settings}
                      className="pb-5 card-detail-slider active-stylist-carousel"
                    >
                      {masterCardData.length &&
                        masterCardData.map((card, index) => {
                          return (
                            <MasterCard
                              key={`masterCard.${index}`}
                              data={card}
                              handleModal={this.handleModal}
                              setDefaultCustomerCard={
                                this.setDefaultCustomerCard
                              }
                            />
                          );
                        })}
                      {/* <MasterCard />
                    <MasterCard /> */}
                      {visaCardData.length &&
                        visaCardData.map((card, index) => {
                          return (
                            <VisaCard
                              key={`visaCard.${index}`}
                              data={card}
                              handleModal={this.handleModal}
                              setDefaultCustomerCard={
                                this.setDefaultCustomerCard
                              }
                            />
                          );
                        })}
                      {/* <VisaCard />
                    <VisaCard /> */}
                    </Slider>
                  </div>
                  <div className="col-md-3">
                    <div className="e-wallet-container pl-lg-5">
                      <div className="latest-transactions d-flex justify-content-between align-items-center py-4">
                        <h5>E-Wallet</h5>
                        <div className="cardateofbirth-drop-down">
                          <Dropdown>
                            <Dropdown.Toggle>
                              <i
                                className="fa fa-ellipsis-h"
                                aria-hidden="true"
                              ></i>
                            </Dropdown.Toggle>

                            <Dropdown.Menu>
                              <div className="card-drop-item">
                                <h5
                                  onClisLoaderick={(e) =>
                                    this.handleModal("edit", e)
                                  }
                                >
                                  <i
                                    className="fa fa-id-card-o"
                                    aria-hidden="true"
                                  ></i>{" "}
                                  View Card
                                </h5>
                              </div>
                              <div className="card-drop-item">
                                <h5 onClick={(e) => this.handleModal("add", e)}>
                                  <i
                                    className="fa fa-id-card-o"
                                    aria-hidden="true"
                                  ></i>{" "}
                                  Add New Card
                                </h5>
                              </div>
                            </Dropdown.Menu>
                          </Dropdown>
                        </div>
                      </div>
                      <div className="e-wallet-credits">
                        <h6 className="mb-2">Wallet Credits</h6>
                        <h4>
                          <span>{walletTransactions.balance}</span> Points
                        </h4>
                        <button className="mt-3">Wallet Transaction</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="latest-transactions d-flex justify-content-between align-items-center pb-4">
                      <h5>Latest Transcation</h5>
                      <div className="filteration">
                        <p>
                          Filter{" "}
                          <i
                            className="fa fa-calendar"
                            aria-hidden="true"
                            onClick={this.handleShowCalendar}
                          ></i>{" "}
                          {showCalendar && (
                            <div className="filter-calendar">
                              <Calendar />{" "}
                            </div>
                          )}{" "}
                        </p>
                      </div>
                    </div>
                    <div className="transactions-day">
                      <h6>Today</h6>
                    </div>
                    {walletTransactions?.transaction?.map((data, index) => {
                      return (
                        <div
                          className="booking-list transaction-list d-flex align-items-center justify-content-between py-4"
                          key={index}
                        >
                          <div className="booking-content d-flex align-items-center">
                            {data.transaction_type === "deduction" ? (
                              <i
                                className="fa fa-times mr-4"
                                aria-hidden="true"
                              ></i>
                            ) : (
                              <i
                                className="fa fa-check mr-4"
                                aria-hidden="true"
                              ></i>
                            )}
                            <div className="transaction-date d-md-flex justify-content-between">
                              <h5>{data.message}</h5>
                              {windowWidth < 767 && (
                                <h6>
                                  {new Date(parseInt(data.created_at))
                                    .toDateString()
                                    .toString()}
                                </h6>
                              )}
                            </div>
                          </div>
                          {windowWidth > 767 && (
                            <>
                              <div className="service-type">
                                <p>{data.message}</p>
                              </div>
                              <div className="transaction-date">
                                <h6>
                                  {new Date(parseInt(data.created_at))
                                    .toDateString()
                                    .toString()}
                                </h6>
                              </div>
                            </>
                          )}
                          <div
                            className={`order-amount d-flex align-items-center ${
                              data.transaction_type === "deduction"
                                ? "failed"
                                : ""
                            } `}
                          >
                            <span>
                              {data.transaction_type === "deduction"
                                ? "-"
                                : "+"}{" "}
                              ${data.amount}
                            </span>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
        <Modal
          show={cardModal}
          onHide={(e) => this.handleModal("close", e)}
          className="add-profile-modal"
        >
          <Modal.Header closeButton></Modal.Header>
          <Modal.Body>
            <div className="profile-container p-0">
              <h2 className="mb-5">
                {cardModal === "add" ? "Add Card" : "Edit Card"}
              </h2>
              <div className="d-flex justify-content-center">
                <div className="img-container position-relative">
                  <Card
                    name={cardInfo.cardHolderName}
                    number={cardInfo.cardNumber}
                    expiry={cardInfo.expiryDate}
                    cvc={cardInfo.cvc}
                    focused={focused}
                    callback={this.handleCallback}
                  />
                  {/* <img src={CardBgImg} alt="" /> */}
                  {/* <div className="position-absolute credit-card-details">
                    <div className="credit-card-img text-right">
                      <img src={CreditCard} alt="" className="d-inline" />
                    </div>
                    <div className="card-holder-name">
                      <h6>Card Holder Name</h6>
                      <h4>Samantha William Smith</h4>
                    </div>
                    <div className="d-flex card-holder-name">
                      <h5>**** **** **** 1289</h5>
                      <h5>09/25</h5>
                    </div>
                  </div> */}
                </div>
              </div>
            </div>
            <div className="signin-container card-modal-container p-0 mt-5 mb-0">
              <form
                className="form-container mt-0"
                onSubmit={(e) => this.onSaveCardHandler(e, cardModal)}
              >
                <div className="form-row">
                  <div className="form-group mb-4 col-md-12 position-relative">
                    <label>Card Number</label>
                    <input
                      type="tel"
                      pattern={cardModal === "add" ? `[\d| ]{16,22` : null}
                      disabled={cardModal === "edit"}
                      className="form-control pl-5"
                      name="cardNumber"
                      value={cardInfo.cardNumber}
                      onChange={this.handleInputChange}
                      onFocus={this.handleInputFocus}
                      // onChange={this.onChangeHandler}
                      placeholder=""
                      autoComplete="off"
                    />
                    {/* <i className="fa fa-id-card-o position-absolute" aria-hidden="true"></i> */}
                    <img
                      src={IconCredit}
                      alt=""
                      className="position-absolute card-form"
                    />
                  </div>
                  <div className="form-group position-relative mb-4 col-md-12 ">
                    <label>Card Holder Name</label>
                    <input
                      type="text"
                      className="form-control pl-5"
                      name="cardHolderName"
                      value={cardInfo.cardHolderName}
                      onChange={this.handleInputChange}
                      onFocus={this.handleInputFocus}
                      // onChange={this.onChangeHandler}
                      placeholder=""
                      autoComplete="off"
                    />
                    {/* <i className="fa fa-user position-absolute" aria-hidden="true"></i> */}
                    <img
                      src={UserIcon}
                      alt=""
                      className="position-absolute card-form"
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group mb-4 col-md-4 position-relative">
                    <label>Expiry</label>
                    <input
                      type="tel"
                      className="form-control pl-5"
                      pattern="\d\d/\d\d"
                      name="expiryDate"
                      value={cardInfo.expiryDate}
                      onChange={this.handleInputChange}
                      onFocus={this.handleInputFocus}
                      // onChange={this.onChangeHandler}
                      placeholder=""
                      autoComplete="off"
                    />
                    {/* <i className="fa fa-calendar-o p/osition-absolute" aria-hidden="true"></i> */}
                    <img
                      src={cardHolder}
                      alt=""
                      className="position-absolute card-form"
                    />
                  </div>
                  {cardModal === "add" && (
                    <div className="form-group mb-4 col-md-4 position-relative">
                      <label>CVC</label>
                      <input
                        type="tel"
                        className="form-control pl-5"
                        name="cvc"
                        pattern="\d{3,4}"
                        value={cardInfo.cvc}
                        onChange={this.handleInputChange}
                        onFocus={this.handleInputFocus}
                        // onChange={this.onChangeHandler}
                        placeholder=""
                        autoComplete="off"
                      />
                      {/* <i className="fa fa-user position-absolute" aria-hidden="true"></i> */}
                      <img
                        src={UserIcon}
                        alt=""
                        className="position-absolute card-form"
                      />
                    </div>
                  )}
                  <div className="form-group mb-4 col-md-4 position-relative">
                    <label>Zip Code</label>
                    <input
                      type="text"
                      className="form-control pl-5"
                      name="zipCode"
                      value={cardInfo.zipCode}
                      onChange={this.handleInputChange}
                      onFocus={this.handleInputFocus}
                      // onChange={this.onChangeHandler}
                      placeholder=""
                      autoComplete="off"
                    />
                    {/* <i className="fa fa-user position-absolute" aria-hidden="true"></i> */}
                    <img
                      src={UserIcon}
                      alt=""
                      className="position-absolute card-form"
                    />
                  </div>
                </div>
                <div className="d-flex align-items-center">
                  <button className="btn btn-primary continue-btn">
                    {isLoader && <i className="fa fa-spinner fa-spin"></i>}
                    save Card
                  </button>
                </div>
              </form>
            </div>
          </Modal.Body>
        </Modal>
        {accountCreation && (
          <div className="upload-image-modal">
            <div className="modal-container">
              <div className="modal-header mt-2">
                <img src={InstacutsIcons} alt="" />
              </div>
              <div className="content my-4">
                <h5>Card Added Successful</h5>
                <p>Any test charges to verify the card will be refunded.</p>
              </div>
            </div>
          </div>
        )}
        {accountCreation && (
          <div className="upload-image-modal">
            <div className="modal-container">
              <div className="modal-header mt-2">
                <img src={InstacutsIcons} alt="" />
              </div>
              <div className="content my-4">
                <h5>Set Refund Method</h5>
                <p>Your refunds will be processed to the selected method.</p>
              </div>
              <div className="filter-actions position-relative py-0 shadow-none">
                <ul>
                  <li className="active">Wallet</li>
                  <li>Credit Card</li>
                </ul>
              </div>
            </div>
          </div>
        )}
      </Fragment>
    );
  }
}

export default windowSize(Payment);
