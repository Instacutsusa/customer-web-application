import React, { Component, Fragment } from "react";
import SideBar from "../../components/Sidebar/Sidebar";
import SidebarHeader from "../../components/Sidebar/SidebarHeader/SidebarHeader";
import BookingImg from "../../assets/images/booking.png";
import "./booking.css";
import { getBookingAllList } from "../../api/booking.api";
import Loader from "../../components/Loader/Loader";
import { BOOKING_TYPE } from "../../_constant/bookingType";

class Booking extends Component {
  state = {
    data: [],
    toggleSidebar: false,
    active: "booking",
    isLoader: true,
  };

  componentDidMount = () => {
    this.fetchAllBookingsList();
  };

  fetchAllBookingsList = async () => {
    try {
      this.setState({
        ...this.state,
        isLoader: true,
      });
      const { data } = await getBookingAllList(10, 1, "new");
      this.setState({
        ...this.state,
        data,
      });
    } catch (err) {
      console.log(err);
    } finally {
      this.setState({
        ...this.state,
        isLoader: false,
      });
    }
  };

  SideBarToggle = () => {
    this.setState({
      toggleSidebar: !this.state.toggleSidebar,
    });
  };

  render() {
    const heading = "Bookings",
      subHeading = "My Bookings";

    const { data, toggleSidebar, active, isLoader } = this.state;

    return (
      <Fragment>
        <div className="d-flex">
          <div className="">
            <SideBar
              SideBarToggle={this.SideBarToggle}
              active={active}
              toggleSidebar={toggleSidebar}
            />
          </div>
          <div className="sideBar-header">
            <SidebarHeader
              heading={heading}
              subHeading={subHeading}
              toggleSidebar={toggleSidebar}
              SideBarToggle={this.SideBarToggle}
            />
            {!isLoader ? (
              <div className="myaccount-container">
                {data.length ? (
                  data.map((item, index) => {
                    return (
                      <div
                        className="booking-list d-flex align-items-center justify-content-between py-4"
                        key={index}
                      >
                        <div className="booking-content d-flex align-items-center">
                          <div className="position-relative mr-4">
                            <img src={BookingImg} alt="" />
                            <i
                              className="fa fa-circle mr-2 position-absolute"
                              aria-hidden="true"
                            ></i>
                          </div>
                          <div className="booking-type d-md-flex justify-content-between w-100">
                            <h5>{item.order_number}</h5>
                            <p>{BOOKING_TYPE[item.booking_type]}</p>
                          </div>
                        </div>
                        <div className="d-md-flex booking-content justify-content-between">
                          <div className="booking-date">
                            <h6>
                              {new Date(parseInt(item.date)).toLocaleString()}
                            </h6>
                          </div>
                          <div className="myaccount-action d-flex justify-content-end">
                            <button>
                              VIEW DETAILS
                              <i
                                className="fa fa-angle-right pl-3"
                                aria-hidden="true"
                              ></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    );
                  })
                ) : (
                  <div>
                    <p>No Booking found!</p>
                  </div>
                )}
              </div>
            ) : (
              <Loader />
            )}
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Booking;
