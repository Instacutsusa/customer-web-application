exports.NOTIFICATIONSMESSAGE = {
  API_ERROR: "Something went wrong",
  OTP_VERIFY: "OTP Verified Successfully!",
  OTP_VERIFY_FAIL: "Failed to Verify OTP",
  OTP_RESEND: "Resent OTP",
  SIGNUP_INVALID_VALUE: "Please enter valid details",
  UPDATE_CUSTOMER_PROFILE: "Your profile has been updated!",
  SET_ADDRESS_ACTIVE: "Address set as active successfully!",
  ADD_NEW_ADDRESS: "Address added successfully!",
  UPDATE_ADDRESS: "Address updated successfully",
  INVALID_CARD_NUMBER: "Please enter valid card number",
  DELETE_PREFERED_STYLIST: "Stylist has been removed!",
  DELETE_BLOCKED_STYLIST: "Blocked stylist has been removed!",
};

exports.NOTIFICATION_TYPE = {
  SUCCESS: "success",
  ERROR: "error",
};
