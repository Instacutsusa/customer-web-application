export const ORDER_STATUS = {
  0: "Awaiting",
  1: "Accepted",
  2: "Reached Location",
  3: "Started service",
  4: "Order completion",
  5: "Rejected",
  6: "cancelled",
  7: "rescheduled",
  8: "expired",
  9: "Start Schedule Service",
};
