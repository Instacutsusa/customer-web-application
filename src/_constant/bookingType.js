export const BOOKING_TYPE = {
    0: "On-Demand Order",
    1: "Scheduled Order",
    2: "Custom Order"
}