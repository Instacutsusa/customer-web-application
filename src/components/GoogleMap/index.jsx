import React, { Component, useEffect, useState } from "react";

import GoogleMapReact from "google-map-react";
import Marker from "./Marker";
import LocationIcon from "../../assets/icons/location.svg";

const MyGoogleMap = (props) => {
  const [mapApi, _mapApi] = useState(null);
  const [places, _places] = useState([]);
  const [center, _center] = useState({
    lat: "",
    lng: "",
  });
  const [zoom, _zoom] = useState(10);
  const [address, _address] = useState("");
  const [draggable, _draggable] = useState(true);
  const [lat, _lat] = useState(null);
  const [lng, _lng] = useState(null);
  const [loadNewMap, _loadNewMap] = useState(true);

  useEffect(() => {
    if (props.location) {
      _onClick(props.location);
    }
    if (props.isEdit) {
      _zoom(10);
    }
  }, []);

  useEffect(() => {
    if (props.isEdit) {
      _zoom(10);
    } else {
      if (!props?.isEnabledPermission) {
        _zoom(3);
      } else {
        _zoom(10);
      }
    }
  }, [props.isEnabledPermission]);

  useEffect(() => {
    if (props.location) {
      _lat(props.location.lat);
      _lng(props.location.lng);
      _center({
        lat: props.location.lat,
        lng: props.location.lng,
      });
    }
  }, [props.location]);

  useEffect(() => {
    if (mapApi && props.location.lat && props.location.lng) _generateAddress();
  }, [props.location.lat, props.location.lng]);

  useEffect(() => {
    _loadNewMap(props.generateNewMap);
  }, [props.generateNewMap]);

  const takeLocationPermission = () => {
    navigator.permissions
      .revoke({ name: "geolocation" })
      .then(function (result) {});
  };

  const onMarkerInteraction = (childKey, childProps, mouse) => {
    _draggable(false);
    _lat(mouse.lat);
    _lng(mouse.lng);
  };
  const onMarkerInteractionMouseUp = (childKey, childProps, mouse) => {
    _draggable(true);
    _generateAddress();
  };

  const _onChange = ({ center, zoom }) => {
    _center(center);
    _zoom(zoom);
  };

  const _onClick = (value) => {
    props.onMarkerInteraction(value);
    _lat(value.lat);
    _lng(value.lng);
    _loadNewMap(true);
  };

  const apiHasLoaded = (map, maps) => {
    _mapApi(maps);
  };

  const _generateAddress = () => {
    const geocoder = new mapApi.Geocoder();
    geocoder.geocode(
      { location: { lat: props.location.lat, lng: props.location.lng } },
      (results, status) => {
        if (loadNewMap)
          props.onGetCurrentLocationLatLong({ lat: lat, lng: lng }, results[0]);
        if (status === "OK") {
          if (results[0]) {
            _address(results[0].formatted_address);
          } else {
            console.log("No results found");
          }
        } else {
          console.error("Geocoder failed due to: " + status);
        }
      }
    );
  };

  return (
    <div className="mapContainer" id="mapContainer">
      <div className="locateme-button">
        <div className="locateme">
          <button onClick={() => props.onGetCurrentLocation()}>
            Locate Me <img src={LocationIcon} alt="" className="ml-2" />
            {/* <i className="fa fa-map-marker ml-2" aria-hidden="true"></i> */}
          </button>
        </div>
      </div>
      <GoogleMapReact
        defaultCenter={props.defaultLocation}
        center={center}
        location={props.location}
        zoom={zoom}
        draggable={draggable}
        onChange={_onChange}
        onChildMouseDown={onMarkerInteraction}
        onChildMouseUp={onMarkerInteractionMouseUp}
        onChildMouseMove={onMarkerInteraction}
        onChildClick={() => ""}
        onClick={(value) => _onClick(value)}
        bootstrapURLKeys={{
          key: process.env.REACT_APP_GOOGLE_API,
        }}
        // bootstrapURLKeys={{
        //     key: 'AIzaSyAM9uE4Sy2nWFfP-Ha6H8ZC6ghAMKJEKps',
        //     libraries: ['places', 'geometry'],
        // }}
        yesIWantToUseGoogleMapApiInternals
        onGoogleApiLoaded={({ map, maps }) => apiHasLoaded(map, maps)}
      >
        <Marker text={address} lat={lat} lng={lng} />
      </GoogleMapReact>
    </div>
  );
};
// }

export default MyGoogleMap;
