import React from "react";
import "./style.css";
import mapLocationIcon from "../../assets/icons/map_location.svg";

const Marker = ({ text, onClick }) => (
  <span className="googleMapMarker" onClick={onClick}>
    <div class="rim1"></div>
    <div class="rim2"></div>
    <div class="rim3"></div>
    <img src={mapLocationIcon} alt="Marker" />
  </span>
);

export default Marker;
