import React, { Fragment, useEffect, useState } from "react";
import NoService from "../../Services/noServices/noServices";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import StylistCard from "../stylistCard/stylistCard";
import Header from "../../DynamicHeader/Header";
import { useLocation, useHistory } from "react-router-dom";

import {
  getAllInactiveStylist,
  searchStylistByName,
} from "../../../api/stylist.api";

import { Dropdown } from "react-bootstrap";
import FilterIcon from "../../../assets/icons/filter.svg";
import CloseIcon from "../../../assets/icons/cancel-icon.svg";
import { StylistCardSkeleton } from "../../StylistSkeleton";

const SearchStylist = (props) => {
  const location = useLocation();
  const history = useHistory();

  const [isLoading, _setIsLoading] = useState(true);
  const [stylistData, _setStylistData] = useState([]);

  // const exp = ["senior", "advanced"];  // line no: 22 to 29 use for static data
  // const sortings = [
  //   "rating_count",
  //   "stylist_fav_count",
  //   "completed_order_count",
  // ];
  // const gender = ["men", "women"];
  // const ratings = [3, 4, 5];

  const [seniorStylist, _setSeniorStylist] = useState({
    allSeniorStylist: [],
    filterdSeniorStylist: [],
  });
  const [advancedStylist, _setAdvancedStylist] = useState({
    allAdvancedStylist: [],
    filteredAdvancedStylist: [],
  });

  const [sort, _setSort] = useState({
    category: null,
  });

  const [filter, _setFilter] = useState({
    gender: null,
    ratings: null,
  });

  const [searchStylist, _setSearchStylist] = useState({
    search: null,
    stylist_level: null,
  });

  useEffect(() => {
    if (location?.state?.searchName) {
      _setIsLoading(true);
      _setSearchStylist({
        ...searchStylist,
        search: location.state.searchName,
      });
      history.replace(location.pathname, null);
    }
  }, []);

  useEffect(() => {
    getStylist();

    // let newData = new Array(200).fill({ name: "" });
    // newData = newData.map((rec, index) => {
    //   return {
    //     ...rec,
    //     _id: "604b8165a008d1001a8e8546",
    //     firstname: "Viraj Desai",
    //     lastname: index,
    //     experience: exp[Math.floor((Math.random() * 10) / 5)],
    //     gender: gender[Math.floor((Math.random() * 10) / 5)],
    //     rating_count: ratings[Math.round((Math.random() * 10) / 5)],
    //     completed_order_count: Math.round(Math.random() * 100),
    //     stylist_fav_count: Math.round(Math.random() * 200),
    //   };
    // });
    // filterByExperience("senior", newData, true);
    // filterByExperience("advanced", newData, true);
    // _setStylistData(data);
  }, []);

  useEffect(() => {
    if (searchStylist.search) {
      getStylist("search");
    } else {
      getStylist();
    }
  }, [searchStylist.search]);

  const getStylist = async (type) => {
    _setIsLoading(true);

    let stylistData = [];

    if (type === "search") {
      const apiBody = {
        search: searchStylist.search,
      };
      const { status, data, message } = await searchStylistByName(apiBody);
      stylistData = data;
    } else {
      const {
        allStylistDataResstatus,
        allStylistData,
        allStylistDataResMessage,
      } = "";
      const { status, data, message } = await getAllInactiveStylist();
      stylistData = data;
    }

    if (stylistData?.body) {
      _setStylistData(stylistData?.body);
      filterByExperience("senior", stylistData?.body, true);
      filterByExperience("advanced", stylistData?.body, true);
    }
    _setIsLoading(false);
  };

  const debounce = (func, delay) => {
    let debounceTimer;
    return function () {
      const context = this;
      const args = arguments;
      clearTimeout(debounceTimer);
      debounceTimer = setTimeout(() => func.apply(context, args), delay);
    };
  };

  useEffect(() => {
    if (
      (seniorStylist.allSeniorStylist.length > 0 ||
        advancedStylist.allAdvancedStylist.length > 0) &&
      !filter.ratings &&
      !filter.gender
    ) {
      _setSeniorStylist({
        ...seniorStylist,
        filterdSeniorStylist: seniorStylist.allSeniorStylist,
      });
      _setAdvancedStylist({
        ...advancedStylist,
        filteredAdvancedStylist: advancedStylist.allAdvancedStylist,
      });
    }
    if (filter.ratings || filter.gender) {
      const newSeniorStylist = seniorStylist.allSeniorStylist?.filter(
        (stylist) => {
          if (filter.ratings && filter.gender === null)
            return stylist.rating_count >= filter.ratings;
          return (
            stylist.gender === filter?.gender &&
            stylist.rating_count >= filter.ratings
          );
        }
      );
      const newAdvancedstylist = advancedStylist.allAdvancedStylist?.filter(
        (stylist) => {
          return (
            stylist.gender === filter?.gender &&
            stylist.rating_count >= filter.ratings
          );
        }
      );
      _setSeniorStylist({
        ...seniorStylist,
        filterdSeniorStylist: newSeniorStylist,
      });
      _setAdvancedStylist({
        ...advancedStylist,
        filteredAdvancedStylist: newAdvancedstylist,
      });
    }
  }, [filter]);

  const onFilterSelectHandler = (filter) => {
    _setSort(filter);
    const newSeniorStylistData = [...seniorStylist.filterdSeniorStylist];
    const newAdvancedStylistData = [...advancedStylist.filteredAdvancedStylist];
    const sortedSeniorStylistData = newSeniorStylistData.sort((a, b) => {
      return b[filter.category] - a[filter.category];
    });
    const sortedAdvancedStylistData = newAdvancedStylistData.sort((a, b) => {
      return b[filter.category] - a[filter.category];
    });
    _setSeniorStylist({
      ...seniorStylist,
      filterdSeniorStylist: sortedSeniorStylistData,
    });
    _setAdvancedStylist({
      ...advancedStylist,
      filteredAdvancedStylist: sortedAdvancedStylistData,
    });
  };

  const filterByExperience = (expLevel, stylistArray, isReset) => {
    const filteredData = stylistArray.filter(
      (stylist) => stylist.experience === expLevel
    );
    if (isReset) {
      if (expLevel === "senior")
        _setSeniorStylist({
          allSeniorStylist: filteredData,
          filterdSeniorStylist: filteredData,
        });
      if (expLevel === "advanced")
        _setAdvancedStylist({
          allAdvancedStylist: filteredData,
          filteredAdvancedStylist: filteredData,
        });
    } else {
      if (expLevel === "senior")
        _setSeniorStylist({
          ...seniorStylist,
          filterdSeniorStylist: filteredData,
        });
      if (expLevel === "advanced")
        _setAdvancedStylist({
          ...advancedStylist,
          filteredAdvancedStylist: filteredData,
        });
    }
  };

  const setStylistSearchName = debounce(function (e) {
    // use Debouncing for reduce api request call
    _setSearchStylist({
      ...searchStylist,
      search: e.target.value,
    });
  }, 500);

  const clearSearchName = () => {
    _setSearchStylist({
      search: null,
      stylist_level: null,
    });
  };

  const noServiceText = "No Active Stylists In your Location";
  const differentService =
    "Try Advanced Stylists or Change Your Chosen Location";

  return (
    <Fragment>
      <Header
        setStylistSearchName={(name) => setStylistSearchName(name)}
        searchStylistName={searchStylist.search}
        clearSearchName={clearSearchName}
        {...props}
      />
      <div className="container pb-5">
        <Tabs>
          <div className="row pb-4 pt-3">
            <div className="col-md-12">
              <div className="heading d-flex justify-content-between flex-wrap">
                <div className="title d-flex align-items-center">
                  <h5>Search Result</h5>
                </div>
                <div className="filterButtonSection d-flex flex-wrap justify-content-center align-items-center">
                  <div className="typeFilteration">
                    <TabList className="react-tabs__tab-list stylist-tabs">
                      <Tab className="react-tabs__tab stylist-tab-content">
                        Senior Stylist
                      </Tab>
                      <Tab className="react-tabs__tab stylist-tab-content">
                        Advanced Stylist
                      </Tab>
                    </TabList>
                  </div>
                  <div className="sortButton my-2">
                    <Dropdown className="d-inline mx-2">
                      <Dropdown.Toggle
                        id="dropdown-autoclose-outside"
                        className="stylist-sort-button filter"
                      >
                        <img src={FilterIcon} alt="" className="ml-2" />
                        <span className="ml-2 filter-span">Sort & Filter</span>
                      </Dropdown.Toggle>
                      <Dropdown.Menu className="dropdownMenu customScroll">
                        <div className="dropdownContainer">
                          <div className="dropdownListHeader d-flex justify-content-between">
                            <span>Sort Stylist By</span>
                            <Dropdown.Toggle
                              id="dropdown-autoclose-outside"
                              className="text-dark"
                            >
                              <span className="ml-2">&#10006;</span>
                            </Dropdown.Toggle>
                          </div>
                          <span
                            onClick={() =>
                              onFilterSelectHandler({
                                category: "rating_count",
                              })
                            }
                            className={`${
                              sort.category === "rating_count" && "activeList"
                            }`}
                          >
                            Average Rated
                          </span>
                          <span
                            onClick={() =>
                              onFilterSelectHandler({
                                category: "completed_order_count",
                              })
                            }
                            className={`${
                              sort.category === "completed_order_count" &&
                              "activeList"
                            }`}
                          >
                            Completed services
                          </span>
                          <span
                            onClick={() =>
                              onFilterSelectHandler({
                                category: "stylist_fav_count",
                              })
                            }
                            className={`${
                              sort.category === "stylist_fav_count" &&
                              "activeList"
                            }`}
                          >
                            Most Preferred
                          </span>
                        </div>
                        <li className="divider"></li>
                        <div className="dropdownContainer">
                          <div className="dropdownListHeader">Filter By</div>
                          <span
                            onClick={() =>
                              _setFilter({ gender: null, ratings: null })
                            }
                            className={`${
                              filter.gender === null && "activeList"
                            }`}
                          >
                            None
                          </span>
                          <span
                            className={`${
                              filter.gender !== null && "activeList"
                            }`}
                          >
                            Gender
                          </span>
                          <div className="filterGenderSection">
                            <span
                              className={`${
                                filter.gender === "men" && "activeList"
                              }`}
                              onClick={() =>
                                _setFilter({ ...filter, gender: "men" })
                              }
                            >
                              Male
                            </span>
                            <span
                              className={`${
                                filter.gender === "women" && "activeList"
                              }`}
                              onClick={() =>
                                _setFilter({ ...filter, gender: "women" })
                              }
                            >
                              Female
                            </span>
                          </div>
                          <div className="dropdownContainer">
                            <span
                              className={`d-flex justify-content-between align-items-center ${
                                filter.ratings !== null && "activeList"
                              }`}
                            >
                              <span>Ratings</span>
                              <span>
                                <select
                                  name="Ratings"
                                  id="ratings"
                                  value={filter.ratings}
                                  onChange={(e) =>
                                    _setFilter({
                                      ...filter,
                                      ratings: e.target.value,
                                    })
                                  }
                                >
                                  <option value="0">None</option>
                                  <option value="3">3+</option>
                                  <option value="4">4+</option>
                                  <option value="5">5</option>
                                </select>
                              </span>
                            </span>
                          </div>
                        </div>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row position-relative">
            <div className="col-md-12">
              {/* <TabList className="react-tabs__tab-list stylist-tabs search-tabs">
                                <Tab className="react-tabs__tab stylist-tab-content">Senior Stylist</Tab>
                                <Tab className="react-tabs__tab stylist-tab-content">Advanced Stylist</Tab>
                            </TabList> */}
              <TabPanel>
                <div className="row ">
                  <div className="col-md-12">
                    <div className="heading">
                      <div className="search-title d-flex align-items-center justify-content-between">
                        <h4>Stylist</h4>
                        <h6>
                          Showing {seniorStylist?.filterdSeniorStylist?.length}{" "}
                          Results
                        </h6>
                      </div>
                    </div>
                  </div>
                </div>
                {isLoading ? (
                  <div className="row">
                    {[...new Array(6)].map((ele, index) => (
                      <StylistCardSkeleton
                        key={`activeSeniorStylist.${index}`}
                      />
                    ))}
                  </div>
                ) : seniorStylist.filterdSeniorStylist?.length > 0 ? (
                  <div className="row">
                    <StylistCard
                      stylistData={seniorStylist.filterdSeniorStylist}
                    />
                  </div>
                ) : (
                  <NoService
                    noServiceText={noServiceText}
                    differentService={differentService}
                  />
                )}
              </TabPanel>
              <TabPanel>
                <div className="row ">
                  <div className="col-md-12">
                    <div className="heading">
                      <div className="search-title d-flex align-items-center justify-content-between">
                        <h4>Stylist</h4>
                        <h6>
                          Showing{" "}
                          {advancedStylist?.filteredAdvancedStylist?.length}{" "}
                          Results
                        </h6>
                      </div>
                    </div>
                  </div>
                </div>
                {isLoading ? (
                  <div className="row">
                    {[...new Array(6)].map((ele, index) => (
                      <StylistCardSkeleton
                        key={`activeStylistAdvanced.${index}`}
                      />
                    ))}
                  </div>
                ) : advancedStylist.filteredAdvancedStylist?.length > 0 ? (
                  <div className="row">
                    <StylistCard
                      stylistData={advancedStylist.filteredAdvancedStylist}
                      stylistCategory="activeStylist"
                    />
                  </div>
                ) : (
                  <NoService
                    noServiceText={noServiceText}
                    differentService={differentService}
                  />
                )}
              </TabPanel>
            </div>
          </div>
        </Tabs>
      </div>
    </Fragment>
  );
};

export default SearchStylist;
