import React, { Component, Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Active1 from "../../assets/images/active1.png";
import Active2 from "../../assets/images/active2.png";
import stylistAvatar from "../../assets/images/stylist-avatar.svg";

import "./stylist.css";
import Slider from "react-slick";
import { HashLoader } from "react-spinners";
import { getAllActiveStylist } from "../../api/stylist.api";
import StylistAvatarSkeleton from "../StylistSkeleton/StylistAvatarSkeleton";
import { config } from "../../config/config";

const ActiveStylist = () => {
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 11,
    slidesToScroll: 5,
    arrows: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 7,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 2,
        },
      },
    ],
  };

  const [isLoading, _setIsLoading] = useState(true);
  const [activeStylist, _setActiveStylist] = useState([]);
  useEffect(() => {
    {
      (async () => {
        const response = await getAllActiveStylist();
        if (response?.data?.result) {
          _setActiveStylist(response?.data?.result);
        }
        _setIsLoading(false);
      })();
    }
  }, []);

  return (
    <Fragment>
      <div className="row pt-3 pb-4">
        <div className="col-md-12">
          <div className="heading d-flex justify-content-between">
            <div className="title d-flex align-items-center">
              <h5>Active Stylists </h5>
              <h6>
                {activeStylist?.length - 6 > 0 && (
                  <>
                    <i className="fa fa-circle mr-2" aria-hidden="true"></i>{" "}
                    {activeStylist?.length - 6}
                  </>
                )}
              </h6>
            </div>
            <div className="show-all-link pr-4">
              <Link to="/active-stylists">See all</Link>
            </div>
          </div>
        </div>
      </div>
      {/* <Carousel responsive={responsive} arrows={false} className="pb-5 active-stylist-carousel"> */}

      <Slider {...settings} className="pb-5 active-stylist-carousel">
        {activeStylist?.map((stylist, index) => (
          <Link
            to={`/stylist-details/${stylist?._id}/${stylist.experience}/${stylist.firstname}`}
            key={`activeStylist.${index}`}
          >
            <div className="active-stylist position-relative">
              <img
                src={
                  !stylist?.profile.includes("https")
                    ? `${
                        config.IMAGE_URL +
                        "/" +
                        config.STYLIST_PROFILE_DIAMENTION +
                        "/" +
                        stylist?.profile
                      }`
                    : stylistAvatar
                }
                alt=""
              />
              <i
                className="fa fa-circle mr-2 position-absolute"
                aria-hidden="true"
              ></i>
              <div className="stylist-name pt-2">
                <h6>{`${stylist?.firstname} ${stylist.lastname}`}</h6>
              </div>
            </div>
          </Link>
        ))}
      </Slider>
    </Fragment>
  );
};

export default ActiveStylist;
