import React, { Fragment, useEffect, useState } from "react";
import NoService from "../../Services/noServices/noServices";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import "./stylistList.css";
import { HashLoader } from "react-spinners";
import FilterIcon from "../../../assets/icons/filter.svg";
import StylistCard from "../stylistCard/stylistCard";
import { Dropdown } from "react-bootstrap";
import {
  getAllInactiveStylist,
  sortFilterStylistData,
} from "../../../api/stylist.api";
import { StylistCardSkeleton } from "../../StylistSkeleton";

const StylistList = ({ noServiceText, differentService }) => {
  const [isLoading, _setIsLoading] = useState(true);
  const [tabIndex, _setTabIndex] = useState(0);

  const [sort, _setSort] = useState({
    category: "",
  });

  const [filter, _setFilter] = useState({
    gender: "",
    ratings: "",
  });

  const [stylistData, _setStylistData] = useState([]);
  const [allStylistData, _setAllStylistData] = useState([]);

  useEffect(() => {
    {
      (async () => {
        const response = await getAllInactiveStylist({ sort, filter });
        if (response?.data?.result) {
          _setStylistData(response?.data?.result);
          _setAllStylistData(response?.data?.result);
        }
        _setIsLoading(false);
      })();
    }
  }, []);

  useEffect(() => {
    const data = [...allStylistData];
    const filterStylist = data?.filter((stylist) => {
      return (
        stylist.gender === filter.gender && stylist.avg_rating >= filter.ratings
      );
    });
    _setStylistData(filterStylist);
  }, [filter]);

  useEffect(() => {
    {
      (async () => {
        _setIsLoading(true);
        const apiBody = {
          stylist_type: tabIndex === 0 ? "senior" : "advanced",
        };
        const response = await getAllInactiveStylist(apiBody);
        if (response?.data?.result) {
          _setStylistData(response?.data?.result);
          _setAllStylistData(response?.data?.result);
        }
        _setIsLoading(false);
      })();
    }
  }, [tabIndex]);

  const onFilterSelectHandler = (filter) => {
    _setSort({ category: filter });
    const data = [...allStylistData];
    const sortedStylist = data.sort((a, b) => {
      return b[filter] - a[filter];
    });
    _setStylistData(sortedStylist);
  };

  return (
    <Fragment>
      <Tabs onSelect={(index) => _setTabIndex(index)}>
        <div className="row">
          <div className="col-md-12">
            <div className="heading d-flex justify-content-between flex-wrap mb-3">
              <div className="title d-flex align-items-center">
                <h5>All Stylists </h5>
              </div>
              <div className="filterButtonSection d-flex flex-wrap justify-content-center align-items-center">
                <div className="typeFilteration">
                  <TabList className="react-tabs__tab-list stylist-tabs">
                    <Tab className="react-tabs__tab stylist-tab-content">
                      Senior Stylist
                    </Tab>
                    <Tab
                      className="react-tabs__tab stylist-tab-content"
                      // onSelect={(e, data) => console.log("selected", e, data)}
                    >
                      Advanced Stylist
                    </Tab>
                  </TabList>
                </div>
                <div className="sortButton my-2">
                  <Dropdown className="d-inline mx-2">
                    <Dropdown.Toggle
                      id="dropdown-autoclose-outside"
                      className="stylist-sort-button filter"
                    >
                      <img src={FilterIcon} alt="" className="ml-2" />
                      <span className="ml-2 filter-span">Sort & Filter</span>
                    </Dropdown.Toggle>
                    <Dropdown.Menu className="dropdownMenu filterModal customScroll">
                      <div className="dropdownContainer">
                        <div className="d-flex justify-content-between">
                          <span>Sort Stylist By</span>
                          <Dropdown.Toggle
                            // id="dropdown-autoclose-outside"
                            className="text-dark"
                          >
                            <span className="ml-2">&#10006;</span>
                          </Dropdown.Toggle>
                        </div>
                        <span
                          onClick={() => onFilterSelectHandler("avg_rating")}
                          // onClick={() => _setSort({ category: "avg_rating" })}
                          className={`${
                            sort.category === "avg_rating" && "activeList"
                          }`}
                        >
                          Average Rated
                        </span>
                        <span
                          onClick={() =>
                            onFilterSelectHandler("completed_order_count")
                          }
                          className={`${
                            sort.category === "completed_order_count" &&
                            "activeList"
                          }`}
                        >
                          Completed services
                        </span>
                        <span
                          onClick={() =>
                            onFilterSelectHandler("most_preferred")
                          }
                          className={`${
                            sort.category === "most_preferred" && "activeList"
                          }`}
                        >
                          Most tPreferred
                        </span>
                      </div>
                      <li className="divider"></li>
                      <div className="dropdownContainer">
                        <div className="dropdownListHeader">Filter By</div>
                        <span
                          onClick={() => _setFilter({ gender: "", ratings: 0 })}
                          className={`${filter.gender === "" && "activeList"}`}
                        >
                          None
                        </span>
                        <span
                          className={`${filter.gender !== "" && "activeList"}`}
                        >
                          Gender
                        </span>
                        <div className="filterGenderSection">
                          <span
                            className={`${
                              filter.gender === "men" && "activeList"
                            }`}
                            onClick={() =>
                              _setFilter({ ...filter, gender: "men" })
                            }
                          >
                            Male
                          </span>
                          <span
                            className={`${
                              filter.gender === "women" && "activeList"
                            }`}
                            onClick={() =>
                              _setFilter({ ...filter, gender: "women" })
                            }
                          >
                            Female
                          </span>
                        </div>
                        <div className="dropdownContainer">
                          <span className="d-flex justify-content-between align-items-center">
                            <span>Ratings</span>
                            <span>
                              <select
                                name="Ratings"
                                id="ratings"
                                value={filter.ratings}
                                onChange={(e) =>
                                  _setFilter({
                                    ...filter,
                                    ratings: e.target.value,
                                  })
                                }
                              >
                                <option value={0}>None</option>
                                <option value="3">3+</option>
                                <option value="4">4+</option>
                                <option value="5">5</option>
                              </select>
                            </span>
                          </span>
                        </div>
                      </div>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row position-relative text-center">
          <div className="col-md-12">
            <TabPanel>
              {isLoading ? (
                <div className="row">
                  {[...new Array(6)].map((ele, index) => (
                    <StylistCardSkeleton key={`activeSeniorStylist.${index}`} />
                  ))}
                </div>
              ) : stylistData?.length > 0 ? (
                <div className="row">
                  <StylistCard
                    stylistData={stylistData}
                    stylistCategory="inActive"
                  />
                </div>
              ) : (
                <NoService
                  noServiceText={noServiceText}
                  differentService={differentService}
                />
              )}
            </TabPanel>
            <TabPanel>
              {isLoading ? (
                <div className="row">
                  {[...new Array(6)].map((ele, index) => (
                    <StylistCardSkeleton
                      key={`activeAdvancedStylist.${index}`}
                    />
                  ))}
                </div>
              ) : stylistData?.length > 0 ? (
                <div className="row">
                  <StylistCard
                    stylistData={stylistData}
                    stylistCategory="inActive"
                  />
                </div>
              ) : (
                <NoService
                  noServiceText={noServiceText}
                  differentService={differentService}
                />
              )}
            </TabPanel>
          </div>
        </div>
      </Tabs>
    </Fragment>
  );
};

export default StylistList;
