import React, { Fragment } from "react";
import Active1 from "../../../assets/images/active1.png";
import NavIcon from "../../../assets/icons/nav-icon.svg";
import "./stylistCard.css";
import { useHistory } from "react-router-dom";

const StylistCard = ({ stylistData, stylistCategory }) => {
  const history = useHistory();

  return (
    <Fragment>
      {stylistData &&
        stylistData.map((stylist, index) => {
          return (
            <div
              className="col-md-6 col-lg-4"
              key={index}
              onClick={() =>
                history.push(
                  `/stylist-details/${stylist?._id}/${stylist.experience}/${stylist.firstname}`
                )
              }
            >
              <div className="stylist-card mb-3">
                <div className="stylist-image position-relative">
                  <img
                    src={stylist?.profile}
                    className="imagePlaceholder"
                    alt=""
                  />
                  <i
                    className={`fa fa-circle mr-2 position-absolute ${
                      !stylist.online && "text-secondary"
                    }`}
                    aria-hidden="true"
                  ></i>
                </div>
                <div className="stylist-content ml-3">
                  <div className="stylist-name">
                    <h5>{`${stylist.firstname} ${stylist.lastname}`}</h5>
                  </div>
                  <div className="stylist-rating">
                    <h6>
                      <i className="fa fa-star-o" aria-hidden="true"></i>{" "}
                      {Math.round(stylist?.rating_count)}{" "}
                      <span>
                        {stylist?.review} {stylist?.review ? " Reviews" : ""}
                      </span>
                    </h6>
                  </div>
                </div>
                {/* <div className="stylist-distance">
                  <img src={NavIcon} alt="" className="text-center" />
                  <i className="fa fa-location-arrow" aria-hidden="true"></i>
                  <span>{item.miles} Miles</span>
                </div> */}
              </div>
            </div>
          );
        })}
    </Fragment>
  );
};

export default StylistCard;
