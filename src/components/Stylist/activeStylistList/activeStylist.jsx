import React, { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { Dropdown } from "react-bootstrap";

import "react-tabs/style/react-tabs.css";
import "react-loading-skeleton/dist/skeleton.css";

import NoService from "../../Services/noServices/noServices";
import StylistCard from "../stylistCard/stylistCard";
import Header from "../../DynamicHeader/Header";
import { getAllActiveStylist } from "../../../api/stylist.api";

import FilterIcon from "../../../assets/icons/filter.svg";
import { StylistCardSkeleton } from "../../StylistSkeleton";

const ActiveStylistCard = (props) => {
  const exp = ["senior", "advanced"];
  const sortings = [
    "rating_count",
    "stylist_fav_count",
    "completed_order_count",
  ];
  const gender = ["men", "women"];
  const ratings = [3, 4, 5];
  const noServiceText = "No Active Stylists In your Location";
  const differentService =
    "Try Advanced Stylists or Change Your Chosen Location";

  const [isLoading, _setIsLoading] = useState(true);
  const [allActiveStylist, _setAllActiveStylist] = useState([]);
  const [seniorStylist, _setSeniorStylist] = useState({
    allSeniorStylist: [],
    filterdSeniorStylist: [],
  });
  const [advancedStylist, _setAdvancedStylist] = useState({
    allAdvancedStylist: [],
    filteredAdvancedStylist: [],
  });

  const [sort, _setSort] = useState({
    category: null,
  });

  const [filter, _setFilter] = useState({
    gender: null,
    ratings: null,
  });

  useEffect(() => {
    {
      (async () => {
        const response = await getAllActiveStylist({ sort, filter });
        if (response?.data?.result) {
          filterByExperience("senior", response?.data?.result, true);
          filterByExperience("advanced", response?.data?.result, true);
          _setAllActiveStylist(response?.data?.result);
          _setIsLoading(false);
        }
      })();
    }
  }, []);

  useEffect(() => {
    if (
      (seniorStylist.allSeniorStylist.length > 0 ||
        advancedStylist.allAdvancedStylist.length > 0) &&
      !filter.ratings &&
      !filter.gender
    ) {
      _setSeniorStylist({
        ...seniorStylist,
        filterdSeniorStylist: seniorStylist.allSeniorStylist,
      });
      _setAdvancedStylist({
        ...advancedStylist,
        filteredAdvancedStylist: advancedStylist.allAdvancedStylist,
      });
    }
    if (filter.ratings || filter.gender) {
      const newSeniorStylist = seniorStylist.allSeniorStylist?.filter(
        (stylist) => {
          if (filter.ratings && filter.gender === null)
            return stylist.rating_count >= filter.ratings;
          return (
            stylist.gender === filter?.gender &&
            stylist.rating_count >= filter.ratings
          );
        }
      );
      const newAdvancedstylist = advancedStylist.allAdvancedStylist?.filter(
        (stylist) => {
          return (
            stylist.gender === filter?.gender &&
            stylist.rating_count >= filter.ratings
          );
        }
      );
      _setSeniorStylist({
        ...seniorStylist,
        filterdSeniorStylist: newSeniorStylist,
      });
      _setAdvancedStylist({
        ...advancedStylist,
        filteredAdvancedStylist: newAdvancedstylist,
      });
    }
  }, [filter]);

  const onFilterSelectHandler = (filter) => {
    _setSort(filter);
    const newSeniorStylistData = [...seniorStylist.filterdSeniorStylist];
    const newAdvancedStylistData = [...advancedStylist.filteredAdvancedStylist];
    const sortedSeniorStylistData = newSeniorStylistData.sort((a, b) => {
      return b[filter.category] - a[filter.category];
    });
    const sortedAdvancedStylistData = newAdvancedStylistData.sort((a, b) => {
      return b[filter.category] - a[filter.category];
    });
    _setSeniorStylist({
      ...seniorStylist,
      filterdSeniorStylist: sortedSeniorStylistData,
    });
    _setAdvancedStylist({
      ...advancedStylist,
      filteredAdvancedStylist: sortedAdvancedStylistData,
    });
  };

  const filterByExperience = (expLevel, stylistArray, isReset) => {
    const filteredData = stylistArray.filter(
      (stylist) => stylist.experience === expLevel
    );
    if (isReset) {
      if (expLevel === "senior")
        _setSeniorStylist({
          allSeniorStylist: filteredData,
          filterdSeniorStylist: filteredData,
        });
      if (expLevel === "advanced")
        _setAdvancedStylist({
          allAdvancedStylist: filteredData,
          filteredAdvancedStylist: filteredData,
        });
    } else {
      if (expLevel === "senior")
        _setSeniorStylist({
          ...seniorStylist,
          filterdSeniorStylist: filteredData,
        });
      if (expLevel === "advanced")
        _setAdvancedStylist({
          ...advancedStylist,
          filteredAdvancedStylist: filteredData,
        });
    }
  };

  return (
    <Fragment>
      <Header {...props} />
      <div className="container pb-5">
        <Tabs defaultIndex={0}>
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/">Home</Link>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                Stylist
              </li>
            </ol>
          </nav>
          <div className="row pb-4 pt-3">
            <div className="col-md-12">
              <div className="heading d-flex justify-content-between flex-wrap">
                <div className="title d-flex align-items-center">
                  <h5>Active Stylists </h5>
                  <h6>
                    <i className="fa fa-circle mr-2" aria-hidden="true"></i>
                    {allActiveStylist?.length > 0 && allActiveStylist?.length}
                    {` +`}
                  </h6>
                </div>
                <div className="filterButtonSection d-flex flex-wrap justify-content-center align-items-center">
                  <div className="typeFilteration">
                    <TabList className="react-tabs__tab-list stylist-tabs">
                      <Tab className="react-tabs__tab stylist-tab-content">
                        Senior Stylist
                      </Tab>
                      <Tab className="react-tabs__tab stylist-tab-content">
                        Advanced Stylist
                      </Tab>
                    </TabList>
                  </div>
                  <div className="sortButton my-2">
                    <Dropdown className="d-inline mx-2">
                      <Dropdown.Toggle
                        id="dropdown-autoclose-outside"
                        className="stylist-sort-button filter"
                      >
                        <img src={FilterIcon} alt="" className="ml-2" />
                        <span className="ml-2 filter-span">Sort & Filter</span>
                      </Dropdown.Toggle>
                      <Dropdown.Menu className="dropdownMenu customScroll">
                        <div className="dropdownContainer">
                          <div className="dropdownListHeader d-flex justify-content-between">
                            <span>Sort Stylist By</span>
                            <Dropdown.Toggle
                              id="dropdown-autoclose-outside"
                              className="text-dark"
                            >
                              <span className="ml-2">&#10006;</span>
                            </Dropdown.Toggle>
                          </div>
                          <span
                            onClick={() =>
                              onFilterSelectHandler({
                                category: "rating_count",
                              })
                            }
                            className={`${
                              sort.category == "rating_count" && "activeList"
                            }`}
                          >
                            Top Rated
                          </span>
                          <span
                            onClick={() =>
                              onFilterSelectHandler({
                                category: "completed_order_count",
                              })
                            }
                            className={`${
                              sort.category == "completed_order_count" &&
                              "activeList"
                            }`}
                          >
                            No service completed
                          </span>
                          <span
                            onClick={() =>
                              onFilterSelectHandler({
                                category: "stylist_fav_count",
                              })
                            }
                            className={`${
                              sort.category === "stylist_fav_count" &&
                              "activeList"
                            }`}
                          >
                            Most Prefered
                          </span>
                        </div>
                        <li className="divider"></li>
                        <div className="dropdownContainer">
                          <div className="dropdownListHeader">Filter By</div>
                          <span
                            onClick={() =>
                              _setFilter({ gender: null, ratings: null })
                            }
                            className={`${
                              filter.gender === null &&
                              filter.ratings === null &&
                              "activeList"
                            }`}
                          >
                            None
                          </span>
                          <span
                            className={`${
                              filter.gender !== null && "activeList"
                            }`}
                          >
                            Gender
                          </span>
                          <div className="filterGenderSection">
                            <span
                              className={`${
                                filter.gender === "men" && "activeList"
                              }`}
                              onClick={() =>
                                _setFilter({ ...filter, gender: "men" })
                              }
                            >
                              Male
                            </span>
                            <span
                              className={`${
                                filter.gender === "women" && "activeList"
                              }`}
                              onClick={() =>
                                _setFilter({ ...filter, gender: "women" })
                              }
                            >
                              Female
                            </span>
                          </div>
                          <div className="dropdownContainer">
                            <span
                              className={`d-flex justify-content-between align-items-center ${
                                filter.ratings !== "" && "activeList"
                              }`}
                            >
                              <span>Ratings</span>
                              <span>
                                <select
                                  name="Ratings"
                                  id="ratings"
                                  value={filter.ratings}
                                  onChange={(e) =>
                                    _setFilter({
                                      ...filter,
                                      ratings: e.target.value,
                                    })
                                  }
                                >
                                  <option value="0">None</option>
                                  <option value="3">3+</option>
                                  <option value="4">4+</option>
                                  <option value="5">5</option>
                                </select>
                              </span>
                            </span>
                          </div>
                        </div>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row position-relative">
            <div className="col-md-12">
              {/* <TabList className="react-tabs__tab-list stylist-tabs">
                                <Tab className="react-tabs__tab stylist-tab-content">Senior Stylist</Tab>
                                <Tab className="react-tabs__tab stylist-tab-content">Advanced Stylist</Tab>
                            </TabList> */}
              <TabPanel>
                {isLoading ? (
                  // <div className="text-center">
                  //   <HashLoader />
                  // </div>
                  <div className="row">
                    {[...new Array(6)].map((ele, index) => (
                      <StylistCardSkeleton
                        key={`activeSeniorStylist.${index}`}
                      />
                    ))}
                  </div>
                ) : seniorStylist.filterdSeniorStylist?.length > 0 ? (
                  <div className="row">
                    <StylistCard
                      stylistData={seniorStylist.filterdSeniorStylist}
                      stylistCategory="activeStylist"
                    />
                  </div>
                ) : (
                  <NoService
                    noServiceText={noServiceText}
                    differentService={differentService}
                  />
                )}
              </TabPanel>
              <TabPanel>
                {isLoading ? (
                  <div className="row">
                    {[...new Array(6)].map((ele, index) => (
                      <StylistCardSkeleton
                        key={`activeAdvancedStylist.${index}`}
                      />
                    ))}
                  </div>
                ) : advancedStylist.filteredAdvancedStylist?.length > 0 ? (
                  <div className="row">
                    <StylistCard
                      stylistData={advancedStylist.filteredAdvancedStylist}
                      stylistCategory="activeStylist"
                    />
                  </div>
                ) : (
                  <NoService
                    noServiceText={noServiceText}
                    differentService={differentService}
                  />
                )}
              </TabPanel>
            </div>
          </div>
        </Tabs>
      </div>
    </Fragment>
  );
};

export default ActiveStylistCard;
