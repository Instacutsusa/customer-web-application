import React, { Component, Fragment } from 'react';
import DemoImg from '../../assets/images/download.png';
// import FilterIcon from '../../assets/images/icon.png';
import FilterIcon from '../../assets/icons/filter.svg';
import './categories.css';
import Slider from "react-slick";

class Categories extends Component {
    state={
        showFilter: false
    }

    handleShowFilter = () => {
        this.setState(prevState => ({
            showFilter: !prevState.showFilter
          }))
    }
    
    render() {
        const { showFilter} = this.state;
        const { activeCat } = this.props;
        var settings = {
            dots: false,
            infinite: false,
            speed: 500,
            arrows: false,
            slidesToShow: 5,
            slidesToScroll: 1,
            responsive: [
                {
                  breakpoint: 1200,
                  settings: {
                    slidesToShow: 5,
                    slidesToScroll: 2
                  }
                },
                {
                  breakpoint: 991,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 767,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }

              ]
          };

        return(
            <Fragment>
                <div className="row py-4">
                    <div className="col-md-12">
                        <div className="heading d-flex justify-content-between">
                            <div className="title">
                                <h5>Categories</h5>
                            </div>
                            <div className="filter pr-4">
                                <i className="fa fa-question-circle-o mr-2" aria-hidden="true"></i>
                                Junior <img src={FilterIcon} alt="" className="ml-2" onClick={this.handleShowFilter} onBlur={() => this.setState({showFilter: false})}/>
                                <div className={showFilter ? "filter-actions" : "filter-actions d-none" }>
                                    <h5>Select Stylist Level</h5>
                                    <ul>
                                        <li>All</li>
                                        <li className="active">Junior</li>
                                        <li>Senior</li>
                                        <li>Advanced</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* <Carousel responsive={responsive} draggable={false} arrows={false} className="pb-5 categories-carousel"> */}
                <Slider {...settings} className="pb-5">
                    <div className={activeCat ? "categories-list active": "categories-list" } onClick={this.props.handleActiveCat}>
                        <img src={DemoImg} alt="" />
                        { activeCat ? <i className="fa fa-check-circle position-absolute" aria-hidden="true"></i> : "" }
                        <span>Hair</span>
                    </div>
                    <div className="categories-list" onClick={this.handleActiveCat}>
                        <img src={DemoImg} alt="" />
                        <span>Face</span>
                    </div>
                    <div className="categories-list">
                        <img src={DemoImg} alt="" />
                        <span>Body</span>
                    </div>
                    <div className="categories-list">
                        <img src={DemoImg} alt="" />
                        <span>Nail</span>
                    </div>
                    <div className="categories-list">
                        <img src={DemoImg} alt="" />
                        <span>Hair</span>
                    </div>
                    <div className="categories-list">
                        <img src={DemoImg} alt="" />
                        <span>Hair</span>
                    </div>
                    <div className="categories-list">
                        <img src={DemoImg} alt="" />
                        <span>Hair</span>
                    </div>
                </Slider>
            </Fragment>
        );
    }
}

export default Categories;