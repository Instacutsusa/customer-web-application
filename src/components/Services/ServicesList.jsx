import React, { Component, Fragment } from 'react';
import FilterIcon from '../../assets/icons/filter.svg';
import './sevices.css';
import NoService from './noServices/noServices';
import ServicesCard from './ServicesCard/ServicesCard';

class ServicesList extends Component {
    state = {
        services: [
                {
                    title: "Men's Hair Cut",
                    type: "Junior Stylist",
                    newPice: "$60.00",
                    oldPrice: "$90.00",
                    quantity: 0,
                    rating: '4.9',
                    img: '/assets/images/Thumbnail.png'
                },
                {
                    title: "Men's Hair Cut",
                    type: "Junior Stylist",
                    newPice: "$60.00",
                    oldPrice: "$90.00",
                    quantity: 0,
                    rating: '4.9',
                    img: '../../assets/images/Thumbnail.png'
                },
                {
                    title: "Men's Hair Cut",
                    type: "Junior Stylist",
                    newPice: "$60.00",
                    oldPrice: "$90.00",
                    quantity: 0,
                    rating: '4.9',
                    img: '../../assets/images/Thumbnail.png'
                },
                {
                    title: "Men's Hair Cut",
                    type: "Junior Stylist",
                    newPice: "$60.00",
                    oldPrice: "$90.00",
                    quantity: 0,
                    rating: '4.9',
                    img: '../../assets/images/Thumbnail.png'
                }
        ],
        noServiceText: "No Services Available In your Location",
        differentService: "Try Different Service or Change Your Chosen Location",
        quantity: 0,
        showFilter: false
    }

    handleQuantity = () => {
        this.setState({
            quantity: this.state.quantity + 1
        })
    }

    handleShowFilter = () => {
        this.setState(prevState => ({
            showFilter: !prevState.showFilter
          }))
    }

    render() {
        const { services, quantity, differentService, noServiceText, showFilter } = this.state;
        return(
            <Fragment>
                <div className="row">
                    <div className="col-md-12">
                        <div className="heading d-flex justify-content-between position-relative">
                            <div className="title">
                                <h5>Services</h5>
                            </div>
                            <div className="filter pr-4">
                                show all <img src={FilterIcon} alt="" className="ml-2" onClick={this.handleShowFilter} onBlur={() => this.setState({showFilter: false})}/>
                                <div className={showFilter ? "filter-actions" : "filter-actions d-none" }>
                                    <h5>Filter Services</h5>
                                    <ul>
                                        <li>Show All</li>
                                        <li className="active">Hair Cut</li>
                                        <li>Hair Coloring</li>
                                        <li>Hair Styling</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                { services && services.length ? <div className="row">
                    <div className="col-md-12">
                        <div className="services-list d-flex justify-content-between">
                            <div className="row">
                                <ServicesCard servicesData={services} quantity={quantity} handleQuantity={this.handleQuantity}/>
                            </div>
                        </div>
                    </div>
                </div> : <NoService noServiceText={noServiceText} differentService={differentService}/> }
            </Fragment>
        );
    }
}

export default ServicesList;