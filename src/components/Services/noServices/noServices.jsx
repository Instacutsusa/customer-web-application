import React from 'react';
import NoServiceImage from '../../../assets/images/Illustration.png'
import { Link } from 'react-router-dom';
import './noservice.css';

const NoService = (props) => {
    return (
        <div className="row">
            <div className="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                <div className="no-service-container text-center">
                    {/* <div className="img-container">
                        <img src={NoServiceImage} alt="" />
                    </div> */}
                    <div className="content">
                        <h5>{props.noServiceText}</h5>
                        <h6>{props.differentService}</h6>
                        <Link to="/">Back To Stylist list</Link>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default NoService;