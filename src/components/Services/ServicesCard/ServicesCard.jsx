import React, { Fragment } from "react";
import ImgThumb from "../../../assets/images/Thumbnail.png";
import PlusIcon from "../../../assets/icons/plus.svg";

const ServicesCard = (props) => {
  return (
    <Fragment>
      {props.servicesData &&
        props.servicesData.map((item, index) => {
          return (
            <div className="col-md-6 col-lg-4" key={index}>
              <div className="services-item d-flex">
                <div className="services-img position-relative">
                  {item?.featured_image ? (
                    <img
                      src={`${process.env.REACT_APP_IMAGE_BASE_URL}${item.featured_image}`}
                      alt=""
                    />
                  ) : (
                    <img src={ImgThumb} alt="" />
                  )}
                  <h6>
                    <i className="fa fa-clock-o" aria-hidden="true"></i> 50
                  </h6>
                </div>
                <div className="services-list-details">
                  <div className="service-title">
                    <h5>{item.title}</h5>
                  </div>
                  <div className="stylist-type d-flex">
                    <h6>{item?.stylist_type?.[0]} Stylist</h6>
                    <div className="stylist-rating ml-2">
                      <h5>
                        {item.is_fav ? (
                          <i
                            className="fa fa-star"
                            aria-hidden="true"
                            style={{ color: "red" }}
                          ></i>
                        ) : (
                          <i className="fa fa-star-o" aria-hidden="true"></i>
                        )}
                        {item?.rating}
                      </h5>
                    </div>
                  </div>
                  <div className="services-price d-flex justify-content-between">
                    <div className="d-flex">
                      <h4>${item?.sale_price}</h4>
                      <span>${item?.regular_price}</span>
                    </div>
                    <div className="counter">
                      {props.quantity > 0 ? (
                        <i className="fa fa-minus mr-1" aria-hidden="true"></i>
                      ) : (
                        ""
                      )}
                      {props.quantity > 0 ? props.quantity : "Add"}
                      {/* <i className="fa fa-plus ml-1" aria-hidden="true" ></i> */}
                      <img
                        src={PlusIcon}
                        alt=""
                        onClick={props.handleQuantity}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
    </Fragment>
  );
};

export default ServicesCard;
