import { usePagination, DOTS } from "./PaginationReducer";

import "./pagination.css";

const PaginationBlock = ({
  currentPage,
  totalCount,
  pageSize,
  onPageChange,
  siblingCount = 1,
}) => {
  const paginationRange = usePagination({
    currentPage,
    totalCount,
    siblingCount,
    pageSize,
  });

  // If there are less than 2 times in pagination range we shall not render the component
  if (currentPage === 0 || paginationRange?.length < 2) {
    return null;
  }

  const onNext = () => {
    onPageChange(currentPage + 1);
  };

  const onPrevious = () => {
    onPageChange(currentPage - 1);
  };

  let lastPage = paginationRange[paginationRange.length - 1];
  return (
    <div className="w-100 d-inline-flex justify-content-center">
      <ul className="paginationButtonContainer">
        <li className="page-button" onClick={() => onPrevious()}>
          <a className="page-button-number" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span className="sr-only">Previous</span>
          </a>
        </li>
        {paginationRange.map((pageNumber, index) => {
          if (pageNumber === "DOTS") {
            return (
              <li className="page-button" key={index}>
                <a className="page-button-number">...</a>
              </li>
            );
          }
          return (
            <li
              key={index}
              onClick={() => onPageChange(pageNumber)}
              className={`page-button ${
                pageNumber === currentPage && "activeButton"
              }`}
            >
              <a className="page-button-number">{pageNumber}</a>
            </li>
          );
        })}
        <li className="page-button" onClick={() => onNext()}>
          <a className="page-button-number" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span className="sr-only">Next</span>
          </a>
        </li>
      </ul>
    </div>
  );
};

export default PaginationBlock;
