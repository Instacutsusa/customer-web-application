import React, { Component, Fragment } from "react";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";

import DemoImage from "../../assets/images/Image.png";
import InstacutsIcons from "../../assets/images/instacuts.png";
import "./profile.css";
import Modal from "react-bootstrap/Modal";
import { Calendar } from "react-date-range";
import StarIcon from "../../assets/icons/star.svg";
import EditIcon from "../../assets/icons/edit.svg";
import DeleteIcon from "../../assets/icons/delete-icon.svg";
import DeleteColorIcon from "../../assets/icons/delete.svg";
import { NotificationAlert } from "../../common/NotificationAlert";
import {
  NOTIFICATIONSMESSAGE,
  NOTIFICATION_TYPE,
} from "../../_constant/notificationsMessage";

import {
  addCustomerFamilyMember,
  deleteCustomerFamilyMember,
  getCustomerProfile,
  markCustomerFamilyMember,
  updateCFamilyMemberProfile,
} from "../../api/profile.api";
import axios from "axios";
import { API_URL } from "../../api/constant";
import { getHeaders } from "../../api";
import { getCustomerRelation } from "../../_utils/getCustomerRelation";
import { userProfile } from "../../redux/actions";

class Profile extends Component {
  state = {
    data: [],
    isOpen: false,
    uploadPicture: false,
    imageUploaded: false,
    profileCreated: false,
    editProfile: false,
    lname: "",
    fname: "",
    dateofbirth: "",
    relation: "",
    editModal: false,
    showDropDown: false,
    date: new Date(),
    showDatePicker: false,
    overlayDrop: false,
    profileImage: null,
    default_profile_image: null,
    editId: null,
    isLoader: false,
    isUploadBtn: true,
    relationOptions: [],
  };

  componentDidMount = async () => {
    this.loadAllFamilyMember();
  };

  loadAllFamilyMember = async () => {
    try {
      const profileData = [];
      let default_profile;

      this.props?.profiles?.map((profile) => {
        if (profile.default_profile) default_profile = profile.profile;
        return profileData.push({
          name: profile.firstname + " " + profile.lastname,
          accountHolder: profile.default_profile,
          selected: profile.default_profile,
          ...profile,
        });
      });

      this.setState({
        ...this.state,
        data: profileData,
        default_profile_image: default_profile,
      });
    } catch (err) {
      console.log("err", err);
    }
  };

  handleModal = (value, e, id) => {
    switch (value) {
      case "add":
        this.setState({
          isOpen: true,
          editModal: false,
          isUploadBtn: false,
        });
        return;

      case "edit":
        this.loadEditModelFieldValue(id);
        this.setState({
          isOpen: true,
          editModal: true,
          isUploadBtn: false,
          editId: id,
        });
        return;

      case "close":
        this.setState({
          isOpen: false,
          showDropDown: false,
        });
        return;

      default:
        return;
    }
  };

  onHandleDropdown = () => {
    this.setState({
      showDropDown: !this.state.showDropDown,
    });
  };

  handleUploadClick = () => {
    this.setState({
      uploadPicture: true,
    });
  };

  handleUploadClose = () => {
    this.setState({
      uploadPicture: false,
    });
  };

  handleImageUpload = (e) => {
    const { isUploadBtn } = this.state;
    this.setState({
      profileImage: e.target.files[0],
      isLoader: isUploadBtn,
      uploadPicture: isUploadBtn,
    });
    // Call API to pdate the profil image
    if (isUploadBtn) this.onUpdateCustomerProfile(e.target.files[0]);
  };

  onChangeHandler = (e) => {
    const { name } = e.target;
    this.setState({
      [name]: e.target.value,
    });
  };

  handleShowDatePicker = () => {
    this.setState({
      showDatePicker: !this.state.showDatePicker,
      overlayDrop: true,
    });
  };

  handleOverlay = () => {
    this.setState({
      overlayDrop: !this.state.overlayDrop,
      showDatePicker: false,
    });
  };

  // USED TO ADD NEW FAMILY MEMBER
  onAddFamilyMemberhandler = async (e) => {
    try {
      e.preventDefault();
      this.setState({
        ...this.state,
        isLoader: true,
      });
      const { fname, lname, date, relation, profileImage, editModal } =
        this.state;

      if (editModal) return this.onEditProfileHandler();

      const apiBody = {
        firstname: fname,
        lastname: lname,
        dob: date,
        relation,
        family_member_profile: profileImage,
      };
      const { message } = await addCustomerFamilyMember(apiBody);
      // empty the input field after successfully added the member
      this.resetUserInputField();
      this.loadAllFamilyMember();
      NotificationAlert(message, NOTIFICATION_TYPE.SUCCESS);
    } catch (err) {
      console.log(err);
      NotificationAlert(err.response.data.message, NOTIFICATION_TYPE.ERROR);
    } finally {
      this.setState({
        ...this.state,
        isLoader: false,
        isOpen: false,
      });
    }
  };

  // USED TO RESET USER FIELD AFTER ADD & EDIT PROFILE
  resetUserInputField = () => {
    this.setState({
      ...this.state,
      fname: "",
      lname: "",
      date: new Date(),
      relation: "",
      profileImage: null,
      editId: "",
    });
  };

  /**
   * USED TO SET VALUES IN INPUT FIELD TO LOAD IN EDIT MODEL
   * @param {String} id member_id which we want to update
   */
  loadEditModelFieldValue = (id) => {
    const { data } = this.state;
    const editData = data.find((profile) => profile._id === id);

    this.setState({
      ...this.state,
      fname: editData.firstname,
      lname: editData.lastname,
      date: editData.dob,
      relation: editData.relation,
      profile: editData.profile,
    });
    this.getMemberRelation(null, editData.dob);
  };

  //  Used for to delete family member
  onDeleteFamilyMember = async (id) => {
    try {
      const apiBody = { member_id: id };
      const { message } = await deleteCustomerFamilyMember(apiBody);
      this.loadAllFamilyMember();
      NotificationAlert(message, "success");
    } catch (err) {
      console.log(err);
    }
  };

  /**
   * Used to set profile as default
   * @param {String} member_id
   */
  onMakeProfileDefault = async (member_id) => {
    try {
      const apiBody = {
        member_id,
      };
      const { message, data } = await markCustomerFamilyMember(apiBody);
      this.setState({
        ...this.state,
        default_profile_image: data.profile,
      });
      NotificationAlert(message, "success");
      this.loadAllFamilyMember();
    } catch (err) {
      console.log(err);
    }
  };

  /**
   * Used to update customer profile image
   * @param {FILE} profile - customer profile Image
   */
  onUpdateCustomerProfile = async (profile) => {
    try {
      const formData = new FormData();
      formData.append("profile", profile, profile.name);

      const headers = {
        headers: getHeaders(),
      };

      await axios.post(
        `${API_URL}/customer/c-update-profile-image`,
        formData,
        headers
      );

      NotificationAlert(
        NOTIFICATIONSMESSAGE.UPDATE_CUSTOMER_PROFILE,
        "success"
      );
    } catch (err) {
      NotificationAlert(err.response.data.message, "error");
    } finally {
      this.setState({
        ...this.state,
        isLoader: false,
        uploadPicture: false,
      });
    }
  };

  // Calculate Memeber age and display relations
  getMemberRelation = async (e, date) => {
    const value = e ? e.target.value : date;

    const age = await getCustomerRelation(new Date(value));
    // If age > 18 then relation is ["Wife", "Husband", "Mother", "Father"]
    // If age < 18 then relation is ["Daughter", "Son"]
    const relationOptions = [];
    if (age > 18) {
      relationOptions.push("Wife", "Husband", "Mother", "Father");
    }
    if (age <= 18) {
      relationOptions.push("Daughter", "Son");
    }
    this.setState({
      ...this.state,
      relationOptions,
    });
  };

  // Redirect to Address Page
  onRedirectToAddressPage = () => {
    this.props.history.push("/address");
  };

  //  UPDATE USER PROFILE INFORMATIONS
  onEditProfileHandler = async () => {
    try {
      const { fname, lname, date, relation, profileImage, editId } = this.state;

      const apiBody = {
        member_id: editId,
        firstname: fname,
        lastname: lname,
        dob: date,
        relation,
        family_member_profile: profileImage,
      };
      const editValue = await updateCFamilyMemberProfile(apiBody);
      // this.resetUserInputField()
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const {
      data,
      isOpen,
      uploadPicture,
      imageUploaded,
      profileCreated,
      editProfile,
      fname,
      lname,
      showDatePicker,
      editModal,
      date,
      overlayDrop,
      profileImage,
      default_profile_image,
      isLoader,
      relationOptions,
    } = this.state;

    return (
      <Fragment>
        <span
          className={
            overlayDrop === true
              ? "screen-overlay zindex-overlay show"
              : "screen-overlay zindex-overlay"
          }
          onClick={this.handleOverlay}
        ></span>
        <div className="container">
          <div className="row">
            <div className="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
              <div className="profile-container text-center">
                <div className="profile-image d-flex justify-content-center">
                  <div className="img-container position-relative">
                    <img
                      src={
                        default_profile_image
                          ? default_profile_image
                          : DemoImage
                      }
                      alt="profile"
                    />
                    <i
                      className="fa fa-pencil-square-o"
                      aria-hidden="true"
                      onClick={this.handleUploadClick}
                    ></i>
                  </div>
                </div>
                <div className="title mt-4 mb-4">
                  <h4>Select Default Profile</h4>
                  <h6>Customize Your Experience</h6>
                </div>
                <div className="profile-list">
                  {data.map((item, index) => {
                    return (
                      <div
                        className="profile-item d-flex justify-content-between align-items-center mt-3"
                        key={index}
                      >
                        <div className="d-flex align-items-center">
                          <div className="profile-item-image">
                            <img
                              src={item?.profile ? item.profile : DemoImage}
                              alt="profile"
                            />
                          </div>
                          <div className="profile-item-name ml-3">
                            <h5>{item.name}</h5>
                            <h6>
                              {item.accountHolder
                                ? "Account Holder"
                                : "Family Member"}
                            </h6>
                          </div>
                        </div>
                        <div className="d-flex">
                          {item.selected ? (
                            <div className="profile-selection">
                              <span>
                                <i
                                  className="fa fa-check"
                                  aria-hidden="true"
                                ></i>{" "}
                                Selected
                              </span>
                            </div>
                          ) : (
                            ""
                          )}
                          <div className="profile-item-actions ml-3">
                            <i
                              className="fa fa-ellipsis-h"
                              aria-hidden="true"
                            ></i>
                            <div className="profile-actions">
                              <ul>
                                {!item.selected && (
                                  <li
                                    onClick={() =>
                                      this.onMakeProfileDefault(item._id)
                                    }
                                  >
                                    {/* <i className="fa fa-star-o" aria-hidden="true"></i>  */}
                                    <img
                                      src={StarIcon}
                                      alt=""
                                      className="mr-3"
                                    />
                                    Set as Default
                                  </li>
                                )}
                                <li
                                  onClick={(e) =>
                                    this.handleModal("edit", e, item._id)
                                  }
                                >
                                  {/* <i className="fa fa-pencil" aria-hidden="true"></i>  */}
                                  <img src={EditIcon} alt="" className="mr-3" />
                                  Edit Profile
                                </li>
                                {!item.selected && (
                                  <li
                                    onClick={() =>
                                      this.onDeleteFamilyMember(item._id)
                                    }
                                  >
                                    {/* <i className="fa fa-trash-o" aria-hidden="true"></i>  */}
                                    <img
                                      src={DeleteIcon}
                                      alt=""
                                      className="mr-3"
                                    />
                                    Delete
                                  </li>
                                )}
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
                <button
                  className="btn btn-primary add-member"
                  onClick={(e) => this.handleModal("add", e)}
                >
                  {/* <img src={AddIcon} alt="" className="mr-2" /> */}
                  <i className="fa fa-user-plus mr-2" aria-hidden="true"></i>
                  Add Family Member
                </button>
                <button
                  className="btn btn-primary continue-btn"
                  onClick={this.onRedirectToAddressPage}
                >
                  Continue
                  <i
                    className="fa fa-long-arrow-right ml-2"
                    aria-hidden="true"
                  ></i>
                </button>
              </div>
            </div>
          </div>
        </div>
        <Modal
          show={isOpen}
          onHide={(e) => this.handleModal("close", e)}
          className="add-profile-modal"
        >
          <Modal.Header closeButton>
            {/* <Modal.Title></Modal.Title> */}
          </Modal.Header>
          <Modal.Body>
            <div className="profile-container p-0">
              <h2 className="mb-5">
                {!editModal ? "Add Family Member" : "Edit Family Member"}
              </h2>
              <div className="profile-image d-flex justify-content-center">
                <div className="img-container position-relative">
                  <img
                    src={
                      profileImage
                        ? URL.createObjectURL(profileImage)
                        : DemoImage
                    }
                    alt="profile"
                  />
                  <i
                    className="fa fa-pencil-square-o"
                    aria-hidden="true"
                    onClick={this.handleUploadClick}
                  ></i>
                </div>
              </div>
              <div className="title mt-4 mb-4">
                <h4>Select Default Profile</h4>
                <h6>Customize Your Experience</h6>
              </div>
            </div>
            <div className="signin-container p-0 mt-5 mb-0">
              <form className="form-container mt-0">
                <div className="form-row">
                  <div className="form-group mb-4 col-md-6">
                    <label>First Name</label>
                    <input
                      type="text"
                      className="form-control"
                      name="fname"
                      id="firstName"
                      value={fname}
                      onChange={this.onChangeHandler}
                      placeholder="First Name"
                      autoComplete="off"
                    />
                  </div>
                  <div className="form-group mb-4 col-md-6">
                    <label>Last Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="lastName"
                      name="lname"
                      value={lname}
                      onChange={this.onChangeHandler}
                      placeholder="Last Name"
                      autoComplete="off"
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group select-gender mb-4 col-md-6 position-relative">
                    <label>Date of Birth</label>
                    <input
                      type="date"
                      className="form-control pr-5"
                      id="date"
                      name="date"
                      value={new Date(date).toISOString().split("T")[0]}
                      onChange={(e) => {
                        this.onChangeHandler(e);
                        this.getMemberRelation(e);
                      }}
                      placeholder="Select Date"
                      autoComplete="off"
                    />
                  </div>
                  <div className="form-group select-gender mb-4 col-md-6 position-relative">
                    <label>Relation</label>
                    <select
                      name="relation"
                      // value="wife"
                      onChange={this.onChangeHandler}
                    >
                      <option>Select Relation</option>
                      {relationOptions.length &&
                        relationOptions.map((relation) => {
                          return (
                            <option value={relation.toLowerCase()}>
                              {relation}
                            </option>
                          );
                        })}
                    </select>
                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                  </div>
                </div>
                <div className="d-flex align-items-center">
                  {editModal ? (
                    <img
                      src={DeleteColorIcon}
                      alt=""
                      className="delete-profile position-relative"
                      onClick={() => {
                        this.handleModal("close");
                        this.onDeleteFamilyMember(this.state.editId);
                      }}
                    />
                  ) : (
                    " "
                  )}
                  <button
                    className="btn btn-primary continue-btn"
                    onClick={this.onAddFamilyMemberhandler}
                  >
                    {isLoader && <i className="fa fa-spinner fa-spin"></i>} save
                    Changes
                  </button>
                </div>
              </form>
            </div>
          </Modal.Body>
        </Modal>

        {uploadPicture && (
          <div className="upload-image-modal">
            <div className="upload-image-container">
              <div className="upload-image-header mt-2">
                <h5>Capture Photo</h5>
                <i
                  className="fa fa-times"
                  aria-hidden="true"
                  onClick={this.handleUploadClose}
                ></i>
              </div>
              {imageUploaded ? (
                <div className="content mb-4 mt-2">
                  <p>
                    Guidelines: <br /> Don't Hide Any Part Of The Face Photo
                    Must Be Captured Under Good Light. You'll Be Banned If The
                    Photo Is Not Yours.
                  </p>
                </div>
              ) : (
                <div className="uploaded-image">
                  <img
                    src={
                      profileImage
                        ? URL.createObjectURL(profileImage)
                        : default_profile_image
                    }
                    alt=""
                    height="52px"
                    width="52px"
                  />
                  <h4>
                    {profileImage?.name ? profileImage.name : "Image Name"}
                  </h4>
                </div>
              )}
              <div className="upload-btn">
                <label htmlFor="file">
                  <h5>
                    {isLoader && <i className="fa fa-spinner fa-spin"></i>}
                    {!isLoader && (
                      <i className="fa fa-camera" aria-hidden="true"></i>
                    )}{" "}
                    Upload Picture
                  </h5>
                </label>

                <input
                  type="file"
                  accept="image/*"
                  name="profileImage"
                  id="file"
                  hidden
                  onChange={this.handleImageUpload}
                />
              </div>
              <h6 className="mt-3">
                Read our <Link to="/login">Privacy Policy</Link>
              </h6>
            </div>
          </div>
        )}

        {profileCreated && (
          <div className="upload-image-modal">
            <div className="modal-container">
              <div className="modal-header mt-2">
                <img src={InstacutsIcons} alt="" />
              </div>
              <div className="content text-center mt-4">
                <h5>Profile Creation Successful</h5>
                <p className="mb-4">
                  Now you can choose this profile to order services for your
                  family member
                </p>
                <Link to="profile">Go Back</Link>
              </div>
            </div>
          </div>
        )}

        {editProfile && (
          <div className="upload-image-modal">
            <div className="modal-container">
              <div className="modal-header mt-2">
                <img src={InstacutsIcons} alt="" />
              </div>
              <div className="content text-center my-4">
                <h5>
                  Changes saved{" "}
                  <i className="fa fa-check" aria-hidden="true"></i>
                </h5>
              </div>
            </div>
          </div>
        )}
        {showDatePicker && (
          <div className="position-absolute dob-picker">
            <Calendar
              onChange={(item) => {
                this.setState({
                  date: item,
                });
              }}
              date={new Date(date)}
            />
            <div className="calender-close">
              <i
                className="fa fa-times"
                aria-hidden="true"
                onClick={this.handleOverlay}
              ></i>
            </div>
          </div>
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return { 
  profiles: state.user.user_profile,
  }
};

export default connect(mapStateToProps)(withRouter(Profile));

// export default Connect()(withRouter(Profile));
