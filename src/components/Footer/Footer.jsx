import React, { Fragment } from 'react';

import './footer.css';
import AppStore from '../../assets/images/appstore.png';
import PlayStore from '../../assets/images/playstore.png';

const Footer = () => {
    return (
        <Fragment>
            <footer>
                <div className="footer-bg">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-7">
                                <div className="heading">
                                    <h2>Making you look Awesome &amp; Beautiful</h2>
                                    <p>With lots of unique blocks, you can easily build a page without coding. Build your next landing page.</p>
                                </div>
                            </div>
                            <div className="col-md-2 col-6 offset-md-1">
                                <div className="img-container">
                                    <img src={AppStore} alt="App Store" />
                                </div>
                            </div>
                            <div className="col-md-2 col-6">
                                <div className="img-container">
                                    <img src={PlayStore} alt="Play Store" className="w-100"/>
                                </div>
                            </div>
                        </div>
                        <div className="divider"></div>
                        <div className="row">
                            <div className="col-md-3 col-12">
                                <h4>instacuts</h4>
                                <p>With lots of unique blocks, you can easily build a page without coding. Build your next landing page.</p>
                                <div className="social-icons">
                                    <ul className="list-unstyled d-flex social-icon-list">
                                        <li className="mr-4"><i className="fa fa-twitter-square" aria-hidden="true"></i></li>
                                        <li className="mr-4"><i className="fa fa-facebook-official" aria-hidden="true"></i></li>
                                        <li className="mr-4"><i className="fa fa-instagram" aria-hidden="true"></i></li>
                                        <li className="mr-4"><i className="fa fa-linkedin-square" aria-hidden="true"></i></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-md-2 col-6 offset-md-1">
                                <h5>Home</h5>
                                <ul className="p-0 list-unstyled">
                                    <li>How It Work</li>
                                    <li>Features</li>
                                    <li>Testimonial</li>
                                </ul>
                            </div>
                            <div className="col-md-2 col-6">
                                <h5>Stylist</h5>
                                <ul className="p-0 list-unstyled">
                                    <li>Became a Stylist</li>
                                    <li>Benefit</li>
                                    <li>Safety</li>
                                </ul>
                            </div>
                            <div className="col-md-2 col-6">
                                <h5>Safety</h5>
                                <ul className="p-0 list-unstyled">
                                    <li>Safety</li>
                                    <li>Customer Safety</li>
                                    <li>Stylist Safety</li>
                                </ul>
                            </div>
                            <div className="col-md-2 col-6">
                                <h5>Company</h5>
                                <ul className="p-0 list-unstyled">
                                    <li>FAQs</li>
                                    <li>Contact Us</li>
                                    <li>About Us</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </Fragment>
    );
}

export default Footer;