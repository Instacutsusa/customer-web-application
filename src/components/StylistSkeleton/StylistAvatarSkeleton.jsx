import React from "react";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

import "./style.css";
const StylistAvatarSkeleton = () => {
  return (
    <>
      <div className="col-md-6 col-lg-4">
        <div className="stylist-card mb-3">
          <div className="stylist-image-skeleton position-relative">
            <SkeletonTheme>
              <Skeleton circle height="100%" />
              <div className="user-name">
                <Skeleton width={70} />
              </div>
            </SkeletonTheme>
          </div>
        </div>
      </div>
    </>
  );
};

export default StylistAvatarSkeleton;
