import React from "react";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

import "./style.css";

const StylistCardSkeleton = () => {
  return (
    <>
      <div className="col-md-6 col-lg-4">
        <div className="stylist-card mb-3">
          <div className="stylist-image-skeleton position-relative">
            <SkeletonTheme>
              <Skeleton circle="true" />
            </SkeletonTheme>
          </div>
          <div className="stylist-content ml-3 w-100 d-flex flex-column align-items-center">
            <div className="stylist-name-skeleton w-100">
              <Skeleton />
            </div>
            <div className="stylist-rating-skeleton w-100 mt-2">
              <div className="stylist-ratingStar-skeleton">
                <Skeleton />
              </div>
              <div className="stylist-review-skeleton flex-grow-1 ml-2">
                <Skeleton />
              </div>
            </div>
          </div>
          {/* <div className="stylist-distance">
                  <img src={NavIcon} alt="" className="text-center" />
                  <i className="fa fa-location-arrow" aria-hidden="true"></i>
                  <span>{item.miles} Miles</span>
                </div> */}
        </div>
      </div>
    </>
  );
};

export default StylistCardSkeleton;
