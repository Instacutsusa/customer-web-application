import React, { Component, Fragment } from "react";
import ProfileImg from "../../../assets/images/selected.png";
import Logo from "../../../assets/images/logo.png";
import { Dropdown } from "react-bootstrap";
import windowSize from "react-window-size";
import "./SidebarHeader.css";
import { Link } from "react-router-dom";
import NoNotiIcon from "../../../assets/images/noti-icon.png";
import MenuIcon from "../../../assets/icons/menu.svg";
import CloseIcon from "../../../assets/icons/cancel-icon.svg";
import NotiIcon from "../../../assets/icons/notification.svg";
import ProfileIcon from "../../../assets/icons/profile.svg";
import UserIcon from "../../../assets/icons/user2.svg";
import EditIcon from "../../../assets/icons/edit-icon.svg";
import StarIcon from "../../../assets/icons/star-icon.svg";
import BuyIcon from "../../../assets/icons/buy.svg";
import PaymentIcon from "../../../assets/icons/payment.svg";
import LogoutIcon from "../../../assets/icons/logout.svg";

class SidebarHeader extends Component {
  render() {
    const { windowWidth, toggleSidebar } = this.props;
    return (
      <Fragment>
        <div className="d-flex justify-content-between align-items-center pt-lg-5 pt-4 pb-lg-0 pb-4 pb-0 sidebar-header sidebar-header-mobile">
          <div className="back-btn">
            <Link to="/home">
              <i className="fa fa-long-arrow-left mr-2" aria-hidden="true"></i>
            </Link>
          </div>
          <div className="sidebar-header-content">
            {windowWidth > 992 ? (
              <>
                <h3>{this.props.heading}</h3>
                <h6>{this.props.subHeading}</h6>
              </>
            ) : (
              <div className="logo text-center">
                <img src={Logo} alt="logo" />
              </div>
            )}
          </div>
          <div className="d-flex align-items-center header-acrtion">
            <div className="position-relative notification-bar mr-3">
              <Dropdown>
                <Dropdown.Toggle>
                  <img src={NotiIcon} alt="" />
                  {/* <i className="fa fa-bell-o" aria-hidden="true"></i> */}
                  <i
                    className="fa fa-circle position-absolute"
                    aria-hidden="true"
                  ></i>
                </Dropdown.Toggle>

                <Dropdown.Menu className="notifications-dropdown sidebar-header-noti ">
                  <div className="notification-dropdown">
                    <div className="notification-header">
                      <h5>Notification</h5>
                    </div>
                    {false && (
                      <div className="notification-list">
                        <p>Today</p>
                        <div className="d-flex">
                          <div className="d-flex">
                            <img src="" alt="" />
                            <div className="notification-details">
                              <h5>
                                Philip Crawford{" "}
                                <span>accepted your booking.</span>
                              </h5>
                              <span></span>
                            </div>
                          </div>
                          <div className="notification-action">
                            <i
                              className="fa fa-ellipsis-h"
                              aria-hidden="true"
                            ></i>
                          </div>
                        </div>
                      </div>
                    )}

                    {true && (
                      <div className="notification-drop">
                        <div className="">
                          <img src={NoNotiIcon} alt="" />
                          <h5>No Notification To Display</h5>
                        </div>
                      </div>
                    )}
                  </div>
                </Dropdown.Menu>
              </Dropdown>
            </div>
            <div className="ml-4">
              <Dropdown>
                <Dropdown.Toggle>
                  <img
                    src={ProfileImg}
                    className="profile-img"
                    alt=""
                    id="dropdown-basic"
                  />
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Link to="/user">
                    {/* <i className="fa fa-user mr-4" aria-hidden="true"></i>  */}
                    <img src={ProfileIcon} alt="" className="mr-4" />
                    Profile
                  </Link>
                  <Link to="/user">
                    {/* <i className="fa fa-users  mr-4" aria-hidden="true"></i>  */}
                    <img src={UserIcon} alt="" className="mr-4" />
                    Select Default Profile
                  </Link>
                  <Dropdown.Divider className="mb-4" />
                  <Link to="/user">
                    {/* <i className="fa fa-pencil-square-o mr-4" aria-hidden="true"></i>  */}
                    <img src={EditIcon} alt="" className="mr-4" />
                    Edit Profile
                  </Link>
                  <Link to="/stylist">
                    {/* <i className="fa fa-star-o mr-4" aria-hidden="true"></i>  */}
                    <img src={StarIcon} alt="" className="mr-4" />
                    Stylist
                  </Link>
                  <Link to="/stylist">
                    {/* <i className="fa fa-shopping-cart mr-4" aria-hidden="true"></i>  */}
                    <img src={BuyIcon} alt="" className="mr-4" />
                    My Order
                  </Link>
                  <Link to="/stylist">
                    {/* <i className="fa fa-id-card-o mr-4" aria-hidden="true"></i>  */}
                    <img src={PaymentIcon} alt="" className="mr-4" />
                    Payment Method
                  </Link>
                  <Link to="/stylist">
                    {/* <i className="fa fa-sign-out mr-4" aria-hidden="true"></i>  */}
                    <img src={LogoutIcon} alt="" className="mr-4" />
                    Logout
                  </Link>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
        </div>
        {windowWidth < 992 ? (
          <div className="sidebar-header-content d-flex justify-content-between mt-4">
            <div>
              <h3>{this.props.heading}</h3>
              <h6>{this.props.subHeading}</h6>
            </div>
            <div className="d-lg-none">
              {toggleSidebar ? (
                <img
                  src={MenuIcon}
                  alt=""
                  className="mr-2"
                  onClick={this.props.SideBarToggle}
                />
              ) : (
                <img
                  src={CloseIcon}
                  alt=""
                  className="mr-2"
                  onClick={this.props.SideBarToggle}
                />
              )}
            </div>
          </div>
        ) : (
          ""
        )}
      </Fragment>
    );
  }
}

export default windowSize(SidebarHeader);
