import React, { Component, Fragment } from "react";
import windowSize from "react-window-size";
import Logo from "../../assets/images/logo.png";
import "./Sidebar.css";
import { Link } from "react-router-dom";
import ProfileIcon from "../../assets/icons/profile.svg";
import ProfileFillIcon from "../../assets/icons/profile-fill.svg";
import MapIcon from "../../assets/icons/locations.svg";
import MapFillIcon from "../../assets/icons/location-fill.svg";
import PaymentIcon from "../../assets/icons/payment-icon.svg";
import PaymentFillIcon from "../../assets/icons/payment-fill.svg";
import StarIcon from "../../assets/icons/star-square.svg";
import StarFillIcon from "../../assets/icons/square-fill.svg";
import StarEmptyIcon from "../../assets/icons/star-icon.svg";
import BuyIcon from "../../assets/icons/shop.svg";
import HistoryIcon from "../../assets/icons/history.svg";
import StarsFillIcon from "../../assets/icons/star-fill.svg";
import BuyFillIcon from "../../assets/icons/buy-fill.svg";
import HistoryFillIcon from "../../assets/icons/history-fill.svg";
import LogoutIcon from "../../assets/icons/logout-icon.svg";

class SideBar extends Component {
  // Pause background scroll

  // shouldComponentUpdate(nextProps, nextState) {
  //     if(nextProps.toggleSidebar && this.props.windowWidth > 991) {
  //         document.body.classList.remove('modal-open');
  //     } else {
  //         document.body.classList.add('modal-open');
  //     }
  //     return true
  // }

  render() {
    const { windowWidth } = this.props;
    return (
      <Fragment>
        <div
          className={
            this.props.toggleSidebar ? "d-flex toggled h-100" : "d-flex h-100"
          }
          id="wrapper"
        >
          <div className="profile-sidebar" id="sidebar-wrapper">
            {windowWidth > 991 && (
              <div className="logo py-lg-5 pb-5 pt-2 text-center">
                <img src={Logo} alt="logo" />
              </div>
            )}
            <div className="list-group list-group-flush">
              <h6>My Account</h6>
              <Link
                to="/user"
                onClick={this.props.SideBarToggle}
                className={
                  this.props.active === "profile"
                    ? "position-relative activeBar"
                    : "position-relative"
                }
              >
                <div className="side-line"></div>
                {this.props.active === "profile" ? (
                  <img src={ProfileFillIcon} alt="" className="mr-4" />
                ) : (
                  <img src={ProfileIcon} alt="" className="mr-4" />
                )}
                Edit Profile
              </Link>
              <Link
                to="/myaccount"
                onClick={this.props.SideBarToggle}
                className={
                  this.props.active === "address"
                    ? "position-relative activeBar"
                    : "position-relative"
                }
              >
                <div className="side-line"></div>
                {this.props.active === "address" ? (
                  <img src={MapFillIcon} alt="" className="mr-4" />
                ) : (
                  <img src={MapIcon} alt="" className="mr-4" />
                )}
                Address
              </Link>
              <Link
                to="/payment"
                onClick={this.props.SideBarToggle}
                className={
                  this.props.active === "payment"
                    ? "position-relative activeBar"
                    : "position-relative"
                }
              >
                <div className="side-line"></div>
                {this.props.active === "payment" ? (
                  <img src={PaymentFillIcon} alt="" className="mr-4" />
                ) : (
                  <img src={PaymentIcon} alt="" className="mr-4" />
                )}
                Payment
              </Link>
              <Link
                to="/stylists"
                onClick={this.props.SideBarToggle}
                className={
                  this.props.active === "stylist"
                    ? "position-relative activeBar"
                    : "position-relative"
                }
              >
                <div className="side-line"></div>
                {this.props.active === "stylist" ? (
                  <img src={StarFillIcon} alt="" className="mr-4" />
                ) : (
                  <img src={StarIcon} alt="" className="mr-4" />
                )}
                Stylist
              </Link>
            </div>
            <div className="list-group list-group-flush">
              <h6>My Bookings</h6>
              <Link
                to="/booking"
                onClick={this.props.SideBarToggle}
                className={
                  this.props.active === "booking"
                    ? "position-relative activeBar"
                    : "position-relative"
                }
              >
                <div className="side-line"></div>
                {this.props.active === "booking" ? (
                  <img src={BuyFillIcon} alt="" className="mr-4" />
                ) : (
                  <img src={BuyIcon} alt="" className="mr-4" />
                )}
                Bookings
              </Link>
              <Link
                to="/history"
                onClick={this.props.SideBarToggle}
                className={
                  this.props.active === "history"
                    ? "position-relative activeBar"
                    : "position-relative"
                }
              >
                <div className="side-line"></div>
                {this.props.active === "history" ? (
                  <img src={HistoryFillIcon} alt="" className="mr-4" />
                ) : (
                  <img src={HistoryIcon} alt="" className="mr-4" />
                )}
                History
              </Link>
            </div>
            <div className="list-group list-group-flush reward-list p-md-0 mt-0">
              <Link
                to="/rewards"
                onClick={this.props.SideBarToggle}
                className={
                  this.props.active === "reward"
                    ? "mt-md-0 position-relative activeBar"
                    : "position-relative mt-md-0"
                }
              >
                <div className="side-line"></div>
                {this.props.active === "reward" ? (
                  <img src={StarsFillIcon} alt="" className="mr-4" />
                ) : (
                  <img src={StarEmptyIcon} alt="" className="mr-4" />
                )}
                Rewards
              </Link>
            </div>
            <div className="list-group list-group-flush p-0 mt-0">
              <Link
                to="/rewards"
                // onClick={this.props.SideBarToggle}
                className="mt-0 position-relative"
              >
                <div className="side-line"></div>
                <img src={LogoutIcon} alt="" className="mr-4" />
                Logout
              </Link>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default windowSize(SideBar);
