import React, { Fragment } from 'react';
import DemoImg from '../../assets/images/download.png';
import Slider from "react-slick";

const HappyCustomers = () => {
    var settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 5,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 991,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }

          ]
      };
    return(
        <Fragment>
            <div className="heading pb-4">
                <div className="title">
                    <h5>Happy Customers</h5>
                </div>
            </div>
            {/* <Carousel responsive={responsive} arrows={false} className="pb-5 categories-carousel"> */}
            <Slider {...settings} className="pb-5 categories-carousel">
                <div className="categories-list">
                    <img src={DemoImg} alt="" />
                </div>
                <div className="categories-list">
                    <img src={DemoImg} alt="" />
                </div>
                <div className="categories-list">
                    <img src={DemoImg} alt="" />
                </div>
                <div className="categories-list">
                    <img src={DemoImg} alt="" />
                </div>
                <div className="categories-list">
                    <img src={DemoImg} alt="" />
                </div>
                <div className="categories-list">
                    <img src={DemoImg} alt="" />
                </div>
                <div className="categories-list">
                    <img src={DemoImg} alt="" />
                </div>
            </Slider>
        </Fragment>
    );
}

export default HappyCustomers;