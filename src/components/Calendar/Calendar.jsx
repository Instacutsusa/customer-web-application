import React, { useState } from "react";
import { DateRange } from "react-date-range";
const Calendar = () => {
  // const [state, setState] = useState([
  //     {
  //       startDate: new Date(),
  //       endDate: addDays(new Date(), 7),
  //       key: 'selection'
  //     }
  // ]);

  const [state, setState] = useState([
    {
      startDate: new Date(),
      endDate: null,
      key: "selection",
    },
  ]);

  return (
    // <DateRangePicker
    //     onChange={item => setState([item.selection])}
    //     showSelectionPreview={true}
    //     moveRangeOnFirstSelection={false}
    //     months={2}
    //     ranges={state}
    //     direction="vertical"
    // />
    <DateRange
      editableDateInputs={true}
      onChange={(item) => setState([item.selection])}
      moveRangeOnFirstSelection={false}
      ranges={state}
      direction="horizontal"
      months={2}
    />
  );
};

export default Calendar;
