import React, { Component, Fragment } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import Active1 from "../../assets/images/active1.png";
import Active2 from "../../assets/images/active2.png";
import "./favStylist.css";

class FavStylist extends Component {
  state = {
    switchChecked: false,
  };

  getChckeboxValue = (event) => {
    const value = event.target.value;
    this.setState({
      switchChecked: !this.state.switchChecked,
    });
  };

  render() {
    const { switchChecked } = this.state;
    const responsive = {
      desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 8,
        slidesToSlide: 8, // optional, default to 1.
      },
      tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2,
        slidesToSlide: 2, // optional, default to 1.
      },
      mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
      },
    };

    return (
      <Fragment>
        <div className="card">
          <div className="fav-stylist-header d-flex align-items-center justify-content-between">
            <h5>Favorite Stylist</h5>
            <label className="switch">
              <input
                type="checkbox"
                onChange={this.getChckeboxValue}
                value={switchChecked}
                checked={switchChecked}
              />
              <span className="slider round"></span>
            </label>
          </div>
          {switchChecked && (
            <div className="fav-stylist-list">
              <p>Displaying Favorites Stylist</p>
              <div className="">
                <Carousel
                  responsive={responsive}
                  arrows={false}
                  className="active-stylist-carousel fav-stylist-carousel"
                >
                  <div className="active-stylist position-relative">
                    <img src={Active1} alt="" />
                    <i
                      className="fa fa-circle fav-icon-active mr-2 position-absolute"
                      aria-hidden="true"
                    ></i>
                    <div className="stylist-name pt-2">
                      <h6>Paul Soto</h6>
                    </div>
                  </div>
                  <div className="active-stylist position-relative">
                    <img src={Active2} alt="" />
                    <i
                      className="fa fa-circle fav-icon-active mr-2 position-absolute"
                      aria-hidden="true"
                    ></i>
                    <div className="stylist-name pt-2">
                      <h6>Paul Soto</h6>
                    </div>
                  </div>
                  <div className="active-stylist position-relative">
                    <img src={Active1} alt="" />
                    <i
                      className="fa fa-circle fav-icon-active mr-2 position-absolute"
                      aria-hidden="true"
                    ></i>
                    <div className="stylist-name pt-2">
                      <h6>Paul Soto</h6>
                    </div>
                  </div>
                  <div className="active-stylist position-relative">
                    <img src={Active2} alt="" />
                    <i
                      className="fa fa-circle fav-icon-active mr-2 position-absolute"
                      aria-hidden="true"
                    ></i>
                    <div className="stylist-name pt-2">
                      <h6>Paul Soto</h6>
                    </div>
                  </div>
                  <div className="active-stylist position-relative">
                    <img src={Active1} alt="" />
                    <i
                      className="fa fa-circle fav-icon-active mr-2 position-absolute"
                      aria-hidden="true"
                    ></i>
                    <div className="stylist-name pt-2">
                      <h6>Paul Soto</h6>
                    </div>
                  </div>
                  <div className="active-stylist position-relative">
                    <img src={Active2} alt="" />
                    <i
                      className="fa fa-circle fav-icon-active mr-2 position-absolute"
                      aria-hidden="true"
                    ></i>
                    <div className="stylist-name pt-2">
                      <h6>Paul Soto</h6>
                    </div>
                  </div>
                  <div className="active-stylist position-relative">
                    <img src={Active2} alt="" />
                    <i
                      className="fa fa-circle fav-icon-active mr-2 position-absolute"
                      aria-hidden="true"
                    ></i>
                    <div className="stylist-name pt-2">
                      <h6>Paul Soto</h6>
                    </div>
                  </div>
                </Carousel>
              </div>
            </div>
          )}
        </div>
      </Fragment>
    );
  }
}

export default FavStylist;
