import React, { Component, Fragment } from 'react';
import Modal from 'react-bootstrap/Modal';
class CancallationPolicy extends Component {
    render() {
        return (
            <Fragment>
                <div className="cancelation-model" onClick={e => this.props.handleModal("open", e)}>
                    <i className="fa fa-question-circle-o" aria-hidden="true"></i>
                    <h5>Cancellation Policy</h5>
                </div>
                <Modal show={this.props.isCancellationModalOpen} onHide={e => this.props.handleModal("close", e)} className="cancellation-policy-modal" scrollable={true}>
                    <Modal.Header>
                    <Modal.Title>Cancellation Policy</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="cancellation-modal-content">
                            <h5>Last Updated: 12th Nov 2020</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ac dui non arcu fermentum ornare ac ac lorem. Etiam fringilla posuere justo eget placerat.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ac dui non arcu fermentum ornare ac ac lorem. Etiam fringilla posuere justo eget placerat.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ac dui non arcu fermentum ornare ac ac lorem. Etiam fringilla posuere justo eget placerat.</p>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <h6 onClick={e => this.props.handleModal("close", e)}>Close</h6>
                    </Modal.Footer>
                </Modal>
            </Fragment>
        );
    }
}

export default CancallationPolicy;