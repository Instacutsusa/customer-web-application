import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { Dropdown } from "react-bootstrap";
import Logo from "../../assets/images/logo.png";
import MapIcon from "../../assets/images/icon-map.png";
import ProfileImg from "../../assets/images/profile.png";
import NoNotiIcon from "../../assets/images/noti-icon.png";
import windowSize from "react-window-size";
import "./DynamicHeader.css";
import LocationIcon from "../../assets/icons/location-icon.svg";
import SearchIcon from "../../assets/icons/search.svg";
import BuyIcon from "../../assets/icons/buy.svg";
import HeartIcon from "../../assets/icons/heart.svg";
import MapsIcon from "../../assets/icons/map-icon.svg";
import ProfileIcon from "../../assets/icons/profile.svg";
import StarIcon from "../../assets/icons/star.svg";
import WalletIcon from "../../assets/icons/wallet.svg";
import LogoutIcon from "../../assets/icons/logout.svg";
import CloseIcon from "../../assets/icons/close-icon.svg"
import { getListAddresses, setActiveAddress } from "../../api/myAccounts.api";
import {
  NOTIFICATIONSMESSAGE,
  NOTIFICATION_TYPE,
} from "../../_constant/notificationsMessage";
import { NotificationAlert } from "../../common/NotificationAlert";
import { useState } from "react";
import { useEffect } from "react";
import { getPrefferedServices } from "../../api/stylist.api";
import FavouriteServiceDropdown from "../../Blocks/FavouriteServiceDropdown";
import { connect } from "react-redux";
import { userProfile } from "../../redux/actions";

const Header = (props) => {
  const [activeLocationDropdown, _setActiveLocationDropdown] = useState(false);
  const [overlayDrop, _setOverlayDrop] = useState(false);
  const [allAddressList, _setAllAddressList] = useState([]);
  const [stylistSearchName, _setStylistSearchName] = useState("");
  const [loader, _setLoader] = useState(false);
  const [activeIndex, _setActiveIndex] = useState(null);
  const history = props.history;

  useEffect(() => {
    if (props.searchStylistName) {
      _setStylistSearchName(props.searchStylistName);
    }else{
      _setStylistSearchName("");
    }
  }, [props.searchStylistName]);

  useEffect(() => {
    {
      (async () => {
        const { data } = await getListAddresses();
        _setAllAddressList(data);


        const user = JSON.parse(localStorage.getItem("customer_info"));
        const apiBody = { user_id: user._id ,type: 0};

        const favServices = await getPrefferedServices(apiBody)
      })();
    }
  }, []);

  const handleDropdown = () => {
    this.setState((prevState) => ({
      activeLocationDropdown: !prevState.activeLocationDropdown,
    }));
  };

  const handleOverlay = () => {
    _setOverlayDrop(!overlayDrop);
  };

  const searchStylist = (e) => {
    _setStylistSearchName(e.target.value);
    if (props.location.pathname !== "/search-results") {
      if (e.target.value) {
        props.history.push({
          pathname: "/search-results",
          state: { searchName: e.target.value },
        });
      }
    } else {
      props?.setStylistSearchName(e);
    }
  };

  const setAddressToActive = async (address_id,index) => {
    try {
      _setLoader(true);
      _setActiveIndex(index);
      const apiBody = { address_id, active: 1 };
      await setActiveAddress(apiBody);
      const { data } = await getListAddresses();
      _setAllAddressList(data);
      NotificationAlert(
        NOTIFICATIONSMESSAGE.SET_ADDRESS_ACTIVE,
        NOTIFICATION_TYPE.SUCCESS
      );
    } catch (err) {
      console.log(err);
    } finally {
      _setLoader(false);
      _setActiveIndex(null);
    }
  };

  const logout = () => {
    props.dispatch(userProfile({ user_profile: [] }));
    localStorage.removeItem("customer_info");
    localStorage.removeItem("access_token");
    history.push('/');
  }

  // const allAddressList = this.state.allAddressList;
  // const { overlayDrop } = this.state;
  // const { windowWidth } = this.props;
  const activeAddress = allAddressList.find((add) => add?.active);

  const clearSearchText = () => {
    if(props.clearSearchName){
      props.clearSearchName();
    }
  }
  return (
    <Fragment>
      {/* <span
          className={
            overlayDrop === true ? "screen-overlay show" : "screen-overlay"
          }
          onClick={this.handleOverlay}
        ></span> */}
      <div className="header-bg">
        <div className="container">
          <header className="py-md-4 d-md-flex justify-content-between align-items-center">
            <div className="d-flex py-4 py-md-0 justify-content-between align-items-center header-mobile-bg">
              <div className="logo mr-4">
                <img src={Logo} alt="logo" />
              </div>
              {!props.title ? (
                <div className="location-bar d-flex align-items-center position-relative">
                  {/* <i className="fa fa-map-marker mr-3 ml-4" aria-hidden="true"></i> */}
                  <img src={LocationIcon} alt="" className="mr-3 ml-4" />
                  <Dropdown>
                    <Dropdown.Toggle>
                      <h6 className="m-0" onClick={() => handleOverlay()}>
                        {" "}
                        {props.windowWidth < 991 ? (
                          "Home"
                        ) : (
                          <>
                            <span>My Location: </span>
                            <span className="selected-address-text">
                              {activeAddress?.address}
                            </span>
                          </>
                        )}{" "}
                        <i
                          className="ml-2 fa fa-angle-down"
                          aria-hidden="true"
                        ></i>
                      </h6>
                    </Dropdown.Toggle>

                    <Dropdown.Menu
                      className="address-dro-width home-address-dropdown"
                      id="address-dropdown"
                    >
                      <div className="location-dropdown">
                        <div className="location-dropdown-header position-relative">
                          <div className="location-img text-center">
                            <img src={MapIcon} alt="" />
                          </div>
                          <div className="location-text py-4">
                            <h5>Select Your Location</h5>
                          </div>
                          <Dropdown.Toggle
                            id="dropdown-autoclose-outside"
                            className="modalCloseButton text-dark position-absolute top-0"
                          >
                            <span className="ml-2">&#10006;</span>
                          </Dropdown.Toggle>
                        </div>
                        <div className="location-items customScroll">
                          {allAddressList?.map((address, index) => (
                            // <div className="location-item mb-3 active" key={index}>
                            <div
                              className={`location-item mb-3 ${
                                address?.active && "active"
                              }`}
                              key={index}
                              onClick={() =>
                                setAddressToActive(address._id, index)
                              }
                            >
                              <h6>
                                <img src={MapsIcon} alt="" />
                                {/* <i className="fa fa-map-marker" aria-hidden="true"></i> */}
                              </h6>
                              <div className="location-address ml-2">
                                <div className="d-flex">
                                  <h5>{address?.address_type}</h5>
                                  {activeIndex === index && (
                                    <div className="location-active-button">
                                      <span className="badge badge-secondary location-badge">
                                        <span className="ml-2">
                                          {loader && (
                                            <i className="fa fa-gear fa-spin"></i>
                                          )}
                                        </span>
                                      </span>
                                    </div>
                                  )}
                                  {address.active && (
                                    <div className="location-active-button">
                                      <span className="badge badge-secondary location-badge">
                                        Active
                                      </span>
                                    </div>
                                  )}
                                </div>
                                <div>
                                  <h6>{address?.address}</h6>
                                </div>
                              </div>
                            </div>
                          ))}
                          {/* <div className="location-item mb-3 active">
                                                <h6>
                                                    <img src={MapsIcon} alt="" /> */}
                          {/* <i className="fa fa-map-marker" aria-hidden="true"></i> */}
                          {/* </h6>
                                                <div className="location-address ml-2">
                                                    <h5>Home</h5>
                                                    <h6>7306 Madisyn Manors Suite 387</h6>
                                                </div>
                                                <div className="ml-5">
                                                    <span className="badge badge-secondary location-badge">Active</span>
                                                </div>
                                            </div> */}
                          {/* <div className="location-item mb-3">
                                                <h6>
                                                <img src={MapsIcon} alt="" /> */}
                          {/* <i className="fa fa-map-marker" aria-hidden="true"></i> */}
                          {/* </h6>
                                                <div className="location-address ml-2">
                                                    <h5>Work</h5>
                                                    <h6>7306 Madisyn Manors Suite 387</h6>
                                                </div>
                                            </div> */}
                        </div>
                        <button
                          className="location-new-address mt-2"
                          onClick={() => props.history.push("/address")}
                        >
                          Add New Address
                        </button>
                      </div>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              ) : (
                <div className="cart-page-header">
                  <div className="cart-header-title">
                    <h5>{props.title}</h5>
                  </div>
                </div>
              )}
            </div>
            <div className="header-right-content">
              <div className="search-bar">
                <img src={SearchIcon} alt="" className="mr--md-3 mr-2 ml-2" />
                {/* <i className="fa fa-search" aria-hidden="true"></i> */}
                <input
                  type="text"
                  autoFocus
                  placeholder="Search Stylist"
                  value={stylistSearchName}
                  onChange={(e) => {
                    e.persist();
                    searchStylist(e);
                  }}
                />
                {stylistSearchName && (
                  <img
                    src={CloseIcon}
                    className="cancelIcon"
                    alt=""
                    onClick={clearSearchText}
                  />
                )}
              </div>
              <div className="cart-icon">
                <Dropdown>
                  <Dropdown.Toggle>
                    <img src={BuyIcon} alt="" className="header-icon" />
                    {/* <i className="fa fa-shopping-cart" aria-hidden="true"></i> */}
                  </Dropdown.Toggle>

                  <Dropdown.Menu className="notifications-dropdown">
                    <div className="notification-dropdown">
                      {false && (
                        <div className="notification-list">
                          <p>Today</p>
                          <div className="d-flex">
                            <div className="d-flex">
                              <img src="" alt="" />
                              <div className="notification-details">
                                <h5>
                                  Philip Crawford{" "}
                                  <span>accepted your booking.</span>
                                </h5>
                                <span></span>
                              </div>
                            </div>
                            <div className="notification-action">
                              <i
                                className="fa fa-ellipsis-h"
                                aria-hidden="true"
                              ></i>
                            </div>
                          </div>
                        </div>
                      )}

                      {true && (
                        <div className="notification-drop cart-drop">
                          <div className="">
                            <img src={NoNotiIcon} alt="" />
                            <h5 className="mb-4">
                              Experience Salon Services At Home
                            </h5>
                            <p className="mb-5">
                              Your Bookings Cart is empty. <br />
                              Add something from the list of services.
                            </p>
                            <Link to="#">
                              Book A Service{" "}
                              <i
                                className="fa fa-long-arrow-right"
                                aria-hidden="true"
                              ></i>
                            </Link>
                          </div>
                        </div>
                      )}
                    </div>
                  </Dropdown.Menu>
                </Dropdown>
              </div>
              <div className="fav-icon">
                <Dropdown>
                  <Dropdown.Toggle>
                    <img src={HeartIcon} alt="" className="header-icon" />
                    {/* <i className="fa fa-heart-o" aria-hidden="true"></i> */}
                  </Dropdown.Toggle>

                  <Dropdown.Menu className="notifications-dropdown fav-drop">
                    <FavouriteServiceDropdown />
                  </Dropdown.Menu>
                </Dropdown>
              </div>
              {/* <div className="noti-icon position-relative notification-bar">
                <Dropdown>
                  <Dropdown.Toggle>
                    <img
                      src={NotificationIcon}
                      alt=""
                      className="header-icon"
                    />
                    <i className="fa fa-bell-o" aria-hidden="true"></i>
                    <span className="position-absolute noti-count">1</span>
                    <i className="fa fa-circle position-absolute" aria-hidden="true"></i>
                  </Dropdown.Toggle>

                  <Dropdown.Menu className="notifications-dropdown  noti-drop address-dro-width">
                    <div className="notification-dropdown">
                      <div className="notification-header">
                        <h5>Notification</h5>
                      </div>
                      {true && (
                        <div className="notification-list">
                          <p>Today</p>
                          <div className="d-flex justify-content-between notification-list-item">
                            <div className="d-flex">
                              <img src={favImg} alt="" />
                              <div className="notification-details ml-3">
                                <h5>
                                  Philip Crawford{" "}
                                  <span>accepted your booking.</span>
                                </h5>
                                <h6>5 mins ago</h6>
                              </div>
                            </div>
                            <div className="notification-action">
                              <i
                                className="fa fa-ellipsis-h"
                                aria-hidden="true"
                              ></i>
                            </div>
                          </div>
                          <div className="d-flex justify-content-between notification-list-item">
                            <div className="d-flex">
                              <img src={favImg} alt="" />
                              <div className="notification-details ml-3">
                                <h5>
                                  Philip Crawford{" "}
                                  <span>accepted your booking.</span>
                                </h5>
                                <h6>5 mins ago</h6>
                              </div>
                            </div>
                            <div className="notification-action">
                              <i
                                className="fa fa-ellipsis-h"
                                aria-hidden="true"
                              ></i>
                            </div>
                          </div>
                        </div>
                      )}

                      {false && (
                        <div className="notification-drop">
                          <div className="">
                            <img src={NoNotiIcon} alt="" />
                            <h5>No Notification To Display</h5>
                          </div>
                        </div>
                      )}
                    </div>
                  </Dropdown.Menu>
                </Dropdown>
              </div> */}
              <div className="profile-img-container">
                <Dropdown>
                  <Dropdown.Toggle>
                    <img src={ProfileImg} className="p-img" alt="" />
                  </Dropdown.Toggle>

                  <Dropdown.Menu>
                    <div className="profile-drop-header text-center mb-4">
                      <img src={ProfileImg} alt="" />
                      <h5 className="mt-3">Samantha William </h5>
                      <h6>Default Profile</h6>
                    </div>
                    <Link to="/user">
                      {/* <i className="fa fa-user mr-4" aria-hidden="true"></i>  */}
                      <img src={ProfileIcon} className="mr-4" alt="" />
                      My Account
                    </Link>
                    <Dropdown.Divider className="mb-4" />
                    <Link to="/stylist">
                      {/* <i className="fa fa-star-o mr-4" aria-hidden="true"></i> */}
                      <img src={StarIcon} className="mr-4" alt="" />
                      Stylist
                    </Link>
                    <Link to="/stylist">
                      {/* <i className="fa fa-shopping-cart mr-4" aria-hidden="true"></i>  */}
                      <img src={BuyIcon} className="mr-4" alt="" />
                      My Order
                    </Link>
                    <Link to="/stylist">
                      {/* <i className="fa fa-id-card-o mr-4" aria-hidden="true"></i>  */}
                      <img src={WalletIcon} className="mr-4" alt="" />
                      Payment Method
                    </Link>
                    <div onClick={() => logout()}>
                      {/* <i className="fa fa-sign-out mr-4" aria-hidden="true"></i>  */}
                      <img src={LogoutIcon} className="mr-4" alt="" />
                      Logout
                    </div>
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            </div>
          </header>
        </div>
      </div>
    </Fragment>
  );
};

const mapStateToProps = (state) => ({
  item: state,
})    //  to use dispatch function 

export default connect(mapStateToProps)(windowSize(Header));
