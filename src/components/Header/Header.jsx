import React, { Component, Fragment } from "react";
import $ from "jquery";
import { Link } from "react-router-dom";
import Logo from "../../assets/images/logo.png";
import "./header.css";

class Header extends Component {
  componentDidMount = () => {
    $("[data-trigger]").on("click", function (e) {
      e.preventDefault();
      e.stopPropagation();
      var offcanvas_id = $(this).attr("data-trigger");
      $(offcanvas_id).toggleClass("show");
      $("body").toggleClass("offcanvas-active");
      $(".screen-overlay").toggleClass("show");
    });

    // Close menu when pressing ESC
    $(document).on("keydown", function (event) {
      if (event.keyCode === 27) {
        $(".mobile-offcanvas").removeClass("show");
        $("body").removeClass("overlay-active");
      }
    });

    $(".btn-close, .screen-overlay").click(function (e) {
      $(".screen-overlay").removeClass("show");
      $(".mobile-offcanvas").removeClass("show");
      $("body").removeClass("offcanvas-active");
    });
  };
  render() {
    return (
      <Fragment>
        <div className="container">
          <header className="px-3 mt-4 py-md-2 d-flex justify-content-between align-items-center">
            <div className="logo">
              <img src={Logo} alt="logo" />
            </div>
            <button
              data-trigger="#navbar_main"
              className="d-lg-none navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <i className="fa fa-bars" aria-hidden="true"></i>
            </button>
            <nav
              className="mobile-offcanvas navbar-expand-lg navbar-light px-lg-0 pb-lg-0"
              id="navbar_main"
            >
              <div className="offcanvas-header">
                <button className="btn btn-close float-right d-lg-none">
                  {" "}
                  <i className="fa fa-times" aria-hidden="true"></i>
                </button>
              </div>
              <div className="w-100">
                <ul className="navbar-nav mr-auto w-100 justify-content-between align-items-center position-relative custom-nav">
                  <li className="nav-item active">
                    <Link className="nav-link" to="#">
                      Home
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="#">
                      Stylist
                    </Link>
                  </li>
                  <li className="nav-item ">
                    <Link className="nav-link" to="#">
                      Safety
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="#">
                      Faq
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="#">
                      Get App
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="#">
                      Contact
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="#">
                      About Us
                    </Link>
                  </li>
                </ul>
                <div className="btn-container d-block d-lg-none">
                  <Link to="">
                    Continue with mobile app{" "}
                    <i
                      className="fa fa-long-arrow-right ml-2"
                      aria-hidden="true"
                    ></i>
                  </Link>
                </div>
              </div>
            </nav>
            <div className="btn-container d-none d-lg-block">
              <Link to="">
                Continue with mobile app{" "}
                <i
                  className="fa fa-long-arrow-right ml-2"
                  aria-hidden="true"
                ></i>
              </Link>
            </div>
          </header>
        </div>
      </Fragment>
    );
  }
}

export default Header;
