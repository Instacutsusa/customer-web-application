import React, { Fragment } from "react";
import "./PickSlot.css";
const PickSlot = (props) => {
  return (
    <Fragment>
      <div className="available-slots-container">
        <div className="card">
          <div className="d-flex justify-content-between align-items-center calendar-header">
            <i className="fa fa-angle-left" aria-hidden="true"></i>
            <h5>Dec, 2021</h5>
            <i className="fa fa-angle-right" aria-hidden="true"></i>
          </div>
          <div className="calendar-dates d-flex justify-content-between">
            <div className="calendar-date text-center">
              <h6>Mon</h6>
              <span>1</span>
            </div>
            <div className="calendar-date text-center">
              <h6>Tue</h6>
              <span>2</span>
            </div>
            <div className="calendar-date text-center">
              <h6>Wed</h6>
              <span>3</span>
            </div>
            <div className="calendar-date text-center">
              <h6>Thu</h6>
              <span>4</span>
            </div>
            <div className="calendar-date text-center">
              <h6>Fri</h6>
              <span>5</span>
            </div>
            <div className="calendar-date text-center">
              <h6>Sat</h6>
              <span>6</span>
            </div>
            <div className="calendar-date text-center">
              <h6>Sun</h6>
              <span>7</span>
            </div>
          </div>
        </div>
        <div className="card">
          <div className="slots-header">
            <h5>Available Slots</h5>
          </div>
          <div className="slot-list">
            <ul className="list-unstyled">
              <li>
                Now <span>(ETA 30Mins)</span>
              </li>
              <li>9:00 - 10:00 AM</li>
              <li>10:30 - 11:30 AM</li>
              <li className="selected">1:30 - 2:30 PM</li>
              <li>3:00 - 4:00 PM</li>
              <li>4:30 - 5:30 PM</li>
            </ul>
          </div>
        </div>
        <div className="payment-btn pt-3">
          <button type="" onClick={props.continueHandler}>
            CONTINUE{" "}
            <i className="fa fa-long-arrow-right" aria-hidden="true"></i>
          </button>
        </div>
      </div>
    </Fragment>
  );
};

export default PickSlot;
