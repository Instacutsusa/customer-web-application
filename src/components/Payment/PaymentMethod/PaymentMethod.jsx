import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import WalletImg from '../../../assets/images/wallet.png';
import VisaImg from '../../../assets/images/visa.png';
import CreditCard from '../../../assets/images/credit-card.png';
import './PaymentMethod.css';

const PaymentMethod = (props) => {

    return (
        <Fragment>
            <div className="mx-md-5">
                <div className="row">
                    <div className="col-lg-8">
                        <div className="card">
                           <div className="payment-method-header">
                               <div className="d-flex justify-content-between mb-2">
                                    <h5>Payment Method</h5>
                                    <Link to="">Add New</Link>
                               </div>
                                <div className="payment-wallet-points">
                                    <div className="payment-wallet-credits">
                                        <div className="d-flex align-items-center">
                                            <div className="wallet-img">
                                                <img src={WalletImg} alt=""/>
                                            </div>
                                            <div className="wallet-credit-content ml-4">
                                                <h5>Wallet Credits</h5>
                                                <p>243 points</p>
                                            </div>
                                        </div>
                                        <div className="wallet-action">
                                            <label className="switch">
                                                <input type="checkbox" onChange={props._getChckeboxValue} value={props.switchChecked} defaultChecked={props.switchChecked}/>
                                                <span className="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="payment-cards">
                                    {props.data.length ? props.data.map((item, index)=> {
                                        return (
                                            <div className="payment-wallet-credits d-flex align-items-center justify-content-between" key={index}>
                                                <div className="d-flex align-items-center">
                                                    <div className="wallet-img">
                                                        <img src={VisaImg} alt=""/>
                                                    </div>
                                                    <div className="wallet-credit-content ml-4">
                                                        <h5>**** **** 4215</h5>
                                                    </div>
                                                </div>
                                                <div className="wallet-action">
                                                    {item.selected ? <i className="fa fa-check" aria-hidden="true"></i> : ""}
                                                </div>
                                            </div>
                                        );
                                    }): 
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="no-service-container text-center">
                                                <div className="img-container">
                                                    <img src={CreditCard} alt="" />
                                                </div>
                                                <div className="content">
                                                    <h5>No Payment Method Found</h5>
                                                    <h6>Please add a new credit card<br/> below to continue</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    }
                                </div>
                           </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card">
                            <div className="order-bill-card">
                                <div className="details-header">
                                    <h5>Total Payable</h5>
                                    <h3><span>$</span> 450</h3>
                                </div>
                                <div className="bill-details-body">
                                    <div className="bill-collapse" onClick={props._handleShowDetails}>
                                        <h5>Bill Details</h5>
                                        { props.showDetails === true ? <i className="fa fa-angle-up" aria-hidden="true"></i> : <i className="fa fa-angle-down" aria-hidden="true"></i>}
                                    </div>
                                    { props.showDetails && 
                                    <Fragment>
                                        <div className="detail-content d-flex justify-content-between my-2">
                                            <h5>Service Charges(4): </h5>
                                            <h6>$410</h6>
                                        </div>
                                        <div className="detail-content d-flex justify-content-between my-2">
                                            <h5>Convenience Fee: </h5>
                                            <h6>$20</h6>
                                        </div>
                                        <div className="detail-content d-flex justify-content-between my-2">
                                            <h5>Discount: </h5>
                                            <h6>- $20</h6>
                                        </div>
                                        <div className="detail-content d-flex justify-content-between my-2">
                                            <h5>Voucher(DX239 Applied) 99% OFF: </h5>
                                            <h6>- $20</h6>
                                        </div>
                                        <div className="detail-content d-flex justify-content-between my-2">
                                            <h5>Tax: </h5>
                                            <h6>- $20</h6>
                                        </div> 
                                    </Fragment>
                                    }
                                    <div className="wallet-credits-amount d-flex align-items-center justify-content-between pt-3">
                                        <p>Wallet Credits</p>
                                        <h6>-243</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="payment-btn">
                            <button>Confirm Payment <i className="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment> 
    );
}

export default PaymentMethod;