import React from "react";
import { HashLoader } from "react-spinners";
import { css } from "@emotion/react";

import "./loader.css";

const Loader = () => {
  const override = css`
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
  `;

  return <HashLoader css={override} />;
};

export default Loader;
