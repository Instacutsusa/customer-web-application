import React from "react";
import StarRatings from "react-star-ratings";

import ReviewImg from "../../assets/images/review.png";

const RatingCard = ({ ratingData }) => {
  return (
    <>
      {ratingData?.map((rating,index) => {
        return (
          <div className="col-md-12" key={index}>
            <div className="review-list d-md-flex py-4">
              <div className="review-img">
                <img src={ReviewImg} alt="" />
              </div>
              <div className="review-content mx-4">
                <h5>Eleanor Summers</h5>
                <h6>{new Date(rating.updated_at).toDateString().toString()}</h6>
              </div>
              <div className="review-comment">
                {
                  rating?.value&& (
                    <StarRatings
                  rating={rating?.value}
                  starDimension="15px"
                  starSpacing="3px"
                  starRatedColor="rgb(255, 153, 31)"
                />
                  )
                }
                <h6>{rating.message}</h6>
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
};

export default RatingCard;
