import React, { Component, Fragment } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";

import windowSize from "react-window-size";
import FilterIcon from "../../assets/images/icon.png";
import RatingCard from "./RatingCard";
import "./review.css";
import "react-tabs/style/react-tabs.css";
import { getStylistRatingList } from "../../api/stylist.api";

class Review extends Component {
  state = {
    showFilter: false,
    tabIndex: 0,
    ratingData: [],
  };

  componentDidMount = () => {
    this.fetchStylistRatingList(this.props.stylisDetail?._id);
  };

  // Fetch Stylist Rating List
  fetchStylistRatingList = async (stylist_id) => {
    try {
      const apiBody = {
        stylist_id,
        filter: "lowest",
      };
      const { data } = await getStylistRatingList(apiBody);
      this.setState({
        ...this.state,
        ratingData: data,
      });
    } catch (err) {
      console.log(err);
    }
  };
  handleShowFilter = () => {
    this.setState((prevState) => ({
      showFilter: !prevState.showFilter,
    }));
  };

  showFilterData = async(filterOption) => {
    
    const apiBody = {
      stylist_id: this.props.stylisDetail?._id,
      filter: filterOption,
    };
    const { data } = await getStylistRatingList(apiBody);
    this.setState({
      ...this.state,
      tabIndex: 0,
      ratingData: data,
    });
  }

  render() {
    const { tabIndex, ratingData } = this.state;
    const { windowWidth } = this.props;

    const fiveStarRating = ratingData.filter((rating) => rating.value === 5);
    const fourStarRating = ratingData.filter((rating) => rating.value === 4);
    const threeStarRating = ratingData.filter((rating) => rating.value === 3);
    const twoStarRating = ratingData.filter((rating) => rating.value === 2);
    const oneStarRating = ratingData.filter((rating) => rating.value === 1);

    return (
      <Fragment>
        <Tabs
          selectedIndex={tabIndex}
          onSelect={(index) => this.setState({ tabIndex: index })}
        >
          <div className="review-wrapper row">
            <div className="col-md-12">
              <div className="heading review-heading d-flex justify-content-between position-relative">
                <div className="title d-flex">
                  <h5>All Reviews</h5>
                  {windowWidth > 767 ? (
                    <div className="review-rating">
                      <TabList>
                        <Tab
                          className={`review-tab ${
                            tabIndex === 0 ? "active" : ""
                          }`}
                        >
                          All ({this.props?.stylisDetail?.rating_count})
                        </Tab>
                        <Tab
                          className={`review-tab ${
                            tabIndex === 1 ? "active" : ""
                          }`}
                          clas
                        >
                          <i className="fa fa-star" aria-hidden="true"></i> 5
                        </Tab>
                        <Tab
                          className={`review-tab ${
                            tabIndex === 2 ? "active" : ""
                          }`}
                        >
                          <i className="fa fa-star" aria-hidden="true"></i> 4
                        </Tab>

                        <Tab
                          defaultChecked={true}
                          default={true}
                          className={`review-tab ${
                            tabIndex === 3 ? "active" : ""
                          }`}
                        >
                          <i className="fa fa-star" aria-hidden="true"></i> 3
                        </Tab>

                        <Tab
                          className={`review-tab ${
                            tabIndex === 4 ? "active" : ""
                          }`}
                        >
                          <i className="fa fa-star" aria-hidden="true"></i> 2
                        </Tab>

                        <Tab
                          className={`review-tab ${
                            tabIndex === 5 ? "active" : ""
                          }`}
                        >
                          <i className="fa fa-star" aria-hidden="true"></i> 1
                        </Tab>
                      </TabList>

                      {/* </Tabs> */}
                    </div>
                  ) : (
                    ""
                  )}
                </div>
                <div className="filter pr-4 dropdown">
                  show all{" "}
                  <img
                    src={FilterIcon}
                    alt=""
                    className="dropdown-toggle pl-2"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    role={"button"}
                  />
                  <div
                    className="dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <h5 className="dropdown-header">Short By</h5>
                    <a className="dropdown-item" onClick={() => this.showFilterData("newest")}>Newest First</a>
                    <a className="dropdown-item" onClick={() => this.showFilterData("highest")}>Highest First</a>
                    <a className="dropdown-item" onClick={() => this.showFilterData("lowest")}>Lowest First</a>
                  </div>
                </div>
              </div>
              {windowWidth < 768 ? (
                <div className="review-rating">
                  <TabList>
                    <Tab
                      className={`review-tab ${tabIndex === 0 ? "active" : ""}`}
                    >
                      All ({this.props?.stylisDetail?.rating_count})
                    </Tab>
                    <Tab
                      className={`review-tab ${tabIndex === 1 ? "active" : ""}`}
                      clas
                    >
                      <i className="fa fa-star" aria-hidden="true"></i> 5
                    </Tab>
                    <Tab
                      className={`review-tab ${tabIndex === 2 ? "active" : ""}`}
                    >
                      <i className="fa fa-star" aria-hidden="true"></i> 4
                    </Tab>

                    <Tab
                      className={`review-tab ${tabIndex === 3 ? "active" : ""}`}
                    >
                      <i className="fa fa-star" aria-hidden="true"></i> 3
                    </Tab>

                    <Tab
                      className={`review-tab ${tabIndex === 4 ? "active" : ""}`}
                    >
                      <i className="fa fa-star" aria-hidden="true"></i> 2
                    </Tab>

                    <Tab
                      className={`review-tab ${tabIndex === 5 ? "active" : ""}`}
                    >
                      <i className="fa fa-star" aria-hidden="true"></i> 1
                    </Tab>
                  </TabList>
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
          <div className="row">
            <TabPanel>
              <RatingCard ratingData={ratingData} />
            </TabPanel>
            <TabPanel>
              <RatingCard ratingData={fiveStarRating} />
            </TabPanel>
            <TabPanel>
              <RatingCard ratingData={fourStarRating} />
            </TabPanel>
            <TabPanel>
              <RatingCard ratingData={threeStarRating} />
            </TabPanel>
            <TabPanel>
              <RatingCard ratingData={twoStarRating} />
            </TabPanel>
            <TabPanel>
              <RatingCard ratingData={oneStarRating} />
            </TabPanel>
          </div>
        </Tabs>
      </Fragment>
    );
  }
}

export default windowSize(Review);
