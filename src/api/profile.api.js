import { apiGet, apiPost } from ".";
import { API_URL } from "./constant";

export const getCustomerProfile = () =>
  apiGet(`${API_URL}/customer/list-all-profiles`);

export const addCustomerFamilyMember = (body) =>
  apiPost(`${API_URL}/customer/add-family-member`, body, "form-data");

export const deleteCustomerFamilyMember = (body) =>
  apiPost(`${API_URL}/customer/delete-family-member-profile`, body);

export const markCustomerFamilyMember = (body) =>
  apiPost(`${API_URL}/customer/make-profile-default`, body);

export const updateCustomerProfileImage = (body) =>
  apiPost(`${API_URL}/customer/c-update-profile-image`, body);

export const updateCFamilyMemberProfile = (body) =>
  apiPost(
    `${API_URL}/customer/update-family-member-profile`,
    body,
    "form-data"
  );
