import { apiPost } from "../index";
import { API_URL } from "../constant";

export const customerLogin = (body) =>
  apiPost(`${API_URL}/userauth/customer-login`, body);

export const customeVerifyOTP = (body) =>
  apiPost(`${API_URL}/userauth/customer-verify-token`, body);

export const customeLoginWithPassword = (body) =>
  apiPost(`${API_URL}/userauth/c-login-with-password`, body);

export const customeSignUp = (body) =>
  apiPost(`${API_URL}/userauth/customer-register`, body);
