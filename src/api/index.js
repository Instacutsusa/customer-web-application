import axios from "axios";
export const checkUserAuth = () => localStorage.getItem("access_token");
export function getHeaders() {
  const tokenData = localStorage.getItem("access_token");
  // const parsedToken = JSON.parse(tokenData);
  // eslint-disable-next-line no-console
  if (!tokenData) {
    return "";
  }
  const headers = {
    Authorization: `Bearer ${tokenData}` || "",
    "Content-Type": "application/json",
  };
  return headers;
}
export function apiReq(
  endPoint,
  data,
  method,
  content_type,
  headers,

  requestOptions = {}
) {
  return new Promise((resolve, reject) => {
    // eslint-disable-next-line no-param-reassign
    headers = {
      ...getHeaders(),
      ...headers,
    };
    if (method === "get" || method === "delete") {
      // eslint-disable-next-line no-param-reassign
      data = {
        ...requestOptions,
        params: data,
        headers,
        data: {},
      };
      // eslint-disable-next-line no-param-reassign
      data.paramsSerializer = (params) =>
        JSON.stringify(params, { arrayFormat: "repeat" });
    }

    const form_data = new FormData();
    if (content_type === "form-data") {
      for (var key in data) {
        form_data.append(key, data[key]);
      }
    }

    const apiData = content_type === "form-data" ? form_data : data;

    axios[method](endPoint, apiData, { headers })
      .then((result) => {
        // eslint-disable-next-line no-shadow
        const { data } = result;
        if (data.status === false) {
          return reject(data);
        }
        return resolve(data);
      })
      .catch((error) => reject(error));
  });
}
export function apiGet(
  endPoint,
  data,
  content_type,
  headers = {},
  requestOptions
) {
  return apiReq(endPoint, data, "get", content_type, headers, requestOptions);
}
export function apiPost(
  endPoint,
  data,
  content_type,
  headers = {},
  requestOptions
) {
  return apiReq(endPoint, data, "post", content_type, headers, requestOptions);
}
export function apiPatch(
  endPoint,
  data,
  content_type,
  headers = {},
  requestOptions
) {
  return apiReq(endPoint, data, "patch", content_type, headers, requestOptions);
}
export function apiPut(
  endPoint,
  data,
  content_type,
  headers = {},
  requestOptions
) {
  return apiReq(endPoint, data, "put", content_type, headers, requestOptions);
}
export function apiDelete(
  endPoint,
  data,
  content_type,
  headers = {},
  requestOptions
) {
  return apiReq(
    endPoint,
    data,
    "delete",
    content_type,
    headers,
    requestOptions
  );
}
