import { apiGet, apiPost } from ".";
import { API_URL } from "./constant";

export const getPrefferedServices = (body) => 
  apiPost(`${API_URL}/customer/list-fav-services`,body)

export const getPrefferedStylistList = (body) =>
  apiPost(`${API_URL}/customer/list-fav-stylist`, body);

export const getBlockedStylistList = () =>
  apiGet(`${API_URL}/customer/list-block-stylist`);

export const deletePrefferedStylist = (body) =>
  apiPost(`${API_URL}/customer/remove-fav-service-stylist`, body);

export const deleteBlockedStylist = (body) =>
  apiPost(`${API_URL}/customer/remove-from-blocked-list`, body);

// Stylist Detail APIs.

export const getStylistDetails = (body) =>
  apiPost(`${API_URL}/customer/stylist-detail`, body);

export const removeStylistFromFav = (body) =>
  apiPost(`${API_URL}/customer/remove-fav-service-stylist`, body);

export const removeServiceFromFav = (body) => 
  apiPost(`${API_URL}/customer/remove-fav-service-stylist`, body);

export const addStylistFromFav = (body) =>
  apiPost(`${API_URL}/customer/add-fav-service-stylist`, body);

export const getStylistServices = (body) =>
  apiPost(`${API_URL}/customer/stylist-services`, body);

// stylist rating APIs

export const getStylistRatingList = (body) =>
  apiPost(`${API_URL}/customer/list-stylist-rating`, body);

// Multiple Stylist List

export const getAllInactiveStylist = (body) =>
  apiPost(`${API_URL}/customer/show-all-inactive`, body);

// Search stylist by name

export const searchStylistByName = (body) =>
  apiPost(`${API_URL}/customer/search-stylist`, body);

//  Sort and filter stylist List

export const sortFilterStylistData = (body) =>
  apiPost(`${API_URL}/customer/search-stylist`, body);

// get All active stylist

export const getAllActiveStylist = (body) =>
  apiPost(`${API_URL}/customer/get-all-active-stylist`, body);
