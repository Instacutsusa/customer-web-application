import { apiGet, apiPost } from ".";
import { API_URL } from "./constant";

export const getListAddresses = () =>
  apiGet(`${API_URL}/customer/list-addresses`);

export const getAddLocation = (body) =>
  apiPost(`${API_URL}/customer/add-location`, body);

export const onDeleteAddress = (body) =>
  apiPost(`${API_URL}/customer/delete-address`, body);

export const setActiveAddress = (body) =>
  apiPost(`${API_URL}/customer/activate-address`, body);
