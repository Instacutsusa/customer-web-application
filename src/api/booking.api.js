import { apiGet } from ".";
import { API_URL } from "./constant";

export const getBookingAllList = (limit, page, filter) =>
  apiGet(
    `${API_URL}/customer/booking-listing?limit=${limit}&page=${page}&filter=${filter}`
  );
