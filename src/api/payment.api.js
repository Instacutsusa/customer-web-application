import { apiGet, apiPost } from ".";
import { API_URL } from "./constant";

export const getAllCardList = () => apiGet(`${API_URL}/payment/get-all-card`);

export const setDeafultCart = (body) =>
  apiPost(`${API_URL}/payment/set-default-card`, body);

export const getwalletTransactions = () =>
  apiGet(`${API_URL}/customer/wallet-transactions`);

export const addNewCustomerCard = (body) =>
  apiPost(`${API_URL}/payment/add-card`, body);

export const updateCustomerCard = (body) => 
    apiPost(`${API_URL}/payment/update-card`,body);