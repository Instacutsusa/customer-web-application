import React, { useEffect, useState } from "react";

import favImg from "../../assets/icons/delete.svg";
import LoveIcon from "../../assets/icons/love.svg";
import loveOutFll from "../../assets/icons/loveOutFll.svg";
import NoNotiIcon from "../../assets/images/noti-icon.png";

import { getPrefferedServices, removeServiceFromFav } from "../../api/stylist.api";

const FavouriteServiceDropdown = () => {
  const [data, _setData] = useState([]);
  const [serviceListLoader, _setServiceListLoader] = useState(true);
  const [serviceLoader, _setServiceLoader] = useState(false);

  useEffect(() => {
    {
      (async () => {
        const user = JSON.parse(localStorage.getItem("customer_info"));
        const apiBody = { user_id: user._id, type: 0 };
        const { message, data, status } = await getPrefferedServices(apiBody);
        if (data) {
          const newServiceData = data?.map((service) => {
            return {
              ...service,
              isFav: true,
            }
          })
          _setData(newServiceData);
        }
      })();
    }
  }, []);

  const setDisLiked = async(service_id) => {
    if(service_id){
      const disLikedResponse = await removeServiceFromFav({service_id});
      const newServiceList = data?.map((service) => {
        if(service?._id === service_id){
          return {
            ...service,
            isFav: false,
          }
        }
      })
      _setData(newServiceList);
    }
    
  }

  return (
    <>
      <div className="notification-dropdown">
        <div className="notification-header">
          <h5>Favorites</h5>
        </div>
        {true && (
          <div className="serviceContainer customScroll">
            {
                data?.map((service, index) => (
                    <div className="fav-card" key={`favService.${index}`}>
              <div className="d-flex justify-content-between align-items-center">
                <div className="d-flex align-items-center">
                  <img src={service.section_image} alt="" />
                  <div className="fav-style-details ml-3">
                    <h5>{service?.title}</h5>
                    <h6>
                      Junior Stylist
                      <span>
                        <i className="fa fa-star-o ml-2" aria-hidden="true"></i>{" "}
                        4.9
                      </span>
                    </h6>
                  </div>
                </div>
                <div className="fav-icon-action" onClick={() => setDisLiked(service._id)}>
                  {!service.isFav ? <img src={LoveIcon} alt="" /> : <img src={loveOutFll} alt="" />}
                  
                  {/* <i className="fa fa-heart" aria-hidden="true"></i> */}
                </div>
              </div>
            </div>
                ))
            }
          </div>
        )}

        {false && (
          <div className="notification-drop">
            <div className="">
              <img src={NoNotiIcon} alt="" />
              <h5>No Favorite Services To Display</h5>
              <p>Once you favorite any service, it’ll show up here.</p>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default FavouriteServiceDropdown;
