import React from 'react';
import './styles.css'


const StylistFilterDropdown = () => {
    return (
        <>
            <div className="dropdownContainer">
                <span className="dropdownListHeader">Sort Stylist By</span>
                <ul className="dropdownList">
                    <li>Top Rated</li>
                    <li>No service completed</li>
                    <li>Most Prefered</li>
                </ul>
                <span className="dropdownListHeader">Filter By</span>
                <ul className="dropdownList">
                    <li>None</li>
                    <li>Gender</li>
                </ul>
            </div>
        </>
    )
}

export default StylistFilterDropdown;