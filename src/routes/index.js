import React from "react";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import "font-awesome/css/font-awesome.min.css";

import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";

// Pages
import Login from "../pages/login/Login";
import Password from "../pages/login/password/password";
import OtpAuth from "../pages/login/otp/otp";
import SignIn from "../pages/signup/SignUp";
import ProfilePage from "../pages/profile/profile";
import AddressMap from "../pages/address/address";
import Home from "../pages/home/home";
import Stylist from "../pages/stylist/stylist";
import ActiveStylistCard from "../components/Stylist/activeStylistList/activeStylist";
import StylistDetails from "../pages/StylistDetails/StylistDetails";
import ServiceDetails from "../pages/ServiceDetails/serviceDetails";
import OrderCart from "../pages/orderCart/orderCart";
import BookingCart from "../pages/bookingCart/bookingCart";
import RateStylish from "../pages/rateStylist/rateStylist";
import Rewards from "../pages/reward/reward";
import UserProfile from "../pages/userProfile/userProfile";
import Booking from "../pages/booking/booking";
import HistoryBooking from "../pages/history/history";
import MyAccount from "../pages/myAddress/myAddress";
import PrefferStylist from "../pages/preferredStylist/stylist";
import Payment from "../pages/payment/Payment";
import Checkout from "../pages/checkout/checkout";
import SearchStylist from "../components/Stylist/searchStylist/searchStylist";
import { ToastContainer } from "react-toastify";

import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { useStore } from "../redux/store";

export const Routes = () => {
  const store = useStore();
  //
  return (
    <div className="main-container">
      <Provider store={store}>
        <PersistGate loading={null} persistor={store.__PERSISTOR}>
          <Router basename="instacuts">
            <Switch>
              {/* Static Pages */}
              <PublicRoute restricted={true} path="/" component={Login} exact />
              <PublicRoute
                restricted={true}
                path="/register"
                component={SignIn}
                exact
              />
              <PublicRoute restricted={true} path="/otp" component={OtpAuth} />
              <PublicRoute
                restricted={true}
                path="/password"
                component={Password}
              />
              <PrivateRoute path="/profile" component={ProfilePage} />
              <PrivateRoute path="/address" component={AddressMap} />

              {/* Dynamic pages */}
              <PrivateRoute path="/home" component={Home} />
              <PrivateRoute path="/near-by-stylists" component={Stylist} />
              <PrivateRoute
                path="/active-stylists"
                component={ActiveStylistCard}
              />
              <PrivateRoute path="/search-results" component={SearchStylist} />
              <PrivateRoute
                path="/stylist-details/:stylist_id/:experience/:name"
                component={StylistDetails}
                exact
              />
              <PrivateRoute
                path="/service-details"
                exact
                component={ServiceDetails}
              />
              <PrivateRoute path="/cart" component={OrderCart} />
              <PrivateRoute path="/booking-cart" component={BookingCart} />
              <PrivateRoute path="/rating" component={RateStylish} />
              <PrivateRoute path="/checkout" component={Checkout} />

              {/* Sidebar Routes */}
              <PrivateRoute path="/rewards" component={Rewards} />
              <PrivateRoute path="/user" component={UserProfile} />
              <PrivateRoute path="/booking" component={Booking} />
              <PrivateRoute path="/history" component={HistoryBooking} />
              <PrivateRoute path="/myaccount" component={MyAccount} />
              <PrivateRoute path="/stylists" component={PrefferStylist} />
              <PrivateRoute path="/payment" component={Payment} />
            </Switch>
          </Router>
          <ToastContainer
            position="top-right"
            hideProgressBar={true}
            autoClose={5000}
            newestOnTop={true}
            closeOnClick={false}
            draggable={false}
            rtl={false}
            theme="dark"
          />
        </PersistGate>
      </Provider>
    </div>
  );
};
  